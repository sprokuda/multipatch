#include <QtWidgets/QApplication>
#include <iostream>

#include "MultiPatch.h"
#include "ThreadLogStream.h"
#include "LogStream.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MultiPatch* w = new MultiPatch();

//    QLogStream qout(std::cout, w->getLogTexEditPtr());
//    QThread* thrd = new QThread();
    ThreadLogStream* qout = new ThreadLogStream(std::cout);
//    qout->moveToThread(thrd);
    QObject::connect(qout, SIGNAL(sendLogString(QString)), w, SLOT(updateLog(QString)));

    w->show();
    return a.exec();
}

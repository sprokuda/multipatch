#pragma once
#include <iostream>
#include <streambuf>
#include <string>

#include <QTextEdit>

class QLogStream : public std::basic_streambuf<char>
{
public:
    QLogStream(std::ostream& stream, QTextEdit* text_edit) : m_stream(stream)
    {
        log_window = text_edit;
        m_old_buf = stream.rdbuf();
        stream.rdbuf(this);
    }
    ~QLogStream()
    {
        // output anything that is left
        if (!m_string.empty())
            log_window->append(m_string.c_str());

        m_stream.rdbuf(m_old_buf);
    }

protected:
    virtual int_type overflow(int_type v)
    {
        lock_guard<std::mutex> lck(m_stringMutex);
        if (v == '\n')
        {
            const char* ttt = m_string.c_str();
            log_window->append(ttt);
            m_string.erase(m_string.begin(), m_string.end());
        }
        else
            m_string += v;

        return v;
    }

    virtual std::streamsize xsputn(const char* p, std::streamsize n)
    {
        lock_guard<std::mutex> lck(m_stringMutex);
        m_string.append(p, p + n);

        int pos = 0;
        while (pos != std::string::npos)
        {
            pos = m_string.find('\n');
            if (pos != std::string::npos)
            {
                std::string tmp(m_string.begin(), m_string.begin() + pos);
                const char* ttt = tmp.c_str();
                log_window->append(ttt);
                m_string.erase(m_string.begin(), m_string.begin() + pos + 1);
            }
        }

        return n;
    }

private:
    std::ostream& m_stream;
    std::streambuf* m_old_buf;
    std::string m_string;


    QTextEdit* log_window;
    std::mutex m_stringMutex;
};

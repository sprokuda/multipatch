#include "stdafx.h"
#include <exception>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>

#include "Surface.h"


// Before calling this function make sure normals are correct
unsigned int Surface::CountFolds(double threshold) const
{
	unsigned int num_folds = 0;
	for (const Triangle& tri : triangles)
	{
		bool is_folded = false;
		for (unsigned int e = 0; e < 3; e++)
			if (tri.HasAdjacentTriangle(e))
				is_folded |= Vector3d::DotProduct(tri.normal, triangles[tri.AdjTriIndx(e)].normal) <= threshold;
		is_folded ? num_folds++ : 0;
	}
	cout << num_folds << " fold triangles (adjacent triangles with dot product of normals under " << threshold << ")\n";
	return num_folds;
}

std::set<unsigned int> Surface::GetFolds(double threshold) const
{
	std::set<unsigned int> tri_set;
	for (unsigned int i = 0; i<triangles.size(); i++)
	{
		const Triangle& tri = triangles[i];
		bool is_folded = false;
		for (unsigned int e = 0; e < 3; e++)
			if (tri.HasAdjacentTriangle(e))
				is_folded |= Vector3d::DotProduct(tri.normal, triangles[tri.AdjTriIndx(e)].normal) <= threshold;
		if (is_folded)
			tri_set.insert(i);
	}
	return tri_set;
}

/*bool Surface::WriteFoldsToFile(double threshold, const char* stl_file_path, const char* title, Color clr) const
{
... get folds by GetFolds() -> tri_set ...
FILE* stl = OpenStlFileForWrite(stl_file_path, title, tri_set.size());
if (stl == nullptr)
return false;

try
{
for (unsigned int i : tri_set)
TriangleToStlStream(stl, triangles[i], clr);
}
catch (exception e)
{
std::cout << "Error while writing to file " << stl_file_path << " : " << e.what() << endl;
fclose(stl);
return false;
}
CloseStlFileForWrite(stl);
return true;
} */


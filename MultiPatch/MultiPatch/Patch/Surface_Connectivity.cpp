#include "stdafx.h"
#include <exception>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>

#include "Surface.h"


int Surface::FindRimVertices()
{
	Stats<unsigned int> links_stats("Number of triangles connected to a vertex");
	unsigned int num_links;

	// Temporary array (one cell per vertex) for determining whether edges are single or double
	int* aux = (int*)calloc(vertices.size(), sizeof(int));

	// Temporary buffers (one cell per vertex) for connectivity information as described further below
	int* aux_pls = (int*)calloc(vertices.size(), sizeof(int));
	int* aux_mns = (int*)calloc(vertices.size(), sizeof(int));

	for (unsigned int i = 0; i< vertices.size(); i++)
		vertices[i].SetNextVertex(-1);

	// Extracting rim vertices/edges 
	num_rim_vertices = 0;
	int link, triangle_index;
	unsigned int *pv;
	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		// Pass 1: adding +1 for another end of incoming edge, -1 for another end of outgoing edge
		link = vertices[i].first_link_index;
		num_links = 0;
		while (link >= 0)
		{
			num_links++;

			triangle_index = Surface::Link2Triangle(link);   // link >> 2;

			pv = triangles[triangle_index].vertex;
			switch (Surface::Link2Vertex(link)) // (link % 4)
			{
			case 0:
				aux[*(pv + 1)] --;
				aux[*(pv + 2)] ++;

				aux_mns[*(pv + 1)] = Triangle::AdjacentCode(triangle_index, 0);
				aux_pls[*(pv + 2)] = Triangle::AdjacentCode(triangle_index, 2);

				break;
			case 1:
				aux[*(pv + 2)] --;
				aux[*(pv)] ++;

				aux_mns[*(pv + 2)] = Triangle::AdjacentCode(triangle_index, 1);
				aux_pls[*(pv)] = Triangle::AdjacentCode(triangle_index, 0);
				break;
			case 2:
				aux[*(pv)] --;
				aux[*(pv + 1)] ++;

				aux_mns[*(pv)] = Triangle::AdjacentCode(triangle_index, 2);
				aux_pls[*(pv + 1)] = Triangle::AdjacentCode(triangle_index, 1);
				break;
			case 3:
				_ASSERT(false);
			}
			link = vertex_links[link];
		}
		links_stats.Add(num_links);

		// Pass 2: look for non-zeroes in aux and restore 0; where 0 was found, it's two neighbor triangles

		// Internal function check_neighbor checks edge connecting current vertex with a specified neighbor vertex.
		// First it checks sum recorded in buffer aux: if the number is 0, then this edge is double, and therefore 
		// not a rim edge, and it connects two triangles; then the connection is recorded for current triangle.
		// The connection info is stored either in buffer aux_mns (for edges originating from vertex i) or
		// in buffer aux_pls (for edges directed towards vertex i); an actual buffer is specified by the caller.
		// If buffer aux's slot contains a -1 of +1, then this is a rim edge. But we only record a rim edge  from
		// the current vertex, that is, if aux's value is -1. In any case, a non-zero aux's cell must be set back to 0 
		// for next use. For both cases of rim edges, absence of an adjacent triangle is marked by -1 in corresponding
		// element of adjacent_triangle member array.
		// Cells in adjacent_triangle correspond to triangle's edges. Cell [0] corresponds to edge originating from vertex[0],
		// adjacent_triangle[1] - to edge originating from vertex[1], and adjacent triangle[2] - from vertex[2].
		// Contents of these cells is either -1 (no adjacent triangle, see above) or a nonnegative code where index and edge of 
		// the adjacent triangle are encoded: 4 * tri_index + edge_index.

		auto check_neighbor = [this, &i, &aux](unsigned int & neighbor_vertex,
			int this_triangle, unsigned int side_index, int* neighbor_triangle_buffer)
		{
//@			ASSERT(neighbor_vertex < vertices.size(), " neighbor = " << neighbor_vertex);
			if (!(neighbor_vertex < vertices.size()))
				throw runtime_error(" neighbor = " + std::to_string(neighbor_vertex));

			switch (aux[neighbor_vertex])
			{
			case 0:
				triangles[this_triangle].adjacent_triangle[side_index] = neighbor_triangle_buffer[neighbor_vertex];
				break;

			case -1:
				vertices[i].SetNextVertex(neighbor_vertex);
				num_rim_vertices += 1;
				aux[neighbor_vertex] = 0;
				triangles[this_triangle].adjacent_triangle[side_index] = -1;
				break;

			case 1:
				aux[neighbor_vertex] = 0;
				triangles[this_triangle].adjacent_triangle[side_index] = -1;
				break;

			default:
				std::cout << " Wrong value of aux[" << neighbor_vertex << "] = " << aux[neighbor_vertex] << ", i=" << i << "\n";
				_ASSERT(false);
			}
		};

		link = vertices[i].first_link_index;
		while (link >= 0)
		{
			triangle_index = Surface::Link2Triangle(link); // link >> 2;
			pv = triangles[triangle_index].vertex;
			switch (Surface::Link2Vertex(link))  // (link % 4)
			{
			case 0:
				check_neighbor(*(pv + 1), triangle_index, 0, aux_pls);
				check_neighbor(*(pv + 2), triangle_index, 2, aux_mns);
				break;
			case 1:
				check_neighbor(*(pv + 2), triangle_index, 1, aux_pls);
				check_neighbor(*(pv), triangle_index, 0, aux_mns);
				break;
			case 2:
				check_neighbor(*(pv), triangle_index, 2, aux_pls);
				check_neighbor(*(pv + 1), triangle_index, 1, aux_mns);
				break;
			case 3:
				_ASSERT(false);
			}
			link = vertex_links[link];
		}

	} // End of outer loop over vertices

	free(aux);
	free(aux_pls);
	free(aux_mns);

	//links_stats.Tell();

	return num_rim_vertices;
}

// This routine may throw SelfIntersectingRimException 
int Surface::FindRimContours()
{
	rims.clear();

	if (num_rim_vertices == 0)
		return 0;

	bool* is_processed = (bool*)calloc(vertices.size(), sizeof(bool));


	unsigned int total_count = 0, local_count;
	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		if (is_processed[i])
			continue;

		if (vertices[i].GetNextVertex() < 0)
			continue;

		local_count = 0;
		int start_vertex = i;
		int next_vertex = start_vertex;
		do
		{
			//	ASSERT(!is_processed[next_vertex], " Vertex " << next_vertex << " was processed before"); TODO: THIS FIRES ON FILE c.stl !!!
			//  When commented out, program goes to infinite loop.
			if (is_processed[next_vertex])
				throw SelfIntersectingRimException(next_vertex);

			local_count++;
			if (is_processed[next_vertex])
				break;

//@			ASSERT(next_vertex >= 0 && next_vertex < (int)vertices.size(), "next_vertex = " << next_vertex << ", i=" << i);
			if (!(next_vertex >= 0 && next_vertex < (int)vertices.size()))
				throw runtime_error("next_vertex = " + std::to_string(next_vertex) + ", i=" + std::to_string(i));

			is_processed[next_vertex] = true;
			next_vertex = vertices[next_vertex].GetNextVertex();
		} while (next_vertex != start_vertex);

		if (next_vertex != start_vertex)  // TODO put assert
			cout << "WARNING  next_vertex " << next_vertex << " != start vertex " << start_vertex << ", next(next)" << vertices[next_vertex].GetNextVertex() << endl;

		rims.push_back(Contour(this, local_count, start_vertex, triangles[GetATriangleOfVertex(start_vertex)].normal));
		total_count += local_count;
		if (total_count >= num_rim_vertices)
			break;
	}
//@	ASSERT(total_count == num_rim_vertices, "Rim vertex count check failed, " << total_count << " vs expected " << num_rim_vertices);
	if (!(total_count == num_rim_vertices))
		throw runtime_error("Rim vertex count check failed, " + std::to_string(total_count) + " vs expected " + std::to_string(num_rim_vertices));

	free(is_processed);

	return rims.size();
}


void Surface::RebuildConnectivity()
{
	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		vertices[i].first_link_index = -1;
		vertices[i].last_link_index = -1;
	}

	unsigned int n = triangles.size();
	vertex_links.resize(4 * n);

	int v_index, link_index;

	for (unsigned int i = 0; i < triangles.size(); i++)
	{
		for (unsigned int j = 0; j < 3; j++)
		{
			v_index = triangles[i].vertex[j];
			link_index = Surface::TriangleAndVertex2Link(i, j);  //4 * i + j;

			if (vertices[v_index].first_link_index < 0)
				vertices[v_index].first_link_index = link_index;
			else
				vertex_links[vertices[v_index].last_link_index] = link_index;

			vertices[v_index].last_link_index = link_index;
			vertex_links[link_index] = -1;
		}
	}

	int k = FindRimVertices();
	FindRimContours();      // this may fire SelfIntersectingRimException
	if (rims.size() == 0)
		cout << "No rims (closed surface)\n";
	else
		cout << "Rims include " << rims.size() << " contours and " << k << " vertices\n";

	// New: calculating mean normals
	for (Triangle& tri : triangles)
		tri.CalculateMeanNormal(&vertices[0], &triangles[0]);
}


// verbose (in) - true for more detailed output
// only_normals (out) - true if everythig is ok except normals
bool Surface::CheckIntegrity(bool verbose, bool& only_normals) const
{
	unsigned int i;
	unsigned int edge, adj_tri, adj_edge;
	unsigned int num_tri_matches = 0;
	unsigned int num_tri_mismatches = 0;
	unsigned int num_edge_matches = 0;
	unsigned int num_edge_mismatches = 0;
	unsigned int num_rim_triangles = 0;
	unsigned int num_not_inside = 0;
	bool is_closed_surface = (rims.size() == 0);
	unsigned int num_vertex_matches = 0;
	unsigned int num_vertex_mismatches = 0;
	unsigned int num_folded_pairs = 0;
	unsigned int num_nans_vertex = 0;
	unsigned int num_nans_triangle = 0;
	unsigned int num_non_unit_normals = 0;

	double norm_tol = 1e-4;
	double max_norm = 0;
	double min_norm = DBL_MAX;
	unsigned int max_norm_triangle, min_norm_triangle;

	for (Vertex v : vertices)
		v.HasNotANumber() ? num_nans_vertex++ : 0;

	const Vertex* valid_pointer = &(vertices[0]);

	for (i = 0; i < triangles.size(); i++)
	{
		const Triangle &tri = triangles[i];
		for (edge = 0; edge < 3; edge++)
		{
			if (tri.HasAdjacentTriangle(edge))
			{
				adj_tri = tri.AdjTriIndx(edge);
				adj_edge = tri.AdjTriEdge(edge);
				(triangles[adj_tri].AdjTriIndx(adj_edge) == i) ? num_tri_matches++ : num_tri_mismatches++;
				(triangles[adj_tri].AdjTriEdge(adj_edge) == edge) ? num_edge_matches++ : num_edge_mismatches++;

				(tri.vertex[edge] == triangles[adj_tri].vertex[(adj_edge + 1) % 3]) ? num_vertex_matches++ : num_vertex_mismatches++;
				(tri.vertex[(edge + 1) % 3] == triangles[adj_tri].vertex[adj_edge]) ? num_vertex_matches++ : num_vertex_mismatches++;
				(tri.vertex[(edge + 2) % 3] == triangles[adj_tri].vertex[(adj_edge + 2) % 3]) ? num_folded_pairs++ : 0;
			}
			else
				num_rim_triangles++;
		}
		(tri.normal.HasNotANumber() || tri.center.HasNotANumber()) ? num_nans_triangle++ : 0;
		if (abs(tri.normal.Norm() - 1.0) > norm_tol)
		{
			num_non_unit_normals++;
			double d = abs(tri.normal.Norm());
			if (d > max_norm)
			{
				max_norm = d;
				max_norm_triangle = i;
			}
			if (d < min_norm)
			{
				min_norm = d;
				min_norm_triangle = i;
			}
		}
	}
	num_folded_pairs = (num_folded_pairs + 1) / 2;   // Even if one coincidence is found, which is hardly possible, num_folds will be still nonzero.

	unsigned int num_link_matches = 0, num_link_mismatches = 0;
	int link, tri, vert;
	for (i = 0; i < vertices.size(); i++)
	{
		link = vertices[i].first_link_index;
		while (link >= 0)
		{
			tri = Surface::Link2Triangle(link);
			vert = Surface::Link2Vertex(link);
			triangles[tri].vertex[vert] == i ? num_link_matches++ : num_link_mismatches++;
			link = vertex_links[link];
		}
	}
	bool all_ok_except_normals = num_tri_mismatches == 0 && num_edge_mismatches == 0 && (num_rim_triangles - num_rim_vertices) == 0 &&
		num_vertex_mismatches == 0 && num_nans_vertex == 0 && num_nans_triangle == 0;
	bool passed = all_ok_except_normals && num_non_unit_normals == 0;
	only_normals = all_ok_except_normals && num_non_unit_normals != 0;

	if (verbose || !passed)
		cout << (passed ? "Integrity check is passed" : "\n\nIntegrity check IS NOT PASSED");
	if (num_folded_pairs != 0 && passed)
		cout << "Integrity is valid BUT THERE " << (num_folded_pairs == 1 ? "IS A FOLDED PAIR" : ("ARE FOLDED PAIRS"));
	if (only_normals)
		cout << " because of the non-unit normals\n";
	if (verbose || !passed)
		cout << endl;

	auto end_line = [](unsigned int bad_things_count)
	{
		if (bad_things_count != 0) cout << "  <======================= NOT GOOD ";
		cout << endl;
	};

	if (verbose || !passed)
	{
		std::cout << "Vertices " << vertices.size() << ", triangles " << triangles.size() << ", contours " << rims.size() << endl;
		std::cout << "Tri index matches " << num_tri_matches << ", mismatches " << num_tri_mismatches; end_line(num_tri_mismatches);
		std::cout << "Tri edge matches " << num_edge_matches << ", mismatches " << num_edge_mismatches;  end_line(num_edge_mismatches);
		std::cout << "Rim triangles " << num_rim_triangles << ", mismatches " << num_rim_triangles - num_rim_vertices;  end_line(num_rim_triangles - num_rim_vertices);
		std::cout << "Vertex link matches " << num_link_matches << ", mismatches " << num_link_mismatches;   end_line(num_link_mismatches);
		std::cout << "Links passed " << (num_link_matches + num_link_mismatches) <<
			", expected " << (3 * triangles.size());   end_line(num_link_matches + num_link_mismatches - 3 * triangles.size());
		std::cout << "Vertex matches " << num_vertex_matches << ", mismatches " << num_vertex_mismatches; end_line(num_vertex_mismatches);
		std::cout << "Nans among vertices " << num_nans_vertex << ", triangle normals and/or centers " << num_nans_triangle; end_line(num_nans_vertex + num_nans_triangle);
		std::cout << "Non-unit normals (tolerance " << norm_tol << ") " << num_non_unit_normals; end_line(num_non_unit_normals);
		if (num_non_unit_normals != 0)
		{
			if (max_norm > 1.0)  std::cout << "Maximum norm normal " << max_norm << " at triangle " << max_norm_triangle << endl;
			if (min_norm < 1.0)  std::cout << "Minimum norm normal " << min_norm << " at triangle " << min_norm_triangle << endl;
		}
		std::cout << "Doubled triangles " << num_folded_pairs; end_line(num_folded_pairs);
	}
	else if (num_folded_pairs != 0)
	{
		std::cout << "Doubled triangles " << num_folded_pairs; end_line(num_folded_pairs);
	}
	if (verbose)
		std::cout << endl;

	return passed;
}


#include "stdafx.h"
#include <exception>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>

#include "Contour.h"
#include "Surface.h"

std::ostream& operator<< (std::ostream &out, const Contour &c)
{
	out << "Contour [" << c.num_vertices << "]:";
	unsigned int n = 0;
	int v = c.first_vertex;
	do
	{
		out << "  " << v;
		n++;
		if (v >= 0)
			v = c.ps->vertices[v].GetNextVertex();
	} while (v >= 0 && v != c.first_vertex && n <= c.num_vertices);

	if (v != c.first_vertex)
		out << "\nLast vertex does not point to first vertex";
	out << endl;
	return out;
}

unsigned int Anchor::GetATriangle(const Surface& s) const
{
	if (type == Type::InsideTriangle)
		return index;
	else if (type == Type::OverEdge)
		return index >> 2;
	else
		return s.GetATriangleOfVertex(index);
}

void Anchor::GetTwoTriangles(const Surface& s, unsigned int& tri1, unsigned int& tri2) const  // TODO: use index2
{
	_ASSERT(type == Type::OverEdge);
	tri1 = index >> 2;
	_ASSERT(s.triangles[tri1].HasAdjacentTriangle(index % 4));
	tri2 = s.triangles[tri1].AdjTriIndx(index % 4);
}

void Anchor::GetTwoTriangles(const Surface& s, unsigned int& tri1, unsigned int& tri2, unsigned int& edge1, unsigned int& edge2) const   // TODO: use index2
{
	_ASSERT(type == Type::OverEdge);
	tri1 = index >> 2;
	edge1 = index % 4;
	_ASSERT(s.triangles[tri1].HasAdjacentTriangle(edge1));
	tri2 = s.triangles[tri1].AdjTriIndx(edge1);
	edge2 = s.triangles[tri1].AdjTriEdge(edge1);
}


Vector3d ContourOnSurface::NormalAtVertex(unsigned int v) const
{
//@	ASSERT(v < num_vertices, "Vertex index " << v << " out of range for contour of " << num_vertices << " vertices\n");
	if (!(v < num_vertices))
		throw runtime_error("Vertex index " + std::to_string(v) + " out of range for contour of " + std::to_string(num_vertices) + " vertices\n");

	switch (vertices[v].anchor.type)
	{
	case Anchor::InsideTriangle:
		return s.triangles[vertices[v].anchor.GetTriangle()].normal;
	case Anchor::OverEdge:
	{
		unsigned int t1, t2;
		vertices[v].anchor.GetTwoTriangles(s, t1, t2);
		return (s.triangles[t1].normal + s.triangles[t2].normal).Normalize();
	}
	case Anchor::OverVertex:
		return s.MeanNormalAtVertex(vertices[v].anchor.GetVertex());
	}
}

bool ContourOnSurface::HasFolds() const
{
	double min_dp = 1;
	for (unsigned int i = 0; i < num_vertices; i++)
	{
		VertexOfContourOnSurface& v0 = vertices[(i - 1) % num_vertices];
		VertexOfContourOnSurface& v1 = vertices[i];
		VertexOfContourOnSurface& v2 = vertices[(i + 1) % num_vertices];
		Vector3d e1(v0, v1);
		Vector3d e2(v1, v2);
		if (e1.Norm() < 1e-10 || e2.Norm() < 1e-10)
			continue;
		double dp = (Vector3d::DotProduct(e1, e2) / e1.Norm()) / e2.Norm();
		if (dp < 0)
		{
			min_dp = min(min_dp, dp);
			cout << "\n\n WARNING:  A FOLD ON CONTOUR ON SURFACE at i " << i << " of " << num_vertices << ", dp " << dp << endl;
		}
	}
	return (min_dp < 0);
}

// Tries to remove folds by moving positions of vertices of the contour on surface.
// If a vertex is moved, its anchor is also updated to be relevant to the new position.
unsigned int ContourOnSurface::RemoveFoldsIfAny()
{
	unsigned int move_count = 0;

	auto are_vertices_too_close = [this](unsigned int i1, unsigned int i2) -> bool
	{
		return Vector3d(vertices[i1 % num_vertices], vertices[i2 % num_vertices]).Norm() < 1e-10;
	};

	auto is_fold_at_vertex = [this](unsigned int i, double& dot_product, double& norm1, double& norm2) -> bool
	{
		VertexOfContourOnSurface& v0 = vertices[(i - 1) % num_vertices];
		VertexOfContourOnSurface& v1 = vertices[i % num_vertices];
		VertexOfContourOnSurface& v2 = vertices[(i + 1) % num_vertices];
		Vector3d e1(v0, v1), e2(v1, v2);
		norm1 = e1.Norm();
		norm2 = e2.Norm();
		dot_product = (norm1 < 1e-9 || norm2 < 1e-9) ? 0 : (Vector3d::DotProduct(e1, e2) / norm1) / norm2;
		return dot_product <= 0;
	};

	for (unsigned int i = 0; i < num_vertices; i++)
	{
		double dp, n1, n2;
		if (is_fold_at_vertex(i, dp, n1, n2))
		{
			VertexOfContourOnSurface& v0 = vertices[(i - 1) % num_vertices];
			VertexOfContourOnSurface& v1 = vertices[i];
			VertexOfContourOnSurface& v2 = vertices[(i + 1) % num_vertices];
			VertexOfContourOnSurface& v3 = vertices[(i + 2) % num_vertices];
			Vector3d e1(v0, v1);
			Vector3d e2(v1, v2);
			if (n1 < 1e-10 && n2 < 1e-10)
				continue;

			double dp_next, n1_next, n2_next;
			if (n1 < 1e-10 || n2 < 1e-10 || is_fold_at_vertex(i + 1, dp_next, n1_next, n2_next))
			{
				Vector3d h = (PerpendicularFromLine(v1, v0, v3) + PerpendicularFromLine(v2, v0, v3)) / 7.0;
				//v1.SetCoords((v0 * 0.66) + (v3 * 0.34) + h);
				//v2.SetCoords((v0 * 0.34) + (v3 * 0.66) + h);
				v1.MoveTo((v0 * 0.66) + (v3 * 0.34) + h, s);
				v2.MoveTo((v0 * 0.34) + (v3 * 0.66) + h, s);
				move_count++;
			}
			else
				continue;   // we cannot remove singular or non-local fold
		}
	}
	return move_count;
}

// Moves vertex coords and updates anchor
void VertexOfContourOnSurface::MoveTo(const Xyz& p, const Surface& s)
{
	unsigned int final_tri;
	if(! s.RefineClosestTriangle_ProjDistanceToEdge(p, anchor.GetATriangle(s), final_tri, this))
		cout << "VertexOfContourOnSurface::MoveTo failed\n";
	SetCoords(p);
}

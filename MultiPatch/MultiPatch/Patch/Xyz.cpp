#include "stdafx.h"
#include <exception>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>

#include "Xyz.h"


const double Xyz::position_tolerance = 1e-5;

bool XyzPrecise::verbose = false;

double Xyz::DistanceToLine(const Xyz& a, const Xyz& b) const
{
	Vector3d ab(a, b);
	double dab = ab.Norm();
	if (dab < 1e-10)
		return DistanceTo(a);
	Vector3d pb(*this, b);
	double hb = Vector3d::DotProduct(ab, pb) / dab;
	return sqrt(pb.Norm2() - hb * hb);
}

Vector3d PerpendicularFromLine(const Xyz& p, const Xyz& a, const Xyz& b)
// Vector perpendicular from line (ab) to this point
{
	Vector3d ab(a, b);
	double dab = ab.Norm();
	if (dab < 1e-10)
		return Vector3d(a, p);
	Vector3d pb(p, b);
	double factor = (Vector3d::DotProduct(pb, ab) / dab) / dab;
	return (ab * factor) - pb;
}

bool IsPointInsideTriangle(const Xyz& point, const Xyz& t0, const Xyz& t1, const Xyz& t2, const Vector3d& normal)
{
	double alpha = (t0.x - point.x)*normal.x + (t0.y - point.y)*normal.y + (t0.z - point.z)*normal.z;
	Xyz projection(
		point.x + alpha * normal.x,
		point.y + alpha * normal.y,
		point.z + alpha * normal.z);
	Vector3d v0(projection, t0);
	Vector3d v1(projection, t1);
	Vector3d v2(projection, t2);
	Vector3d r0 = Vector3d::CrossProduct(v0, v1);
	Vector3d r1 = Vector3d::CrossProduct(v1, v2);
	Vector3d r2 = Vector3d::CrossProduct(v2, v0);
	bool is_inside =
		Vector3d::AreNearlyCollinearVectorsCodirected(r0, r1) &&
		Vector3d::AreNearlyCollinearVectorsCodirected(r1, r2);
	//((r0.x <= 0 && r1.x <= 0 && r2.x <= 0) || (r0.x >= 0 && r1.x >= 0 && r2.x >= 0)) &&
	//((r0.y <= 0 && r1.y <= 0 && r2.y <= 0) || (r0.y >= 0 && r1.y >= 0 && r2.y >= 0)) &&
	//((r0.z <= 0 && r1.z <= 0 && r2.z <= 0) || (r0.z >= 0 && r1.z >= 0 && r2.z >= 0));
	return is_inside;
}


bool DoSectionsIntersect(const Xyz& a, const Xyz& b, const Xyz& v0, const Xyz& v1, const Vector3d normal, bool& is_v0_on_the_left)
{
	Vector3d ab(a, b);
	double proj_v0 = Vector3d::DotProduct(ab, Vector3d(a, v0)) / ab.Norm();
	double proj_v1 = Vector3d::DotProduct(ab, Vector3d(a, v1)) / ab.Norm();

	if (max(proj_v0, proj_v1) < 0 || min(proj_v0, proj_v1) > ab.Norm())
		return false;

	Vector3d r1(b, v0);
	Vector3d r2(v0, a);
	Vector3d r3(a, v1);
	Vector3d r4(v1, b);

	Vector3d cp1 = Vector3d::CrossProduct(r1, r2);
	Vector3d cp2 = Vector3d::CrossProduct(r2, r3);
	Vector3d cp3 = Vector3d::CrossProduct(r3, r4);
	Vector3d cp4 = Vector3d::CrossProduct(r4, r1);

	bool do_they_intersect =
		Vector3d::AreVectorsDirectionsClose(cp1, cp2) &&
		Vector3d::AreVectorsDirectionsClose(cp2, cp3) &&
		Vector3d::AreVectorsDirectionsClose(cp3, cp4);

	if (do_they_intersect)
		is_v0_on_the_left = Vector3d::AreVectorsDirectionsClose(Vector3d::CrossProduct(Vector3d(v0, v1), Vector3d(a, b)), normal);

	return do_they_intersect;
}

bool DoSectionsIntersect(const Xyz& a, const Xyz& b, const Xyz& v0, const Xyz& v1)
{
	Vector3d ab(a, b);
	double proj_v0 = Vector3d::DotProduct(ab, Vector3d(a, v0)) / ab.Norm();
	double proj_v1 = Vector3d::DotProduct(ab, Vector3d(a, v1)) / ab.Norm();

	if (max(proj_v0, proj_v1) < 0 || min(proj_v0, proj_v1) > ab.Norm())
		return false;

	Vector3d r1(b, v0);
	Vector3d r2(v0, a);
	Vector3d r3(a, v1);
	Vector3d r4(v1, b);

	Vector3d cp1 = Vector3d::CrossProduct(r1, r2);
	Vector3d cp2 = Vector3d::CrossProduct(r2, r3);
	Vector3d cp3 = Vector3d::CrossProduct(r3, r4);
	Vector3d cp4 = Vector3d::CrossProduct(r4, r1);

	bool do_they_intersect =
		Vector3d::AreVectorsDirectionsClose(cp1, cp2) &&
		Vector3d::AreVectorsDirectionsClose(cp2, cp3) &&
		Vector3d::AreVectorsDirectionsClose(cp3, cp4);

	return do_they_intersect;
}

bool DoSectionsIntersect_verbose(const Xyz& a, const Xyz& b, const Xyz& v0, const Xyz& v1)
{
	cout << "\nDoSectionsIntersect_verbose\n";
	cout << "[ " << a << b << " ]  vs  \n[ " << v0 << v1 << " ] \n";

	Vector3d ab(a, b);
	double proj_v0 = Vector3d::DotProduct(ab, Vector3d(a, v0)) / ab.Norm();
	double proj_v1 = Vector3d::DotProduct(ab, Vector3d(a, v1)) / ab.Norm();

	cout << "ab.Norm() " << ab.Norm() << "    proj_v0 " << proj_v0 << "    proj_v1 " << proj_v1 << endl;
	if (max(proj_v0, proj_v1) < 0 || min(proj_v0, proj_v1) > ab.Norm())
		cout << "No intersection\n";
	else cout << endl;

	if (max(proj_v0, proj_v1) < 0 || min(proj_v0, proj_v1) > ab.Norm())
		return false;

	Vector3d r1(b, v0);
	Vector3d r2(v0, a);
	Vector3d r3(a, v1);
	Vector3d r4(v1, b);

	cout << " r1 " << r1 << endl;
	cout << " r2 " << r2 << endl;
	cout << " r3 " << r3 << endl;
	cout << " r4 " << r4 << endl;

	Vector3d cp1 = Vector3d::CrossProduct(r1, r2);
	Vector3d cp2 = Vector3d::CrossProduct(r2, r3);
	Vector3d cp3 = Vector3d::CrossProduct(r3, r4);
	Vector3d cp4 = Vector3d::CrossProduct(r4, r1);

	cout << " cp1 " << cp1 << endl;
	cout << " cp2 " << cp2 << endl;
	cout << " cp3 " << cp3 << endl;
	cout << " cp4 " << cp4 << endl;

	cout << "    dot prod (cp1, cp2)  " << Vector3d::DotProduct(cp1, cp2) << endl;
	cout << "    dot prod (cp2, cp3)  " << Vector3d::DotProduct(cp2, cp3) << endl;
	cout << "    dot prod (cp3, cp4)  " << Vector3d::DotProduct(cp3, cp4) << endl;

	bool do_they_intersect =
		Vector3d::AreVectorsDirectionsClose(cp1, cp2) &&
		Vector3d::AreVectorsDirectionsClose(cp2, cp3) &&
		Vector3d::AreVectorsDirectionsClose(cp3, cp4);

	cout << endl << endl;
	return do_they_intersect;
}



#pragma once
#include <iostream>
using namespace std;

#include "Auxiliary.h"
#include "Xyz.h"

class Triangle
{
public:
	Triangle() {};
	Triangle(unsigned int v0, unsigned int v1, unsigned int v2, Vector3d n, Xyz c) : normal(n), center(c)
	{
		vertex[0] = v0;
		vertex[1] = v1;
		vertex[2] = v2;
		adjacent_triangle[0] = -1;
		adjacent_triangle[1] = -1;
		adjacent_triangle[2] = -1;
	};
	unsigned int vertex[3];  // Index of each vertex

	enum Status { Unknown, Out, Stay };
	// By convention, vertices are ordered conterclockwise, and an edge index match that of its source vertex. That is,  edge [0]
	// goes from vertex [0] to vertex [1], edge [1] - from vertex [1] to vertex [2], and edge [2] - from vertex[2] to vertex [0].

	int adjacent_triangle[3]; // Neighbouring triangles across the three edges in the form  AdjacentTriangle(triangle_index, edge_index) 
							  // or -1 if no neighbour, where triangle_index is other triangle's index and edge_index is border edge's index in that triangle.

	static inline int AdjacentCode(int triangle_index, int edge_index) { return triangle_index * 4 + edge_index; };
	inline bool HasAdjacentTriangle(int edge_index) const { return (adjacent_triangle[edge_index] >= 0); };
	inline int AdjTriIndx(int edge_index) const { return (adjacent_triangle[edge_index] >> 2); };
	inline int AdjTriEdge(int edge_index) const { return (adjacent_triangle[edge_index] % 4); };

	Vector3d normal;
	Vector3d mean_normal;
	Xyz center;
	Xyz ProjectPoint(const Xyz& point) const;
	void ProjectPoint(const Xyz& point, Xyz& projection) const;

	void SetVertices(unsigned int v0, unsigned int v1, unsigned int v2, const Vertex* pvertices)
	{
		vertex[0] = v0;
		vertex[1] = v1;
		vertex[2] = v2;
		CalculateNormalAndCenter(pvertices);
	}


	void CalculateNormalAndCenter(const Vertex* pv)
	{
//@		ASSERT(pv != nullptr, "Null pointer to vertices   ");
		if (!(pv != nullptr))
			throw runtime_error("Null pointer to vertices   ");

		const Vertex& v0 = pv[vertex[0]];
		const Vertex& v1 = pv[vertex[1]];
		const Vertex& v2 = pv[vertex[2]];
		Vector3d cross = Vector3d::CrossProduct(Vector3d(v0, v1), Vector3d(v0, v2));
		double d = cross.Norm();
		if (d < 1e-15)
			throw DegenerateTriangleException();
		//   ASSERT(d > 1e-15, "Very small cross product's norm " << d);
		normal.x = cross.x / d;
		normal.y = cross.y / d;
		normal.z = cross.z / d;
		center.x = (v0.x + v1.x + v2.x) / 3;
		center.y = (v0.y + v1.y + v2.y) / 3;
		center.z = (v0.z + v1.z + v2.z) / 3;
	}

	void CalculateMeanNormal(const Vertex* pv, const Triangle* pt)
	{
		mean_normal = normal;
		if (HasAdjacentTriangle(0))
			mean_normal += pt[AdjTriIndx(0)].normal;
		if (HasAdjacentTriangle(1))
			mean_normal += pt[AdjTriIndx(1)].normal;
		if (HasAdjacentTriangle(2))
			mean_normal += pt[AdjTriIndx(2)].normal;
		mean_normal.Normalize();
	}

	friend std::ostream& operator<< (std::ostream &out, const Triangle &tri) {
		out << " Triangle " << endl;
		out << "      normal  " << tri.normal << endl;
		out << "      vertex 0  " << tri.vertex[0] << endl;
		out << "      vertex 1  " << tri.vertex[1] << endl;
		out << "      vertex 2  " << tri.vertex[2] << endl;
		out << "      center  " << tri.center << endl;
		out << "      edge 0  " << (tri.HasAdjacentTriangle(0) ? tri.AdjTriIndx(0) : -1) << endl;
		out << "      edge 1  " << (tri.HasAdjacentTriangle(1) ? tri.AdjTriIndx(1) : -1) << endl;
		out << "      edge 2  " << (tri.HasAdjacentTriangle(2) ? tri.AdjTriIndx(2) : -1) << endl;
		return out;
	};

	friend std::ostream& operator<< (std::ostream &out, const Triangle::Status& s) {
		out << (s == Triangle::Status::Unknown ? " - " : (s == Triangle::Status::Out ? " Out" : " Stay"));
		return out;
	};
};

#pragma once
#include <iostream>
using namespace std;

#include "Auxiliary.h"
#include "Xyz.h"
#include "Triangle.h"

class Surface;

class Anchor
{
public:
	enum Type { InsideTriangle, OverEdge, OverVertex };
	enum Type type;
	unsigned int index;
	unsigned int index2;
	unsigned int projected_vertex;
	Anchor() {}
	Anchor(enum Type t, unsigned int idx) : type(t), index(idx) {
		_ASSERT(t == Type::InsideTriangle || t == Type::OverVertex);
	}
	Anchor(enum Type t, unsigned int tri, unsigned int edge) : type(t), index(4 * tri + edge) {
		_ASSERT(t == Type::OverEdge);
		_ASSERT(edge <= 2);
	}
	Anchor(enum Type t, unsigned int tri, unsigned int edge, unsigned int tri2, unsigned int edge2) : type(t), index(4 * tri + edge), index2(4 * tri2 + edge2) {
		_ASSERT(t == Type::OverEdge);
		_ASSERT(edge <= 2);
		_ASSERT(edge2 <= 2);
	}
	void Set(enum Type t, unsigned int idx)
	{
		_ASSERT(t == Type::InsideTriangle || t == Type::OverVertex);
		type = t;
		index = idx;
	}
	void Set(enum Type t, unsigned int tri, unsigned int edge)
	{
//sprokuda		_ASSERT(t == Type::OverEdge);
//sprokuda		_ASSERT(edge <= 2);
		type = t;
		index = 4 * tri + edge;
	}
	void Set(enum Type t, unsigned int tri, unsigned int edge, unsigned int tri2, unsigned int edge2)
	{
		_ASSERT(t == Type::OverEdge);
		_ASSERT(edge <= 2);
		_ASSERT(edge2 <= 2);
		type = t;
		index = 4 * tri + edge;
		index2 = 4 * tri2 + edge2;
	}

	unsigned int GetTriangle() const {
		_ASSERT(type == Type::InsideTriangle);
		return index;
	}
	unsigned int GetVertex() const {
		_ASSERT(type == Type::OverVertex);
		return index;
	}
	unsigned int GetATriangle(const Surface& s) const; // works for any anchor's type
	void GetTwoTriangles(const Surface& s, unsigned int& tri1, unsigned int& tri2) const;
	void GetTwoTriangles(const Surface& s, unsigned int& tri1, unsigned int& tri2, unsigned int& edge1, unsigned int& edge2) const;

	friend std::ostream& operator<< (std::ostream &out, const Anchor &anch) {
		if (anch.type == Anchor::Type::InsideTriangle)
			out << "InsideTriangle ^" << anch.index;
		else if (anch.type == Anchor::Type::OverVertex)
			out << "OverVertex *" << anch.index;
		else
			out << "OverEdge ^" << (anch.index >> 2) << "." << (anch.index % 4) << " | ^" << (anch.index2 >> 2) << "." << (anch.index2 % 4);
		return out;
	}
};

class Contour
{
public:
	unsigned int num_vertices;
	unsigned int first_vertex;
	Vector3d first_vertex_normal;

	Contour(Surface* psurf) : ps(psurf), num_vertices(0) { }
	Contour(Surface* psurf, unsigned int num, unsigned int first, Vector3d& norm) : ps(psurf), num_vertices(num), first_vertex(first), first_vertex_normal(norm) { }
	unsigned int Count() const { return num_vertices; }
	void Initialize(Surface* psurf) {
		ps = psurf;
		num_vertices = 0;
	}
	//const Vector3d& NormalAtVertex(unsigned int v)
	//{
	//	ASSERT(v < ps->vertices.size(), "Vertex index " << v << " out of range for surface of " << ps->vertices.size() << "vertices");
	//	return ps->MeanNormalAtVertex(v);
	//}

	friend std::ostream& operator<< (std::ostream &out, const Contour &c);

private:
	Surface* ps;
};


class VertexOfContourOnSurface : public Xyz
{
public:
	Anchor anchor;
	VertexOfContourOnSurface() {}
	void SetCoords(const Xyz& p) { x = p.x; y = p.y; z = p.z; }
	void MoveTo(const Xyz& p, const Surface& s);
	friend std::ostream& operator<< (std::ostream &out, const VertexOfContourOnSurface &v) {
		out << " (" << v.x << ", " << v.y << ", " << v.z << ")  " << v.anchor;
		return out;
	}
};


class ContourOnSurface
{
public:
	const Surface& s;
	unsigned int num_vertices;
	const Contour& prototype_contour;
	VertexOfContourOnSurface* vertices;
	ContourOnSurface(const Surface &surface, const Contour& prototype) :
		s(surface),
		prototype_contour(prototype),
		num_vertices(prototype.num_vertices),
		vertices(new VertexOfContourOnSurface[prototype.num_vertices]) {}
	unsigned int Count() const { return num_vertices; }
	//~ContourOnSurface() { delete vertices; } <- If uncommented, vertices are deleted when passing object as return value (copy/move ctor may help)

	// Returns normal at the surface of projection
	Vector3d NormalAtVertex(unsigned int v) const;

	bool HasFolds() const;

	unsigned int RemoveFoldsIfAny();  // Returns number of supposed fold removals

};
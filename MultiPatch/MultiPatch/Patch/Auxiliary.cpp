#include "stdafx.h"
#include <exception>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <string>

#include "Auxiliary.h"


FILE* OpenStlFileForWrite(const char* stl_file_path, const char* title, unsigned int num_facets)
{
	cout << "Writing '" << title << "' (" << num_facets << " facets) to file '" << stl_file_path << "' ...  ";
	FILE* stl = nullptr;
	errno_t errn;
	try
	{
		errn = fopen_s(&stl, stl_file_path, "wb");
	}
	catch (exception e)
	{
		std::cout << "Error while opening for writing file " << stl_file_path << "\n";
		std::cout << e.what();
		return nullptr;
	}

	if (errn != 0 || stl == nullptr)
	{
		std::cout << "File " << stl_file_path << " could not be opened for writing, error code " << errn << "\n";
		return nullptr;
	}

	try
	{
		fwrite((void*)title, 1, 80, stl);

		uint32_t n = num_facets;
		fwrite((void*)&n, 4, 1, stl);
		return stl;
	}
	catch (exception e)
	{
		std::cout << "Error while writing to file " << stl_file_path << " : " << e.what() << endl;
		if (stl != nullptr)
			fclose(stl);
		return nullptr;
	}
}

void CloseStlFileForWrite(FILE* stl)
{
	fclose(stl);
	cout << "done\n";
}


// Make sure nbytes (size of result buffer) is at least strlen(path) + strlen(postfix) + 1
void AddPostfixToPath(const char* path, const char* postfix, char* result, size_t nbytes)
{
	std::string p(path);
	std::string x(postfix);
	size_t dot_index = p.rfind('.');
	std::string res = p.substr(0, dot_index) + x + (dot_index < p.npos ? p.substr(dot_index, p.npos - dot_index) : std::string(""));

//@	ASSERT(nbytes >= res.length() + 1, "Insufficient buffer length for postfixed file name, at least " << (res.length() + 1) << " bytes is required");
	if (!(nbytes >= res.length() + 1))
		throw runtime_error("Insufficient buffer length for postfixed file name, at least " + std::to_string(res.length() + 1) + " bytes is required");

	strncpy_s(result, nbytes, res.c_str(), res.length());
	result[res.length()] = '\0';
}

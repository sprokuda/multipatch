#pragma once
#include <algorithm>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <iostream>

#include<QObject>

using namespace std;
#include "Auxiliary.h"
#include "Xyz.h"
#include "Triangle.h"
#include "Contour.h"




class PatchedSurface;
class SeamTriangle;


class Surface //: public QObject
{
//	Q_OBJECT
public:
	Surface()  {};
	Surface(char* stl_file);
	Surface(const Surface& s, const Triangle::Status tri_stat[], std::vector<Triangle> new_triangles);
	Surface(const Surface l, const Surface r, const std::vector<SeamTriangle> seam);

	~Surface() 
	{ 
		rims.clear(); rims.shrink_to_fit();
		vertex_links.clear(); vertex_links.shrink_to_fit();
		triangles.clear(); triangles.shrink_to_fit();
		vertices.clear(); vertices.shrink_to_fit();
	}

	std::vector<Vertex> vertices;
	std::vector<Triangle> triangles;
	std::vector<int> vertex_links;
	std::vector<Contour> rims;

	// Connectivity / adjacency is entirely defined by triangles sharing common vertices.
	// A secondary but useful data construct is 'vertex_links' that allows fast traverse of triangles sharing a given vertex.
	// It is described below.
	//
	// vertex_links is an array of length 4 * num_triangles, so it has 4 fields reserved for each triangle.
	// Only 3 of them are used. 4 are reserved because "divide by 4" and "modulo 4" are faster than corresponding operations with 3.
	// The 3 used fields correspond to the 3 vertices of a triangle (see Triangle class), each containing index ("link") of 
	// a similar field of the same vertex in another triangle sharing it. So for each vertex, in essence it is sort of a list
	// of triangles that share that vertex.  Index of triangle is given by link/4, and local vertex index (0, 1, 2) in that triangle
	// is link%4. The first "link" of the given vertex is specified by data field 'first_vertex_link' of Vertex class, and the end 
	// of the list is marked by link value (index of the next item) -1.
	//
	// Another secondary connectivity information is contained in Triangle class.

	unsigned int num_rim_vertices;

	bool CheckIntegrity() const { return CheckIntegrity(true);  }
	bool CheckIntegrity(bool verbose) const { bool onlynorm; return CheckIntegrity(verbose, onlynorm); }
	bool CheckIntegrity(bool verbose, bool& only_normals) const;
	unsigned int CountFolds(double threshold) const;
	std::set<unsigned int> GetFolds(double threshold) const;
	//bool WriteFoldsToFile(double threshold, const char* stl_file_path, const char* title, Color clr) const;

	bool EnsureNotBelowBase(const Surface& base, const vector<ContourOnSurface> vcs);
	PatchedSurface ApplyPatch(Surface& patch);

	bool WriteToFile(const char* stl_file_path, const char* title, Color clr) const;
	bool FragmentToFile(const char* stl_file_path, const char* title, const std::set<unsigned int> &tri_indices, Color clr) const;
	bool FragmentToFile(const char* stl_file_path, const char* title, const std::vector<unsigned int> &tri_indices, Color clr) const;
	template <class T>
		bool TrianglesToFile(const char* stl_file_path, const char* title, const T &tri_indices, Color clr) const;
	bool OutputToStlStream(FILE* stl, Color clr) const;
	bool TriangleToStlStream(FILE* stl, const Triangle& tri, Color clr) const;
	bool TriangleToStlStream(FILE* stl, unsigned int i_tri, Color clr) const { 
//@		ASSERT(i_tri < triangles.size(), "Triangle index " << i_tri << " out of range 0 - " << (triangles.size() - 1));
		if (!(i_tri < triangles.size()))
			throw runtime_error("Triangle index " + std::to_string(i_tri) + " out of range 0 - " + std::to_string(triangles.size() - 1));
		return TriangleToStlStream(stl, triangles[i_tri], clr); 
	}
	int GetATriangleOfVertex(const int vertex) const;

	void SmoothRim();
	void RemoveRimDents();
	void FillRimIndents();

	void ReverseRim(unsigned int rim_vertex);
	unsigned int CountRimLength(unsigned int vertex) const
	{
		unsigned int n = 0;
		unsigned int v = vertex;
		do
		{
			n++;
			v = vertices[v].GetNextVertex();

		} while (v >= 0 && v != vertex && n <= vertices.size());

//@		ASSERT(v == vertex, "Not a closed contour");
		if (!(v == vertex))
			throw runtime_error("Not a closed contour");
		return n;
	}

	// Local refinement around the given vertices and in given triangles (tri_faces)
	void RefineMesh(vector<unsigned int> centers, std::vector<unsigned int> tri_faces, bool verbose);

	bool TryToRepairTriangle(unsigned int tri_index);

	void RemoveTriangles(std::set<unsigned int> tri_to_remove);

	void TellAroundTriangle(unsigned int t0) const;

	unsigned int DegreeOfVertex(unsigned int v0) const;

	unsigned int  VertexAcrossEdge(unsigned int tri_index, int edge_index) const
	{
//@		ASSERT(triangles[tri_index].HasAdjacentTriangle(edge_index), "Triangle should have adjacent triangle to have a vertex across");
		if (!(triangles[tri_index].HasAdjacentTriangle(edge_index)))
			throw runtime_error("Triangle should have adjacent triangle to have a vertex across");

		unsigned int t = triangles[tri_index].AdjTriIndx(edge_index);
		unsigned int e = triangles[tri_index].AdjTriEdge(edge_index);
		return triangles[t].vertex[(e + 2) % 3];
	}
	bool IsRimVertex(unsigned int v) const { return vertices[v].IsRimVertex(); }

	void AddFanToSet(unsigned int center_vertex, std::set<unsigned int> &tri_indices) const;

	Vector3d MeanNormalAtVertex(unsigned int vertex0) const // Unit vector is returned
	{
		Vector3d vmean(0, 0, 0);
		int link = vertices[vertex0].first_link_index;
		while (link >= 0)
		{
//@			ASSERT(Surface::Link2Triangle(link) < (int)triangles.size(), "Invalid link for vertex " << vertex0);
			if (!(Surface::Link2Triangle(link) < (int)triangles.size()))
				throw runtime_error("Invalid link for vertex " + std::to_string(vertex0));

			vmean.OffsetCoords(triangles[Surface::Link2Triangle(link)].normal);
			link = vertex_links[link];
		}
		double nrm = vmean.Norm();
		vmean.x /= nrm;
		vmean.y /= nrm;
		vmean.z /= nrm;
		return vmean;
	}

	static inline int Link2Triangle(int link) { return link >> 2; }
	static inline int Link2Vertex(int link) { return link % 4; }
	static inline int TriangleAndVertex2Link(unsigned int triangle, unsigned int vertex) { return triangle * 4 + vertex; }

	bool RefineClosestTriangle_ProjDistanceToEdge(const Xyz& point, unsigned int starting_triangle, unsigned int& final_triangle,
		VertexOfContourOnSurface *presult) const;

private:

	char* m_stl_path = nullptr;
	int m_number_of_folds = -1;  // -1 means folds were not counted
	bool ReadTriangles(char* stl_file);
	bool ImportTriangles(const Surface& s, const Triangle::Status tri_stat[], std::vector<Triangle> new_triangles);
	void RebuildConnectivity();
	void RemoveVertexAndItsTriangles(unsigned int vertex_to_remove);   

	int FindRimVertices();
	int FindRimContours();

	ContourOnSurface& ProjectRimContourToSurface(const Surface& s, const Contour &cntr, bool verbose) const;
	void AlterRimVertices(ContourOnSurface& cs);
	unsigned int NearestRimVertex(const ContourOnSurface& cs, unsigned int vertex, unsigned int hint) const;
	unsigned int NearestRimVertex(const ContourOnSurface& cs, unsigned int vertex) const;
	double DistanceToRim(const ContourOnSurface& cs, unsigned int vertex, unsigned int hint) const;

	unsigned int FindACloseTriangle(const Xyz& point, const Vector3d& normal) const;
	unsigned int RefineClosestTriangle_L2DistanceToCenter(const Xyz& point, unsigned int starting_triangle) const;

	bool HasACoincidentVertex(const Xyz& point, double tolerance, unsigned int starting_item, bool is_triangle, unsigned int &closest_vertex) const;

	bool IsPointInsideTriangle(const Xyz& point, const Triangle& tri) const;
	bool IsPointInsideTriangle(const Xyz& point, const Triangle& tri, bool verbose) const;
	bool IsPointInsideTriangle(const Xyz& point, const Triangle& tri, unsigned int& edge) const;

	bool ProjectPointToTriangleAndCheckIfInside(const Xyz& point, const unsigned int& tri_index, Xyz& projection, unsigned int& edge) const;

	void ProjectPointToTriangle(const Xyz& point, const unsigned int& tri_index, Xyz& projection) const;
	double MeasureDistanceToTriangle(const Xyz& projection, const unsigned int& tri_index,
		unsigned int& edge_or_vertex, bool& is_edge) const;

	Xyz ProjectPointOverEdge(unsigned int tri_index_1, const Xyz& proj_1, unsigned int tri_index_2, const Xyz& proj_2) const;


	bool DoesSectionIntersectEdge(const Xyz& a, const Xyz&b, unsigned int tri_index, unsigned int edge, bool& is_v0_on_the_left) const;

	unsigned int FindBestEdge(const Xyz& a, const Xyz&b, unsigned int tri_index, unsigned int entry_edge, bool& is_v0_on_the_left) const;

	void CutByContour(const ContourOnSurface& cs, Triangle::Status tri_stat[], Vertex::Status vert_stat[]) const;
	
	void MarkDeletedParts(Triangle::Status tri_stat[], Vertex::Status vert_stat[]) const;

	void TellStatusesAroundFan(unsigned int ivertex, Triangle::Status tri_stat[], Vertex::Status vert_stat[]) const;

	void ShowDisputedVertices(Triangle::Status tri_stat[], Vertex::Status vert_stat[]) const;

	bool IsBadTriangle(unsigned int tri_index) const;

	bool RepairTriangle(unsigned int tri_index);

	double MeanRelativeDeltaAroundVertex(unsigned int v0, const Vector3d& mean_normal) const
	{
		double d_mean = 0;
		unsigned int count = 0;
		int link = vertices[v0].first_link_index;
//@		ASSERT(link >= 0, "No triangles having vertex " << v0);
		if (!(link >= 0))
			throw runtime_error("No triangles having vertex " + std::to_string(v0));
		while (link >= 0)
		{
			unsigned int t = Surface::Link2Triangle(link);
			unsigned int v = Surface::Link2Vertex(link);

//@			ASSERT(t < (int)triangles.size(), "Invalid link for vertex " << v0);
			if (!(t < (int)triangles.size()))
				throw runtime_error("Invalid link for vertex " + std::to_string(v0));

			unsigned int v1 = triangles[t].vertex[(v + 1) % 3];
			Vector3d e(vertices[v0], vertices[v1]);
			d_mean += Vector3d::DotProduct(mean_normal, e) / e.Norm();
			count++;
			link = vertex_links[link];
		}
		if (count > 0)
			d_mean /= count;
		return d_mean;
	}

	void UpdateNormalsAroundVertex(unsigned int vertex)
	{
		const Vertex* p = &(vertices[0]);
		int link = vertices[vertex].first_link_index;
		while (link >= 0)
		{
			triangles[Surface::Link2Triangle(link)].CalculateNormalAndCenter(p);
			link = vertex_links[link];
		}
	}

};






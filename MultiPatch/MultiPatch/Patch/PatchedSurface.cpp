#include "stdafx.h"
#include <exception>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>

#include "Surface.h"
#include "PatchedSurface.h"


bool SeamTriangle::WriteSeamToFile(char* stl_file_path, char* title, const vector<SeamTriangle> seam,
	const Surface& l, const Surface& r, Color clr_seam)
{
	FILE* stl = OpenStlFileForWrite(stl_file_path, title, seam.size());
	if (stl == nullptr)
		return false;

	try
	{
		for (const SeamTriangle& stri : seam)
			stri.OutputToStlStream(stl, l, r, clr_seam);
	}
	catch (exception e)
	{
		std::cout << "Error while writing to file " << stl_file_path << " : " << e.what() << endl;
		fclose(stl);
		return false;
	}
	CloseStlFileForWrite(stl);
	return true;
}

bool PatchedSurface::WriteToFile(char* stl_file_path, char* title, Color clr_l, Color clr_r, Color clr_seam) const
{
	cout << "Writing '" << title << "' to file " << stl_file_path << "' ...  ";
	FILE* stl = nullptr;
	errno_t errn;
	try
	{
		errn = fopen_s(&stl, stl_file_path, "wb");
	}
	catch (exception e)
	{
		std::cout << "Error while opening for writing file " << stl_file_path << "\n";
		std::cout << e.what();
		return false;
	}

	if (errn != 0 || stl == nullptr)
	{
		std::cout << "File " << stl_file_path << " could not be opened for writing, error code " << errn << "\n";
		return false;
	}

	try
	{
		fwrite((void*)title, 1, 80, stl);

		uint32_t n = triangles.size(); // left.triangles.size() + right.triangles.size() + seam.size();
		fwrite((void*)&n, 4, 1, stl);

		//right.OutputToStlStream(stl);
		//left.OutputToStlStream(stl);
		//for (const SeamTriangle& stri : seam)
		//	stri.OutputToStlStream(stl, left, right);

		for (unsigned int i = 0; i < triangle_offset; i++)
			TriangleToStlStream(stl, triangles[i], clr_l);

		for (unsigned int i = triangle_offset; i < seam_offset; i++)
			TriangleToStlStream(stl, triangles[i], clr_r);

		for (unsigned int i = seam_offset; i < n; i++)
			TriangleToStlStream(stl, triangles[i], clr_seam);

		fclose(stl);
		cout << "done\n";
		return true;
	}
	catch (exception e)
	{
		std::cout << "Error while writing to file " << stl_file_path << " : " << e.what() << endl;
		if (stl != nullptr)
			fclose(stl);
		return false;
	}

}



void PatchedSurface::RefineSeamMesh()
{
//@	ASSERT(seam_offset <= triangles.size(), "Seam_offset " << seam_offset << " exceeds number of triangles " << triangles.size())
		if (!(seam_offset <= triangles.size()))
			throw runtime_error("Seam_offset " + std::to_string(seam_offset) + " exceeds number of triangles " + std::to_string(triangles.size()));

		unsigned int n = triangles.size() - seam_offset;
	std::vector<unsigned int> refine_faces(n);

	for (unsigned int i = 0; i < n; i++)
		refine_faces[i] = i + seam_offset;
	RefineMesh(std::vector<unsigned int>(), refine_faces, false);
}

// NB! As a result of this procedure, some seam triangles may "flip" with neighboring base or patch triangles.
// Therefore seam would not be as easily distinguished. Therefore avoid to call this procedure repeatedly.
void PatchedSurface::RepairSeam()
{
	auto a_sharp_transition = [this](unsigned int t1, unsigned int t2) -> bool
	{ return (Vector3d::DotProduct(triangles[t1].normal, triangles[t2].normal) < -0.01); };

	// Loop over seam triangles: trying to repair sharpness
	unsigned int successful_repairs = 0;
	unsigned int unsuccessful_repairs = 0;
	std::set<TriangleFoldWithCoincidentVertices> folds;

	cout << endl;

	for (unsigned int i = seam_offset; i < triangles.size(); i++)
	{
		try
		{
			for (unsigned int e = 0; e < 3; e++)
			{
				unsigned int neighbor_tri = triangles[i].AdjTriIndx(e);
				if (a_sharp_transition(i, neighbor_tri))
				{
					//if (i == 203120 && neighbor_tri == 17097)
					//	TellAroundTriangle(i);

					//cout << "Sharp transition between " << i << " and " << neighbor_tri << endl;
					TryToRepairTriangle(i) ? successful_repairs++ : unsuccessful_repairs++;
				}
			}
		}
		catch (TriangleFoldWithCoincidentVertices ex)
		{
			cout << "Folded triangles " << ex.i_tri_1 << ", " << ex.i_tri_2 << endl;
			folds.insert(ex);
		}
	}
	cout << "Total repair trials: " << (successful_repairs + unsuccessful_repairs) << ", successful " << successful_repairs << endl;

	if (folds.size() > 0)
	{
		cout << "Removing " << folds.size() << " triangle folds... \n";

		std::set<unsigned int> tri_to_remove;

		for (auto ex : folds)
		{
			tri_to_remove.insert(ex.i_tri_1);
			tri_to_remove.insert(ex.i_tri_2);

			int link = vertices[ex.i_vrt_1].first_link_index;
			while (link >= 0)
			{
				Triangle& tri = triangles[Surface::Link2Triangle(link)];

				for (unsigned int j = 0; j < 3; j++)
					if (tri.vertex[j] == ex.i_vrt_1)
						tri.vertex[j] = ex.i_vrt_2;

				link = vertex_links[link];
			}
		}
		RemoveTriangles(tri_to_remove);
	}
}

void PatchedSurface::SmoothPatchedSurface(unsigned int num_it1, unsigned int num_it2)
{
	bool verbose = true;
	auto is_triangle_from_left = [this](unsigned int t) -> bool
	{ return (t < triangle_offset); };

	auto is_vertex_from_left = [this](unsigned int v) -> bool
	{ return (v < vertex_offset); };

	auto is_vertex_from_right = [this](unsigned int v) -> bool
	{ return (v >= vertex_offset); };

	auto not_very_smooth_transition = [this](unsigned int t1, unsigned int t2) -> bool
	{ return (Vector3d::DotProduct(triangles[t1].normal, triangles[t2].normal) < 0.85); };

	auto a_rough_transition = [this](unsigned int t1, unsigned int t2) -> bool
	{ return (Vector3d::DotProduct(triangles[t1].normal, triangles[t2].normal) < 0.45); };

	auto a_very_sharp_transition = [this](unsigned int t1, unsigned int t2) -> bool
	{ return (Vector3d::DotProduct(triangles[t1].normal, triangles[t2].normal) < 0); };


	auto normals_diverge = [this](unsigned int t1, unsigned int t2) -> bool
	{
		Vector3d e(triangles[t1].center, triangles[t2].center);
		return (
			Vector3d::DotProduct(triangles[t1].normal, e) -
			Vector3d::DotProduct(triangles[t2].normal, e) < 0);
	};

	auto calculate_mean_normal = [this](unsigned int vertex0) -> Vector3d // Unit vector is returned
	{
		Vector3d vmean(0, 0, 0);
		int link = vertices[vertex0].first_link_index;
		while (link >= 0)
		{
//@			ASSERT(Surface::Link2Triangle(link) < (int)triangles.size(), "Invalid link for vertex " << vertex0);
			if (!(Surface::Link2Triangle(link) < (int)triangles.size()))
				throw runtime_error("Invalid link for vertex " + std::to_string(vertex0));

			vmean.OffsetCoords(triangles[Surface::Link2Triangle(link)].normal);
			link = vertex_links[link];
		}
		double nrm = vmean.Norm();
		vmean.x /= nrm;
		vmean.y /= nrm;
		vmean.z /= nrm;
		return vmean;
	};

	auto project_point_to_vector = [](const Xyz& point, const Xyz& v0, const Vector3d& unit_vector) -> Xyz
	{
		double coeff = Vector3d::DotProduct(Vector3d(v0, point), unit_vector);
		return Xyz(v0.x + coeff * unit_vector.x, v0.y + coeff * unit_vector.y, v0.z + coeff * unit_vector.z);
	};

	std::set<unsigned int> smoothing_set;


	auto mean_position_over_fixed = [this, &smoothing_set, &calculate_mean_normal, &project_point_to_vector]
	(unsigned int vertex, Vector3d& mean_normal)->Xyz
	{
		Xyz mean(0, 0, 0);
		unsigned int count = 0;
		mean_normal = calculate_mean_normal(vertex);
//@		ASSERT(!mean_normal.HasNotANumber(), "NAN in mean normal for vertex " << vertex);
		if (!(!mean_normal.HasNotANumber()))
			throw runtime_error("NAN in mean normal for vertex " + std::to_string(vertex));

		int link = vertices[vertex].first_link_index;
		while (link >= 0)
		{
			unsigned int t = Surface::Link2Triangle(link);
			unsigned int v = triangles[t].vertex[(Surface::Link2Vertex(link) + 1) % 3];
			if (smoothing_set.find(v) == smoothing_set.end()) // Vertex v is not in smoothing_set
			{
				Xyz proj = project_point_to_vector(vertices[v], vertices[vertex], mean_normal);
				double sinus = Vector3d::CrossProduct(triangles[t].normal, mean_normal).Norm();  // because norms of both normals must be 1
				double shift = vertices[v].DistanceTo(proj) * sinus * (1 - sinus);
				double sgn = (Vector3d::DotProduct(triangles[t].normal, Vector3d(vertices[v], proj)) > 0) ? -1 : 1;
				mean.OffsetCoords(proj + mean_normal * (shift * sgn));
				//cout << vertices[v].DistanceTo(proj) << endl;
				//cout << "    mean_normal.norm " << mean_normal.Norm() << "    sin " << sinus << "    shift " << shift << "    sign " << sgn << endl;
				count++;
			}

			link = vertex_links[link];
		}

		if (count == 0)
		{
			mean.SetCoords(0, 0, 0);
			link = vertices[vertex].first_link_index;
			while (link >= 0)
			{
				unsigned int t = Surface::Link2Triangle(link);
				unsigned int v = triangles[t].vertex[(Surface::Link2Vertex(link) + 1) % 3];
				mean.OffsetCoords(project_point_to_vector(vertices[v], vertices[vertex], mean_normal));
				count++;
				link = vertex_links[link];
			}
		}
//@		ASSERT(count != 0, "No neighbor vertices for vertex " << vertex);
		if (!(count != 0))
			throw runtime_error("No neighbor vertices for vertex " + std::to_string(vertex));

		mean.x /= count;
		mean.y /= count;
		mean.z /= count;
		return mean;
	};

	auto update_normals_around_vertex = [this](unsigned int vertex)
	{
		int link = vertices[vertex].first_link_index;
		const Vertex* p = &(vertices[vertex - vertex]);  // [0]
		while (link >= 0)
		{
			triangles[Surface::Link2Triangle(link)].CalculateNormalAndCenter(p);
			link = vertex_links[link];
		}
	};

	// Smoothing by mean position over fixed vertices (those not included into smoothing_set)

	smoothing_set.clear();
	for (unsigned int i = seam_offset; i < triangles.size(); i++)  // never put here triangle_offset!
		for (unsigned int e = 0; e < 3; e++)
		{
			if (!triangles[i].HasAdjacentTriangle(e))
				continue;

			unsigned int neighbor_tri = triangles[i].AdjTriIndx(e);
			if (not_very_smooth_transition(i, neighbor_tri))
			{
				unsigned int vrtx = triangles[i].vertex[(e + 2) % 3];
				//if (!is_vertex_from_left(vrtx))     // initially this if was not commented
				smoothing_set.insert(vrtx);
				smoothing_set.insert(triangles[i].vertex[(e + 1) % 3]);
				smoothing_set.insert(triangles[i].vertex[e]);

				//unsigned int neighbor_e = triangles[i].AdjTriEdge(e);
				//unsigned int v_across = triangles[neighbor_tri].vertex[(neighbor_e + 2) % 3];
				//if (!is_vertex_from_left(v_across))
				//	smoothing_set.insert(v_across);
			}
		}
	if (verbose)
		cout << "Smoothing stage 1: " << smoothing_set.size() << " vertices\n";


	for (unsigned int it1 = 0; it1 < num_it1; it1++)
	{
		// Loop over seam triangles

		for (unsigned int v : smoothing_set)
		{
			Vector3d mean_normal;
			Xyz mean = mean_position_over_fixed(v, mean_normal);
			vertices[v].SetCoords(mean);
		}

		for (unsigned int v : smoothing_set)
			update_normals_around_vertex(v);
	}

	if (num_it2 == 0)
		return;



	// Second stage: smoothing by universal mean 

	struct VertexInfo
	{
		Xyz new_position;
		VertexInfo() {}
		VertexInfo(const Xyz& new_pos) : new_position(new_pos) {}
	};
	std::map<unsigned int, VertexInfo> new_smoothing_set;

	// Loop over seam triangles
	for (unsigned int i = seam_offset; i < triangles.size(); i++)

		if (not_very_smooth_transition(i, triangles[i].AdjTriIndx(0)) ||
			not_very_smooth_transition(i, triangles[i].AdjTriIndx(1)) ||
			not_very_smooth_transition(i, triangles[i].AdjTriIndx(2)))

			for (unsigned int v = 0; v < 3; v++)
				new_smoothing_set.insert({ triangles[i].vertex[v], VertexInfo() });

	if (verbose)
		cout << "Smoothing stage 2: " << new_smoothing_set.size() << " vertices\n";

	auto add_neighbors_to_smoothing_set = [this, &is_vertex_from_right](unsigned int vertex, std::map<unsigned int, VertexInfo> &sm_set)
	{
		int link = vertices[vertex].first_link_index;
		while (link >= 0)
		{
			unsigned int t = Surface::Link2Triangle(link);
			unsigned int v = triangles[t].vertex[(Surface::Link2Vertex(link) + 1) % 3];

			if (!is_vertex_from_right(v)) // New condition aimed at preserving base surface
				sm_set.insert({ v, VertexInfo() });

			link = vertex_links[link];
		}
	};
	


	auto mean_position_over_all = [this, &calculate_mean_normal, &project_point_to_vector](unsigned int vertex)->Xyz
	{
		Xyz mean(0, 0, 0);
		double total_weight = 0;
		Vector3d mean_normal = calculate_mean_normal(vertex);

		auto add_vertex_to_mean = [this, &mean, &mean_normal, &total_weight, &project_point_to_vector](unsigned int v, unsigned int t, unsigned int center)
		{
			double wght = 1.0 / vertices[v].DistanceTo(vertices[center]);
			Xyz proj = project_point_to_vector(vertices[v], vertices[center], mean_normal);
			/*
			double sinus = Vector3d::CrossProduct(triangles[t].normal, mean_normal).Norm();  // because norms of both normals must be 1
			double shift = vertices[v].DistanceTo(proj) * sinus * (1 - sinus);
			double sgn = (Vector3d::DotProduct(triangles[t].normal, Vector3d(vertices[v], proj)) > 0) ? -1 : 1;
			proj = proj + mean_normal * shift * sgn;
			*/
			mean.OffsetCoords(proj * wght);
			total_weight += wght;
		};

		int link = vertices[vertex].first_link_index;
		while (link >= 0)
		{
			unsigned int t = Surface::Link2Triangle(link);
			unsigned int v = triangles[t].vertex[(Surface::Link2Vertex(link) + 1) % 3];

			add_vertex_to_mean(v, t, vertex);

			unsigned int t1 = triangles[t].AdjTriIndx((Surface::Link2Vertex(link) + 1) % 3);
			unsigned int e1 = triangles.at(t).AdjTriEdge((Surface::Link2Vertex(link) + 1) % 3);
//			if (t1 > triangles.size() - 1)
//				throw runtime_error("Index out of range");//@@@
//			unsigned int v1 = triangles.at(t1).vertex[(e1 + 2) % 3];
			unsigned int v1 = triangles[t1].vertex[(e1 + 2) % 3];
			add_vertex_to_mean(v1, t1, vertex);

			link = vertex_links[link];
		}
		mean.x /= total_weight;
		mean.y /= total_weight;
		mean.z /= total_weight;

		const double vertex_weight = 0.7;
		mean.x = (1 - vertex_weight) * mean.x + vertex_weight * vertices[vertex].x;
		mean.y = (1 - vertex_weight) * mean.y + vertex_weight * vertices[vertex].y;
		mean.z = (1 - vertex_weight) * mean.z + vertex_weight * vertices[vertex].z;

		return mean;
	};

	for (unsigned int i = 0; i < num_it2; i++)
	{
		for (auto& pair : new_smoothing_set)
			pair.second.new_position = mean_position_over_all(pair.first);

		for (auto& pair : new_smoothing_set)
			vertices[pair.first].SetCoords(pair.second.new_position);

		for (auto& pair : new_smoothing_set)
			update_normals_around_vertex(pair.first);

		//if (i == num_it2 - 1)
		//	break;
		if (i < 3)
		{
			std::map<unsigned int, VertexInfo> smset_copy(new_smoothing_set);
			for (auto& pair : smset_copy)
				add_neighbors_to_smoothing_set(pair.first, new_smoothing_set);
		}
	}
	/*
	auto distance_to_line = [this](unsigned int v0, unsigned int v1, unsigned int v2) -> double
	{
	const Vertex& v = vertices[v0];
	const Vertex& a = vertices[v1];
	const Vertex& b = vertices[v2];
	Vector3d segment(a, b);
	Vector3d hypotenuse(a, v);
	double cathetus = Vector3d::DotProduct(hypotenuse, segment) / segment.Norm();
	return sqrt(hypotenuse.Norm2() - cathetus*cathetus);
	};

	// Signed distance: positive if point is below the plane and negative if above (the normal point upwards - to above)
	auto distance_to_plane = [this](Xyz& v, Vector3d& normal, Xyz& point_on_plane) -> double
	{
	return Vector3d::DotProduct(normal, Vector3d(v, point_on_plane));
	};

	for (auto pair : smoothing_set)
	{
	unsigned int vrtx = pair.first;
	unsigned int seam_t = pair.second.seam_tri;
	unsigned int neib_t = pair.second.neighbor_tri;
	Vector3d mean_normal = calculate_mean_normal(vrtx);
	double h = distance_to_plane(vertices[vrtx], triangles[neib_t].normal, vertices[pair.second.v1]);  // signed!
	double x = h / Vector3d::DotProduct(triangles[neib_t].normal, mean_normal); // distance along mean normal divided by mean_normal.Norm()
	vertices[vrtx].OffsetCoords(h * mean_normal.x, h * mean_normal.y, h * mean_normal.z);
	} */
	if (verbose)
		cout << "Smoothing stage 3: completed";
}

// Attaching two surfaces along their rims by triangulation of the gap between them.
// Contours and vertex statuses (they refer to the right surface, or base) serve for establishing correspondence between rims.
std::vector<SeamTriangle> Stitch(Surface& left, Surface& right, std::vector<ContourOnSurface> vcs, Vertex::Status r_vert_stat[])
{
	bool verbose = true;
	std::vector<SeamTriangle> seam;
	std::vector<bool> is_left_contour_used(left.rims.size(), false);
	//std::vector<bool> is_right_contour_used(right.rims.size(), false);

	// TODO: May be using contour-on-surface objects to identify pair of contours (instead of vicinity) would be better
	for (Contour&right_contour : right.rims)
	{
		Vertex& v = right.vertices[right_contour.first_vertex];

		//{
		//	std::set<unsigned int> frag;
		//	right.AddFanToSet(right_contour.first_vertex, frag);
		//
		//	char buf[128];
		//	sprintf_s(buf, "frag_%d.stl", right_contour.first_vertex);
		//	right.FragmentToFile(buf, buf, frag, Color::Green());
		//}

		// Searching for closest to v vertex along left.rims
		double d, dmin = DBL_MAX;
		int lv, lv_min = -1, lv_min_contour;
		// NB: lv has signed type because GetNextVertex may return -1 

		for (unsigned int i_left_rim = 0; i_left_rim < left.rims.size(); i_left_rim++)
		{
			if (is_left_contour_used[i_left_rim])
				continue;

			Contour&left_contour = left.rims[i_left_rim];
			lv = left_contour.first_vertex;
			do
			{
				d = left.vertices[lv].DistanceTo(v);
				if (dmin > d)
				{
					dmin = d;
					lv_min = lv;
					lv_min_contour = i_left_rim;
				}
				lv = left.vertices[lv].GetNextVertex();

			} while (lv >= 0 && lv != left_contour.first_vertex);
		}
//@		ASSERT(lv_min >= 0, "Could not find closest vertex for right rim vertex " << right_contour.first_vertex);
		if (!(lv_min >= 0))
			throw runtime_error("Could not find closest vertex for right rim vertex " + std::to_string(right_contour.first_vertex));
		//{
		//	std::set<unsigned int> frag;
		//	left.AddFanToSet(lv_min, frag);
		//
		//	char buf[128];
		//	sprintf_s(buf, "frag_patch_%d.stl", lv_min);
		//	left.FragmentToFile(buf, buf, frag, Color::Purple());
		//}

		// Closest rim vertices are found
		if (verbose)
		{
			//cout << "Stitching starts from vertices l." << lc.first_vertex << " and r." << rv_min << endl;
			cout << "Rim vertex counts " << right.CountRimLength(right_contour.first_vertex) << " and " << left.CountRimLength(lv_min) << ", distance " << dmin;
		}
		is_left_contour_used[lv_min_contour] = true;

		unsigned int seam_offset = seam.size();
		unsigned int num_closing_tri = StitchAlongRim(left, right, lv_min, right_contour.first_vertex, seam);
		//if (verbose)
		//	cout << ",  closing triangles " << num_closing_tri << endl;
		unsigned int f1, f2;
		IsSeamGood(seam, seam_offset, seam.size() - seam_offset, f1, f2);
	}
	if (verbose)
		cout << "Seam triangles total " << seam.size() << endl;

	for (auto &tri : seam)
		tri.CalculateNormal(left, right);

	unsigned int not_used = 0;
	for (unsigned int i_left_rim = 0; i_left_rim < left.rims.size(); i_left_rim++)
		is_left_contour_used[i_left_rim] ? 0 : not_used++;

	if (not_used > 0)
		cout << "\n\n   ***   WARNING:  " << not_used << " left (patch) contours remained unused for seams, nonclosed surface may result   *** \n\n";

	return seam;
}


bool IsSeamGood(std::vector<SeamTriangle>& seam, unsigned int base, unsigned int n, unsigned int &num_double_folds, unsigned int &num_single_folds)
{
//@	ASSERT(base + n <= seam.size(), "Base and/or length of seam subvector out of range " << seam.size());
	if (!(base + n <= seam.size()))
		throw runtime_error("Base and/or length of seam subvector out of range " + std::to_string(seam.size()));

	num_double_folds = 0;
	num_single_folds = 0;
	double dotprod_threshold = -0.75;
	for (unsigned int i = 0; i < n; i++)
	{
		double dp_prev = Vector3d::DotProduct(seam[base + i].normal, seam[base + ((i - 1) % n)].normal);
		double dp_next = Vector3d::DotProduct(seam[base + i].normal, seam[base + ((i + 1) % n)].normal);
		bool folded_prev = dp_prev < dotprod_threshold;
		bool folded_next = dp_next < dotprod_threshold;
		if (folded_next || folded_prev)
			(folded_next && folded_prev) ? num_double_folds++ : num_single_folds++;
	}
	cout << "   Folds: " << num_double_folds << " / " << num_single_folds << " of " << n << endl;
	return (num_double_folds == 0 && num_single_folds == 0);
}

// It is supposed that vleft and vright are closest vertices on two rims
unsigned int StitchAlongRim(Surface& left, Surface& right, unsigned int vleft, unsigned int vright, std::vector<SeamTriangle>& seam)
{
	// We have to reverse one rim's order in order to enable visiting close vertices of two rims concurrently.
	// Why reversing right rim (rather than left): because if we look at the gap between surfaces so that 'left' is on the left
	// and 'right' is on the right, then, due to the counterclockwise ordering of triangle vertices, left rim would be directed forward 
	// and right rim - backward. And it is more intuitive to move (stitch) forward along the rims than backward.

	right.ReverseRim(vright);

	unsigned int vl = vleft, vr = vright;
	unsigned int vl_next = left.vertices[vl].GetNextVertex();
	unsigned int vr_next = right.vertices[vr].GetNextVertex();
	bool vl_moved = false, vr_moved = false;

	auto normal_of = [](const Vertex& v0, const Vertex& v1, const Vertex& v2, bool& degenerate) -> Vector3d
	{
		//Vector3d cross = Vector3d::CrossProduct(Vector3d(v0, v1), Vector3d(v0, v2));
		Vector3d cross = Vector3d::CrossProduct(Vector3d(v0, v1), Vector3d(v1, v2));
		double d = cross.Norm();
		degenerate = (d < 1e-15);
		if (degenerate) d = 1;
		return Vector3d(cross.x / d, cross.y / d, cross.z / d);
	};

	auto left_rim_normal = [&left](unsigned int vcurr, unsigned int vnext) -> Vector3d
	{
//@		ASSERT(vcurr <= left.vertices.size(), "Vertex index " << vcurr << " out of range 0 - " << (left.vertices.size() - 1));
		if (!(vcurr <= left.vertices.size()))
			throw runtime_error("Vertex index " + std::to_string(vcurr) + " out of range 0 - " + std::to_string(left.vertices.size() - 1));

//@		ASSERT(vnext <= left.vertices.size(), "Vertex index " << vnext << " out of range 0 - " << (left.vertices.size() - 1));
		if (!(vnext <= left.vertices.size()))
			throw runtime_error("Vertex index " + std::to_string(vnext) + " out of range 0 - " + std::to_string(left.vertices.size() - 1));

		// At the left rim, vertex vcurr precedes vnext in their common triangle
		int link = left.vertices[vcurr].first_link_index;
		while (link >= 0)
		{
			unsigned int t = Surface::Link2Triangle(link);
			unsigned int v = Surface::Link2Vertex(link);
			if (left.triangles[t].vertex[(v + 1) % 3] == vnext)
			{
//@				ASSERT(left.triangles[t].adjacent_triangle[v] == -1,
//@					"Left triangle having vertices " << vcurr << " and " << vnext << " should be rim triangle");
				if (!(left.triangles[t].adjacent_triangle[v] == -1))
					throw runtime_error("Left triangle having vertices " + std::to_string(vcurr) + " and " + std::to_string(vnext) + " should be rim triangle");

				return left.triangles[t].normal;
			}
			link = left.vertex_links[link];
		}
		// Triangle not found...
//@		ASSERT(false, "No left triangle having both vertices " << vcurr << " and " << vnext << " could be found");
		if (!(false))
			throw runtime_error("No left triangle having both vertices " + std::to_string(vcurr) + " and " + std::to_string(vnext) + " could be found");
	};

	auto right_rim_normal = [&right](unsigned int vcurr, unsigned int vnext) -> Vector3d
	{
//@		ASSERT(vcurr <= right.vertices.size(), "Right vertex index " << vcurr << " out of range 0 - " << (right.vertices.size() - 1));
		if (!(vcurr <= right.vertices.size()))
			throw runtime_error("Right vertex index " + std::to_string(vcurr) + " out of range 0 - " + std::to_string(right.vertices.size() - 1));

//@		ASSERT(vnext <= right.vertices.size(), "Right vertex index " << vnext << " out of range 0 - " << (right.vertices.size() - 1));
		if (!(vnext <= right.vertices.size()))
			throw runtime_error("Right vertex index " + std::to_string(vnext) + " out of range 0 - " + std::to_string(right.vertices.size() - 1));

		// We should remember that at the right rim, vertex vnext precedes vcurr in their common triangle, so we start from vnext
		int link = right.vertices[vnext].first_link_index;
		while (link >= 0)
		{
			unsigned int t = Surface::Link2Triangle(link);
			unsigned int v = Surface::Link2Vertex(link);
			if (right.triangles[t].vertex[(v + 1) % 3] == vcurr)
			{
//@				ASSERT(right.triangles[t].adjacent_triangle[v] == -1,
//@					"Right triangle having vertices " << vcurr << " and " << vnext << " should be rim triangle");
				if (!(right.triangles[t].adjacent_triangle[v] == -1))
					throw runtime_error("Right triangle having vertices " + std::to_string(vcurr) + " and " + std::to_string(vnext) + " should be rim triangle");

				return right.triangles[t].normal;
			}
			link = right.vertex_links[link];
		}
		// Triangle not found...
//@		ASSERT(false, "No right triangle having both vertices " << vcurr << " and " << vnext << " could be found");
		if (!(false))
			throw runtime_error("No right triangle having both vertices " + std::to_string(vcurr) + " and " + std::to_string(vnext) + " could be found");
	};

	auto goodness_of_normal = [&normal_of, &seam](const Vertex& v0, const Vertex& v1, const Vertex& v2,
		const Vector3d& left_rim_normal, const Vector3d& right_rim_normal) -> double
	{
		bool is_degenerate;
		Vector3d normal = normal_of(v0, v1, v2, is_degenerate);
		return is_degenerate ? -100 : Vector3d::DotProduct(normal, left_rim_normal) + Vector3d::DotProduct(normal, right_rim_normal);

		//	(seam.size() > 0 ? 2* Vector3d::DotProduct(normal, (seam.end() -1)->normal) : // NB: This works poorly! Facet's norm should be compared to rim's
		//						  Vector3d::DotProduct(normal, left_rim_normal) + Vector3d::DotProduct(normal, right_rim_normal));
	};

	// The routine below checks if any of the given points (array of pointers pvert) encroaches onto the triangle (v0, v1, v2)
	auto any_encroachment = [](const Vertex& v0, const Vertex& v1, const Vertex& v2, const Vector3d& normal,
		const Vertex* pvert[], unsigned int count) -> bool
	{
		for (unsigned int i = 0; i < count; i++)
			if (IsPointInsideTriangle(*(pvert[i]), v0, v1, v2, normal))
				return true;
		return false;
	};

	// Checks if any of the given sections intersects with section [p1, p2].
	// Sections are given by vertives as follows:  [*pvert[0], *pvert[1]], ... [*pvert[count-2], *pvert[count-1]] 
	// Total number of sections specified by pvert is count-1, where count is number of points in pvert.
	auto any_crossing = [](const Xyz& p1, const Xyz& p2, const Vertex* pvert[], unsigned int count) -> bool
	{
		//return false;   <- Returning false is equivalent to not using the crossing criterion.
		// Doing so results in numerous fans. Don't remove check of crossings. 
//@		ASSERT(count >= 2, "At least 2 vertices are required to check intersections");
		if (!(count >= 2))
			throw runtime_error("At least 2 vertices are required to check intersections");

		for (unsigned int i = 1; i < count; i++)
			if (DoSectionsIntersect(p1, p2, *pvert[i - 1], *pvert[i]))
				return true;
		return false;
	};
	auto any_crossing_verb = [](const Xyz& p1, const Xyz& p2, const Vertex* pvert[], unsigned int count) -> bool
	{
//@		ASSERT(count >= 2, "At least 2 vertices are required to check intersections");
		if (!(count >= 2))
			throw runtime_error("At least 2 vertices are required to check intersections");

		for (unsigned int i = 1; i < count; i++)
			if (DoSectionsIntersect_verbose(p1, p2, *pvert[i - 1], *pvert[i]))
				return true;
		return false;
	};

	struct SeamQualityRegister
	{
		unsigned int current_fan_count, max_fan_count;
		bool is_current_left;
		double max_tack;
		const double big_tack_threshold = 1.1;
		const unsigned int big_fan_threshold = 5;

		SeamQualityRegister() : current_fan_count(0), max_fan_count(0), max_tack(0) {}
		void AddTack(char side, const Xyz& p1, const Xyz& p2)
		{
//@			ASSERT(side == 'L' || side == 'R', "Wrong side parameter '" << side << "', must be L or R");
			if (!(side == 'L' || side == 'R'))
				throw runtime_error("Wrong side parameter '" + std::to_string(side) + "', must be L or R");

			double d = p1.DistanceTo(p2);
			max_tack = max(max_tack, d);

			bool same_side = ((is_current_left && side == 'L') || ((!is_current_left) && side == 'R')) && max_fan_count > 0;
			if (same_side)
				current_fan_count++;
			else
				current_fan_count = 1;
			is_current_left = (side == 'L');
			max_fan_count = max(max_fan_count, current_fan_count);
		}
		void Tell() {
			cout << "  Max fan count " << max_fan_count << ", max tack " << max_tack;
			if (max_fan_count > big_fan_threshold || max_tack > big_tack_threshold)
				cout << "  <-- WARNING! Take a look at this seam";
		}
	};

	SeamQualityRegister sqr;

	bool right_move = true;  // This value is also used when checking sharp triangles at right that may be cut off the seam
	do
	{
//@		ASSERT(vl_next >= 0, "Contour l must be closed");
		if (!(vl_next >= 0))
			throw runtime_error("Contour l must be closed");

//@		ASSERT(vr_next >= 0, "Contour r must be closed");
		if (!(vr_next >= 0))
			throw runtime_error("Contour r must be closed");

		const Vertex& v0 = left.vertices[vl];
		const Vertex& v1 = right.vertices[vr];
		const Vertex& a = left.vertices[vl_next];
		const Vertex& b = right.vertices[vr_next];

		Vector3d left_normal = left_rim_normal(vl, vl_next);
		Vector3d right_normal;
		// Why right normal is not updated each time:
		// Because calculating a rim normal requires two consecutive rim vertices.
		// It is convenient to take vr, vr_next as consecutive right vertices. But 
		// exactly these vertices - vr and vr_next - are not always consecutive,
		// if a triangle was "cut off" from the right rim. 

		/* NEW: Cutting off a sharp triangle along right edge, if not enhcroached by near left points*/
		/* This should only be done once for a given vr; that is ensured by the condition "if (right_move)" */
		if (right_move)
		{
			right_normal = right_rim_normal(vr, vr_next);
			if (vr_next != vright)
			{
				unsigned int vr_next_next = right.vertices[vr_next].GetNextVertex();
				const Vertex& bb = right.vertices[vr_next_next];
				const Vector3d e(v1, b), ee(b, bb);
				bool degenerate_tri;
				if (Vector3d::DotProduct(e, ee) < (0.1 * e.Norm() * ee.Norm()) &&
					Vector3d::DotProduct(normal_of(v1, b, bb, degenerate_tri), right_normal) > 0.5 &&
					!degenerate_tri)
				{
					const Vertex* pvert[4];
					pvert[0] = &v0;
					pvert[1] = &a;

					Vertex& aa = left.vertices[a.GetNextVertex()];
					Vertex& aaa = left.vertices[aa.GetNextVertex()];
					pvert[2] = &aa;
					pvert[3] = &aaa;
					if (!any_encroachment(v1, b, bb, right_normal, pvert, 4))
					{
						// Cutting off triangle (v1, b, bb):  adding it to seam and pretending (v1, bb) is the rim at right
						seam.push_back(SeamTriangle(vr, false, vr_next, false, vr_next_next, false));

						vr_next = vr_next_next;
						// NB! vr_moved should not be set here, because vr is not changed
					}
				}

			}
		}

		// First we check if a potential new edge (v0-b or v1-a) crosses any existing borders (would be very bad)
		{
			const Vertex* pvert_l[3];
			pvert_l[0] = &a;
			pvert_l[1] = &(left.vertices[a.GetNextVertex()]);
			pvert_l[2] = &(left.vertices[(*pvert_l[1]).GetNextVertex()]);

			const Vertex* pvert_r[3];
			pvert_r[0] = &b;
			pvert_r[1] = &(right.vertices[b.GetNextVertex()]);
			pvert_r[2] = &(right.vertices[(*pvert_r[1]).GetNextVertex()]);

			//bool too_bad = false;
			bool cross_b = any_crossing(v0, b, pvert_l, 3) || any_crossing(v0, b, pvert_r + 1, 2);
			bool cross_a = any_crossing(v1, a, pvert_r, 3) || any_crossing(v1, a, pvert_l + 1, 2);

			if (cross_b && !cross_a)
				right_move = false;
			else if (cross_a && !cross_b)
				right_move = true;
			else
			{
				//if (cross_b && cross_a)
				//{
				//	too_bad = true;
				//}

				double goodness_a = goodness_of_normal(v0, v1, a, left_normal, right_normal);
				double goodness_b = goodness_of_normal(v0, v1, b, left_normal, right_normal);

				// NEW: considering triangle's complements. 
				// For triangle A(v0, v1, a) complement is (v1, b, a);  
				// for triangle B(v0, v1, b) complement is (v0, b, a). 
				// The idea behind using complements is that complement's normal must be good as well,
				// even if we don't know yet if that complement triangle will be formed on next step.
				// Because a bad complement's normal means a seam fold.

				const double goodness_cutoff = -0.5; // initially was 0, but don't put 0! Nonlocal seam may result.
													 // The closer this value to -2, the nore often distance criterion is used.
													 // (But if used always, it yields nonlocal seam at 'es' testcase.)

													 //if(too_bad)
													 //	cout << "\n  ****** TOO BAD  ****** goodness  " << goodness_a << ", " << goodness_b <<" \n\n";
													 //if (goodness_a < goodness_cutoff &&
													 //	goodness_b < goodness_cutoff)
													 //{
													 //	cout << "\n  ****** BAD  goodness  " << goodness_a << ", " << goodness_b << ", seam position " << seam.size() << " \n\n";
													 //	std::vector<SeamTriangle> seam_frag(seam.end() - 7, seam.end());
													 //	char buf[128];
													 //	sprintf_s(buf, "seam_frag_%d.stl", seam.size());
													 //	SeamTriangle::WriteSeamToFile(buf, "Seam fragment", seam_frag, left, right, Color::Yellow());
													 //}

				if (goodness_a > goodness_cutoff &&
					goodness_b > goodness_cutoff)
				{
					double goodness_a_complement = goodness_of_normal(v1, b, a, left_normal, right_normal);
					double goodness_b_complement = goodness_of_normal(v0, b, a, left_normal, right_normal);

					if (goodness_a_complement > goodness_cutoff &&
						goodness_b_complement > goodness_cutoff)
						right_move = (v0.DistanceTo(b) < v1.DistanceTo(a));
					else
						right_move = goodness_b_complement > goodness_a_complement;
				}
				else if (goodness_a > goodness_cutoff || goodness_b > goodness_cutoff)
					right_move = (goodness_b > goodness_a);
				else
					right_move = (v0.DistanceTo(b) < v1.DistanceTo(a));
			}
		}

		if (right_move)
		{
			seam.push_back(SeamTriangle(vl, true, vr, false, vr_next, false));
			(seam.end() - 1)->CalculateNormal(left, right);
			vr = vr_next;
			vr_next = right.vertices[vr].GetNextVertex();
			vr_moved = true;
			sqr.AddTack('R', v0, b);

			//cout << "  right moved \n";
		}
		else
		{
			seam.push_back(SeamTriangle(vl, true, vr, false, vl_next, true));
			(seam.end() - 1)->CalculateNormal(left, right);
			vl = vl_next;
			vl_next = left.vertices[vl].GetNextVertex();
			vl_moved = true;
			sqr.AddTack('L', v1, a);

			//cout << "  left moved \n";
		}
	} while (!(vl_moved && vl == vleft) && !(vr_moved && vr == vright));

	// Now seam reached one of its initial points
	// Closing the seam	

	unsigned int seam_length_before_closing = seam.size();
	if (vl == vleft)
		do
		{
			seam.push_back(SeamTriangle(vleft, true, vr, false, vr_next, false));
			(seam.end() - 1)->CalculateNormal(left, right);
			sqr.AddTack('R', right.vertices[vr_next], left.vertices[vleft]);
			vr = vr_next;
			vr_next = right.vertices[vr].GetNextVertex();

		} while (vr != vright);
	else
		do
		{
			seam.push_back(SeamTriangle(vright, false, vl_next, true, vl, true));
			(seam.end() - 1)->CalculateNormal(left, right);
			sqr.AddTack('L', left.vertices[vl_next], right.vertices[vright]);
			vl = vl_next;
			vl_next = left.vertices[vl].GetNextVertex();

		} while (vl != vleft);

		unsigned int num_closing_tri = seam.size() - seam_length_before_closing;
		sqr.Tell();

		// Restoring original rim order
		right.ReverseRim(vright);
		return num_closing_tri;
}

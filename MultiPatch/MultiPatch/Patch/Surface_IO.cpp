#include "stdafx.h"
#include <exception>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>

#include "Surface.h"

bool Surface::OutputToStlStream(FILE* stl, Color clr) const
{
	unsigned int nan_count = 0;
	try
	{
		float fx, fy, fz;
		for (unsigned int i = 0; i < triangles.size(); i++)
		{
			// Writing triangle's normal
			fx = (float)triangles[i].normal.x;
			fy = (float)triangles[i].normal.y;
			fz = (float)triangles[i].normal.z;
			fwrite((void*)&fx, 4, 1, stl);
			fwrite((void*)&fy, 4, 1, stl);
			fwrite((void*)&fz, 4, 1, stl);

			// Writing vertices
			for (unsigned int j = 0; j < 3; j++)
			{
				fx = (float)vertices[triangles[i].vertex[j]].x;
				fy = (float)vertices[triangles[i].vertex[j]].y;
				fz = (float)vertices[triangles[i].vertex[j]].z;

				fwrite((void*)&fx, 4, 1, stl);
				fwrite((void*)&fy, 4, 1, stl);
				fwrite((void*)&fz, 4, 1, stl);

				if (NotANumber(fx) || NotANumber(fy) || NotANumber(fz))
					nan_count++;
				//if( !( fx > -40 && fx < 40 && fy > -40 && fy < 40 && fz > -40 && fz < 40) )
				//cout << triangles[i].vertex[j] << "   ( " << fx << "    " << fy << "    " << fz << " ) \n";
			}
			// Writing the trailing 2 bytes
			fwrite(clr.c, 1, 2, stl);
		}
		if (nan_count > 0)
			cout << "\n\n**** WARNING from OutputToStlFile: " << nan_count << " vertices containing NaN coords ****\n";

		return true;
	}
	catch (exception e)
	{
		std::cout << "Error while writing to stl stream: ";
		std::cout << e.what();
		return false;
	}
}

bool Surface::TriangleToStlStream(FILE* stl, const Triangle& tri, Color clr) const
{
	try
	{
		float fx, fy, fz;
		{
			// Writing triangle's normal
			fx = (float)tri.normal.x;
			fy = (float)tri.normal.y;
			fz = (float)tri.normal.z;
			fwrite((void*)&fx, 4, 1, stl);
			fwrite((void*)&fy, 4, 1, stl);
			fwrite((void*)&fz, 4, 1, stl);

			// Writing vertices
			for (unsigned int j = 0; j < 3; j++)
			{
				fx = (float)vertices[tri.vertex[j]].x;
				fy = (float)vertices[tri.vertex[j]].y;
				fz = (float)vertices[tri.vertex[j]].z;

				fwrite((void*)&fx, 4, 1, stl);
				fwrite((void*)&fy, 4, 1, stl);
				fwrite((void*)&fz, 4, 1, stl);
			}
			// Writing the trailing 2 bytes
			//unsigned char uc[2] = { 0xA, 0 };
			//fwrite((void*)color, 1, 2, stl);
			fwrite(clr.c, 1, 2, stl);
		}
		return true;
	}
	catch (exception e)
	{
		std::cout << "Error while writing to stl stream: ";
		std::cout << e.what();
		return false;
	}
}

bool Surface::FragmentToFile(const char* stl_file_path, const char* title, const std::set<unsigned int> &tri_indices, Color clr) const
{
	return TrianglesToFile<std::set<unsigned int>>(stl_file_path, title, tri_indices, clr);
}

bool Surface::FragmentToFile(const char* stl_file_path, const char* title, const std::vector<unsigned int> &tri_indices, Color clr) const
{
	return TrianglesToFile<std::vector<unsigned int>>(stl_file_path, title, tri_indices, clr);
}

template <class T>
bool Surface::TrianglesToFile(const char* stl_file_path, const char* title, const T &tri_indices, Color clr) const
{
	cout << "Writing fragment '" << title << "' consisting of " << tri_indices.size() << " triangles to file " << stl_file_path << "...  ";
	FILE* stl = nullptr;
	errno_t errn;
	try
	{
		errn = fopen_s(&stl, stl_file_path, "wb");
	}
	catch (exception e)
	{
		std::cout << "Error while opening for writing file " << stl_file_path << endl;
		std::cout << e.what();
		return false;
	}

	if (errn != 0 || stl == nullptr)
	{
		std::cout << "File " << stl_file_path << " could not be opened for writing, error code " << errn << endl;
		return false;
	}

	try
	{
		fwrite((void*)title, 1, 80, stl);

		uint32_t n = tri_indices.size();
		fwrite((void*)&n, 4, 1, stl);

		bool ok;
		for (unsigned int i : tri_indices)
			ok &= TriangleToStlStream(stl, triangles[i], clr);

		fclose(stl);
		cout << "done\n";
		return ok;
	}
	catch (exception e)
	{
		std::cout << "Error while writing to file " << stl_file_path << " :  " << e.what() << endl;
		if (stl != nullptr)
			fclose(stl);
		return false;
	}
}

bool Surface::WriteToFile(const char* stl_file_path, const char* title, Color clr) const
{
	cout << "Writing '" << title << "' to file " << stl_file_path << "...  ";
	FILE* stl = nullptr;
	errno_t errn;
	try
	{
		errn = fopen_s(&stl, stl_file_path, "wb");
	}
	catch (exception e)
	{
		std::cout << "Error while opening for writing file " << stl_file_path << endl;
		std::cout << e.what();
		return false;
	}

	if (errn != 0 || stl == nullptr)
	{
		std::cout << "File " << stl_file_path << " could not be opened for writing, error code " << errn << endl;
		return false;
	}

	try
	{
		//char uc[80] = { "A surface" };
		fwrite((void*)title, 1, 80, stl);

		uint32_t n = triangles.size();
		fwrite((void*)&n, 4, 1, stl);

		bool ok = OutputToStlStream(stl, clr);

		fclose(stl);
		cout << "done\n";
		return ok;
	}
	catch (exception e)
	{
		std::cout << "Error while writing to file " << stl_file_path << " :  " << e.what() << endl;
		if (stl != nullptr)
			fclose(stl);
		return false;
	}
}


#if 0
// Reading without vertex tolerance
bool Surface::ReadTriangles(char* stl_file_path)
{
	FILE* stl = nullptr;
	errno_t errn;
	try
	{
		errn = fopen_s(&stl, stl_file_path, "rb");
	}
	catch (exception e)
	{
		std::cout << "Error while opening file for read " << stl_file_path << "\n";
		std::cout << e.what();
		return false;
	}

	if (errn != 0 || stl == nullptr)
	{
		std::cout << "File " << stl_file_path << " could not be opened, error code " << errn << "\n";
		return false;
	}
	//set<Vertex> vertex_set;
	set<Xyz1> set1;
	set<Xyz2> set2;

	try
	{
		char uc[80] = { ' ' };
		fread_s(uc, sizeof(uc), 1, 80, stl);
		if (!std::strncmp(uc, "solid ", 6))
		{
			std::cout << "Ascii STL format is detected in " << stl_file_path << endl << endl;
			std::fclose(stl);
			return false;
		}

		uint32_t n;
		fread_s((void*)&n, sizeof(n), 4, 1, stl);
		std::cout << stl_file_path << ": " << n << " triangles\n";

		if (n > (INT32_MAX >> 2))
		{
			std::cout << "Sorry but that number of triangles is too high. Maximum supported number is " << (INT32_MAX >> 2) << endl;
			return false;
		}

		Xyz1 vrtx1;
		Xyz2 vrtx2;

		float fx, fy, fz;

		set<Xyz1>::iterator last_pos1 = set1.begin();
		set<Xyz2>::iterator last_pos2 = set2.begin();

		bool first_time = true;

		for (unsigned int i = 0; i < n; i++)
		{
			// Getting triangle's normal
			fread_s(&fx, 4, 4, 1, stl);
			fread_s(&fy, 4, 4, 1, stl);
			fread_s(&fz, 4, 4, 1, stl);

			for (int j = 0; j < 3; j++)
			{
				vrtx1.Initialize();
				vrtx2.Initialize();

				fread_s(&fx, 4, 4, 1, stl);
				fread_s(&fy, 4, 4, 1, stl);
				fread_s(&fz, 4, 4, 1, stl);
				vrtx1.SetCoords(double(fx), double(fy), double(fz));
				vrtx2.SetCoords(double(fx), double(fy), double(fz));

				Xyz1::verbose = (i == 114248 && j == 2);
				Xyz2::verbose = Xyz1::verbose;

				//if (i == 114248)
				//{
				//	cout << endl;
				//	auto it1 = set1.find(vrtx1);
				//	cout << "set1 size " << set1.size() << ", vertex1 " << vrtx1;
				//	if(it1 == set1.end())
				//		cout << " not found\n";
				//	else
				//		cout << " found: " << *it1 << endl;
				//
				//	auto it2 = set2.find(vrtx2);
				//	cout << "set2 size " << set2.size() << ", vertex2 " << vrtx2;
				//	if (it2 == set2.end())
				//		cout << " not found\n";
				//	else
				//		cout << " found: " << *it2 << endl;
				//}

				//auto it1 = set1.find(vrtx1);
				//auto it2 = set2.find(vrtx2);
				//
				//if(it1 == set1.end())
				//last_pos1 = (it1 == set1.end()) ? set1.insert(last_pos1, vrtx1) : it1;
				//last_pos2 = (it2 == set2.end()) ? set2.insert(last_pos2, vrtx2) : it2;

				/*last_pos1 =*/ set1.insert(vrtx1); // last_pos1, vrtx1);

				if (Xyz1::verbose) cout << endl;

				/*last_pos2 =*/ set2.insert(vrtx2); // last_pos2, vrtx2);


				if (set1.size() != set2.size() && first_time)
				{
					first_time = false;
					cout << " i = " << i << ", j = " << j << endl;
					cout << vrtx1 << "   size " << set1.size() << endl;
					cout << vrtx2 << "   size " << set2.size() << endl;
				}
			}
			// Skipping the trailing 2 bytes
			fread_s(uc, sizeof(uc), 1, 2, stl);

		}
		fclose(stl);

		std::cout << "Position tolerance " << Xyz1::position_tolerance << endl;
		std::cout << "Final number of vertices " << set1.size() << endl << endl;

		std::cout << "Position tolerance " << Xyz2::position_tolerance << endl;
		std::cout << "Final number of vertices " << set2.size() << endl << endl;

		return false;
	}

	catch (exception e)
	{
		std::cout << "Error while reading stl file " << stl_file_path;
		std::cout << e.what();
		if (stl != nullptr)
			fclose(stl);
		vertices.clear();
		triangles.clear();
		vertex_links.clear();
		set1.clear();
		set2.clear();
		return false;
	}
}
#else
// Reading with vertex tolerance: if coords of two vertices differ less than Xyz::position_tolerance,
// they are treated as one vertex (if they both belong to a triangle, such a triangle is removed).
// Class XyzPrecise is an important part of that logic.
// This logic was implemented because some input files have many close vertex pairs that form folds.

bool Surface::ReadTriangles(char* stl_file_path)
{
	FILE* stl = nullptr;
	errno_t errn;
	const unsigned int max_num_triangles = INT32_MAX >> 3;
	try
	{
		errn = fopen_s(&stl, stl_file_path, "rb");
	}
	catch (exception e)
	{
		std::cout << "Error while opening file for read " << stl_file_path << "\n";
		std::cout << e.what();
		return false;
	}

	if (errn != 0 || stl == nullptr)
	{
		std::cout << "File " << stl_file_path << " could not be opened, error code " << errn << "\n";
		return false;
	}
	std::set<Vertex> vertex_set;
	std::map<XyzPrecise, unsigned int> vertex_equivalence_map;

	try
	{
		char uc[80] = { ' ' };
		fread_s(uc, sizeof(uc), 1, 80, stl);
		if (!std::strncmp(uc, "solid ", 6))
		{
			std::cout << "Ascii STL format is detected in " << stl_file_path << endl << endl;
			std::fclose(stl);
			return false;
		}

		uint32_t n;
		fread_s((void*)&n, sizeof(n), 4, 1, stl);
		std::cout << stl_file_path << ": " << n << " triangles\n";

		if (n > max_num_triangles)
		{
			std::cout << "Sorry but that number of triangles is too high. Maximum supported number is " << max_num_triangles << endl;
			return false;
		}

		triangles.resize(n);
		vertices.resize(n);  // There are always fewer vertices than triangles
		vertex_links.resize(4 * n);

		Vertex *pvertex = &vertices[0];
		Xyz tri_center;
		float fx, fy, fz;

		set<Vertex>::iterator last_pos = vertex_set.begin();
		unsigned int last_size = vertex_set.size();
		unsigned int triangle_count = 0;    // In the end may be less than n, if some triangles turn out bad

		for (unsigned int i = 0; i < n; i++)
		{
			// Getting triangle's normal
			Triangle& current_tri = triangles[triangle_count];
			fread_s(&fx, 4, 4, 1, stl);
			fread_s(&fy, 4, 4, 1, stl);
			fread_s(&fz, 4, 4, 1, stl);
			// Normals will be recalculated anyway, so the read vector is ignored

			// Reading the 3 vertices
			tri_center.Initialize();

			for (int j = 0; j < 3; j++)
			{
				pvertex->Initialize();

				fread_s(&fx, 4, 4, 1, stl);
				fread_s(&fy, 4, 4, 1, stl);
				fread_s(&fz, 4, 4, 1, stl);
				pvertex->x = double(fx);
				pvertex->y = double(fy);
				pvertex->z = double(fz);

				unsigned int v_index;

				XyzPrecise xyz1(*pvertex);
				auto res_map = vertex_equivalence_map.find(xyz1);
				if (res_map == vertex_equivalence_map.end())
				{
					// This vertex is new the equivalence map, so it must be new also for the set
					auto res_set = vertex_set.insert(*pvertex);
					last_pos = res_set.first;

					if (vertex_set.size() == last_size)
					{
//@						ASSERT(!res_set.second, "Vertex set size didn't change, insert status should be false but it is not. At size " << vertex_set.size());
						if (!(!res_set.second))
							throw runtime_error("Vertex set size didn't change, insert status should be false but it is not. At size " + std::to_string(vertex_set.size()));

						// This vertex is sticked to a close vertex inserted before
						v_index = last_pos._Ptr->_Myval.vertex_index;
					}
					else
					{
//@						ASSERT(res_set.second, "Vertex set size changed, insert status should be true but it is not. At size " << vertex_set.size());
						if (!(res_set.second))
							throw runtime_error("Vertex set size changed, insert status should be true but it is not. At size " + std::to_string(vertex_set.size()));

//@						ASSERT(vertex_set.size() == last_size + 1, "Wrong size of vertex set " << vertex_set.size() << ", " << (last_size + 1) << " expected");
						if (!(vertex_set.size() == last_size + 1))
							throw runtime_error("Wrong size of vertex set " + std::to_string(vertex_set.size()) + ", " + std::to_string(last_size + 1) + " expected");

						// This vertex is new for both set and map
						v_index = last_size;
						last_pos._Ptr->_Myval.vertex_index = v_index;
						last_size += 1;
						pvertex += 1;
					}

					vertex_equivalence_map.insert({ xyz1, v_index });
				}
				else
					v_index = res_map->second;

				current_tri.vertex[j] = v_index;   // Triangle has indices of the vertices
			}
			// Skipping the trailing 2 bytes
			fread_s(uc, sizeof(uc), 1, 2, stl);

			// Now we decide if this triangle is bad
			if (current_tri.vertex[0] == current_tri.vertex[1] ||
				current_tri.vertex[1] == current_tri.vertex[2] ||
				current_tri.vertex[2] == current_tri.vertex[0])
				continue;

			// Current triangle is to stay. 
			current_tri.CalculateNormalAndCenter(&vertices[0]);

			triangle_count++;
		}
		fclose(stl);

		vertices.resize(vertex_set.size());
		if (triangle_count < n)
		{
			triangles.resize(triangle_count);
			vertex_links.resize(4 * triangle_count);
		}
		std::cout << "Position tolerance " << Xyz::position_tolerance << endl;
		std::cout << "Final number of vertices " << vertex_set.size() << endl;
		std::cout << "Final number of triangles " << triangle_count << endl;
		std::cout << "Rejected triangles  " << (n - triangle_count) << endl;

		vertex_set.clear();
		return true;
	}

	catch (exception e)
	{
		std::cout << "Error while reading stl file " << stl_file_path;
		std::cout << e.what();
		if (stl != nullptr)
			fclose(stl);
		vertices.clear();
		triangles.clear();
		vertex_links.clear();
		vertex_set.clear();
		return false;
	}
}
#endif

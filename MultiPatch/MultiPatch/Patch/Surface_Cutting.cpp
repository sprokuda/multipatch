#include "stdafx.h"
#include <exception>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>

#include "Surface.h"


// Ultimate purpose of this routine is marking triangles to be deleted.
// This is done by first marking deleted vertices (starting from seed vertices marked earlier and propagating through blank triangles 
// without stepping over a marked triangle). Then all blank triangles that have at least one deleted vertex are marked out.
// Before calling this routine some vertices and triangles are already marked (that sets seeds and boundaries of deleted areas).
void Surface::MarkDeletedParts(Triangle::Status tri_stat[], Vertex::Status vert_stat[]) const
{
	//
	// Stage 1. Marking vertices
	// 
	unsigned int* vertex_buffer = (unsigned int*)calloc(vertices.size(), sizeof(unsigned int));
	bool* vertex_processed = (bool*)calloc(vertices.size(), sizeof(bool));    // sets to falses
	unsigned int buffer_fill = 0;
	unsigned int buffer_done = 0;


	// 1. Seeding by the existing'left' vertices
	for (unsigned int v = 0; v < vertices.size(); v++)
		if (vert_stat[v].Out())
		{
			vertex_buffer[buffer_fill++] = v;
			vertex_processed[v] = true;
		}
	//cout << "Seed vertices    " << buffer_fill << endl;


	unsigned int num_out = 0, num_disputed = 0, num_stay = 0;
	// Seeding vertices are in the buffer, now we visit them one-by-one
	do
	{
		auto process_vertex = [vertex_processed, vertex_buffer, &buffer_fill, vert_stat, &num_out, &num_disputed, &num_stay](unsigned int vertex)
		{
			if (!vertex_processed[vertex])
			{
				if (!vert_stat[vertex].Unknown())
					vert_stat[vertex].Out() ? num_out++ : (vert_stat[vertex].Disputed() ? num_disputed++ : num_stay++);

				vert_stat[vertex].MakeOut();
				vertex_buffer[buffer_fill++] = vertex;
				vertex_processed[vertex] = true;
			}
		};

		unsigned int seed = vertex_buffer[buffer_done++];
		int link = vertices[seed].first_link_index;
//@		ASSERT(link >= 0, "No first link for vertex " << seed);
		if (!(link >= 0))
			throw runtime_error("No first link for vertex " + std::to_string(seed));

		unsigned int t = Surface::Link2Triangle(link);    // t is triangle's global index
		unsigned int v = Surface::Link2Vertex(link);  // v is local index (0 - 2), vertex is global index 
		unsigned int first_t = t;

		do
		{
			const Triangle& tri = triangles[t];
//@			ASSERT(tri.vertex[v] == seed, "That trangle's vertex is expected to be the seed vertex");
			if (!(tri.vertex[v] == seed))
				throw runtime_error("That trangle's vertex is expected to be the seed vertex");


			if (tri_stat[t] == Triangle::Status::Unknown)
			{
				unsigned int v1 = tri.vertex[(v + 1) % 3];
				unsigned int v2 = tri.vertex[(v + 2) % 3];

				if (vert_stat[v1].Out())
					process_vertex(v2);
				else if (vert_stat[v2].Out())
					process_vertex(v1);
			}
			t = tri.AdjTriIndx(v);
			v = (tri.AdjTriEdge(v) + 1) % 3;  // this is again seed vertex's local index
		} while (t != first_t);

	} while (buffer_done < buffer_fill);

	//cout << "Vertices marked out " << buffer_fill << endl;

	delete vertex_processed;
	delete vertex_buffer;

	//
	// Stage 2. Marking triangles
	//
	unsigned int k = 0, m = 0;
	for (unsigned int i = 0; i < triangles.size(); i++)
		if (tri_stat[i] == Triangle::Status::Out)
			k++;
		else if (tri_stat[i] == Triangle::Status::Unknown)
		{
			const Triangle& tri = triangles[i];
			if (vert_stat[tri.vertex[0]].Out() ||
				vert_stat[tri.vertex[1]].Out() ||
				vert_stat[tri.vertex[2]].Out())
			{
				tri_stat[i] = Triangle::Status::Out;
				m++;
			}
		}
	//cout << "Marked out " << (k + m) << " triangles (" << k << " while projecting rims and " << m << " after)" << endl;
}

bool Surface::DoesSectionIntersectEdge(const Xyz& a, const Xyz&b, unsigned int tri_index, unsigned int edge, bool& is_v0_on_the_left) const
{
	_ASSERT(edge >= 0 && edge <= 2);
	const Triangle& tri = triangles[tri_index];
	return DoSectionsIntersect(a, b, vertices[tri.vertex[edge]], vertices[tri.vertex[(edge + 1) % 3]], tri.normal, is_v0_on_the_left);
}


// Finds edge of second intersection of section [a b] with the given triangle, first intersection being at entry_edge. 
unsigned int Surface::FindBestEdge(const Xyz& a, const Xyz&b, unsigned int tri_index, unsigned int entry_edge, bool& is_v0_on_the_left) const
// TODO: return max negative dot product (maxprod_negative) for making statistics
{
	const Triangle& tri = triangles[tri_index];
	const unsigned int too_big_edge_index = 7;
	unsigned int edge, maxprod_pos_edge = too_big_edge_index, maxprod_neg_edge = too_big_edge_index, maxprod_edge;
	double maxprod_positive = -DBL_MAX, maxprod_negative = -DBL_MAX, dmin, d1, d2, d3;

	const double tol = 1e-3;
	Vector3d tol_vec(tol, tol, tol);

	for (edge = 0; edge <= 2; edge++)
	{
		if (edge == entry_edge)
			continue;

		const Vertex& v0 = vertices[tri.vertex[edge]];
		const Vertex& v1 = vertices[tri.vertex[(edge + 1) % 3]];

		Vector3d r1 = Vector3d(b, v0);
		Vector3d r2 = Vector3d(v0, a);
		Vector3d r3 = Vector3d(a, v1);
		Vector3d r4 = Vector3d(v1, b);

		Vector3d cp1 = Vector3d::CrossProduct(r1, r2);
		Vector3d cp2 = Vector3d::CrossProduct(r2, r3);
		Vector3d cp3 = Vector3d::CrossProduct(r3, r4);
		Vector3d cp4 = Vector3d::CrossProduct(r4, r1);

		d1 = Vector3d::DotProduct(cp1, cp2);
		d2 = Vector3d::DotProduct(cp2, cp3);
		d3 = Vector3d::DotProduct(cp3, cp4);
		// TODO: maybe d4 is needed?

		if (d1 >= 0 && d2 >= 0 && d3 >= 0)
		{
			if (d1 > maxprod_positive || d2 > maxprod_positive || d3 > maxprod_positive)
			{
				maxprod_positive = max(maxprod_positive, d1);
				maxprod_positive = max(maxprod_positive, d2);
				maxprod_positive = max(maxprod_positive, d3);
				maxprod_pos_edge = edge;
			}
		}
		else
		{
			dmin = min(d1, min(d2, d3));
			if (dmin > maxprod_negative)
			{
				maxprod_negative = dmin;
				maxprod_neg_edge = edge;
			}
		}
	}

	maxprod_edge = maxprod_positive > 0 ? maxprod_pos_edge : maxprod_neg_edge;
	//if (maxprod_negative > 0)
	{
		const Vertex& v0 = vertices[tri.vertex[maxprod_edge]];
		const Vertex& v1 = vertices[tri.vertex[(maxprod_edge + 1) % 3]];
		is_v0_on_the_left = Vector3d::AreVectorsDirectionsClose(Vector3d::CrossProduct(Vector3d(v0, v1), Vector3d(a, b)), tri.normal);
	}

	return maxprod_edge;

	//if (do_they_intersect)
	//	is_v0_on_the_left = Vector3d::AreVectorsDirectionsClose(Vector3d::CrossProduct(Vector3d(v0, v1), Vector3d(a, b)), tri.normal);
}

// This routine marks 'Out' triangles and "left" and "right" vertices of the base surface.
// "Left" means vertices that will be cut away (go) from this surface, and "right" are vertices to stay. 
// As a rule of thumb all triangles crossed by contour go (i.e. do not stay) as well as those to the left of the contour
// when traversing contour along direction of its edges.
// This method is not const because contours use fields in Vertex objects.
void Surface::CutByContour(const ContourOnSurface& cs, Triangle::Status tri_stat[], Vertex::Status vert_stat[]) const
{
	const unsigned int too_big_triangle_index = triangles.size() + 1;
	const unsigned int too_big_edge_index = 7;

	unsigned int n = cs.Count(), i;
	unsigned int curr_tri, next_tri, goal_tri, goal_tri2;
	unsigned int entry_edge = too_big_edge_index, exit_edge = too_big_edge_index;
	bool is_v0_at_left;
	int num_inversion = 0, num_notfound = 0;

	bool verbose = false;

	auto vertex_to_left = [&vert_stat, &verbose, &i](unsigned int idx)
	{
		vert_stat[idx].IncrementLCount();
		if (verbose) cout << "   LEFT <-- vertex " << idx << endl;
	};
	auto vertex_to_left_permanent = [&vert_stat, &verbose](unsigned int idx)
	{
		vert_stat[idx].MakeOut();
		if (verbose) cout << "   LEFT PERM <-- vertex " << idx << endl;
	};
	auto vertex_to_right = [&vert_stat, &verbose](unsigned int idx)
	{
		vert_stat[idx].IncrementRCount();
		if (verbose) cout << "   RIGHT <-- vertex " << idx << endl;
	};

	auto triangle_out = [&tri_stat, &verbose](unsigned int idx)
	{
		tri_stat[idx] = Triangle::Status::Out;
		if (verbose) cout << "   TRI out     ^" << idx << endl;
	};


	for (i = 0; i < n; i++)
	{
		VertexOfContourOnSurface& begin = cs.vertices[i];
		VertexOfContourOnSurface& end = cs.vertices[(i + 1) % n];

		// For the current contour segment, we first determine its start and end ("goal") triangles.
		// Then traverse triangles crossed by this segment between start and goal.

		entry_edge = too_big_edge_index;
		exit_edge = too_big_edge_index;

		//=====================================================
		//          Obtaining starting triangle
		//=====================================================
		if (begin.anchor.type == Anchor::Type::InsideTriangle)
		{
			next_tri = begin.anchor.GetTriangle();
			triangle_out(next_tri);
		}
		else if (begin.anchor.type == Anchor::Type::OverEdge)
		{
			unsigned int tri1, tri2, e1, e2;
			begin.anchor.GetTwoTriangles(*this, tri1, tri2, e1, e2);

			// Instead of testing only one side of the OverEdge pair (the positive dotproduct with normal is the right one), we'll compare the two,
			// the greater one being the right one. Because in some curvy cases they are both positive.

			Vector3d cross1 = Vector3d::CrossProduct(Vector3d(
				vertices[triangles[tri1].vertex[e1]],
				vertices[triangles[tri1].vertex[(e1 + 1) % 3]]),
				Vector3d(begin, end));

			Vector3d cross2 = Vector3d::CrossProduct(Vector3d(
				vertices[triangles[tri2].vertex[e2]],
				vertices[triangles[tri2].vertex[(e2 + 1) % 3]]),
				Vector3d(begin, end));

			next_tri =
				Vector3d::DotProduct(cross1, triangles[tri1].normal) >
				Vector3d::DotProduct(cross2, triangles[tri2].normal) ? tri1 : tri2;

			// Setting entry edge for the first triangle (next_tri)
			entry_edge = (next_tri == tri1) ? e1 : e2;

			curr_tri = Vector3d::AreNearlyCollinearVectorsCodirected(Vector3d::CrossProduct(Vector3d(
				vertices[triangles[tri1].vertex[e1]],
				vertices[triangles[tri1].vertex[(e1 + 1) % 3]]),
				Vector3d(cs.vertices[(i - 1) % n], begin)), triangles[tri1].normal) ?
				tri2 : tri1;

			//if (verbose_local) cout << "   curr_tri " << curr_tri << ",  next_tri " << next_tri << endl;

			if (next_tri != curr_tri)
			{
				// Sorting the two vertices to left and right
				vertex_to_left(triangles[next_tri].vertex[entry_edge]);
				vertex_to_right(triangles[next_tri].vertex[(entry_edge + 1) % 3]);
			}
			triangle_out(tri1);
			triangle_out(tri2);

		}
		else if (begin.anchor.type == Anchor::Type::OverVertex)
		{   // OverVertex
			unsigned int vertex_index = begin.anchor.GetVertex();
			vertex_to_left_permanent(vertex_index);

			const Vertex& vertex_obj = vertices[vertex_index];
			int link;
			if (end.anchor.type == Anchor::Type::OverVertex)
			{
				link = vertex_obj.first_link_index;
				while (link >= 0)
				{
					unsigned int t = Surface::Link2Triangle(link);
					unsigned int v = Surface::Link2Vertex(link);

					if (triangles[t].vertex[(v + 1) % 3] == end.anchor.GetVertex() ||
						triangles[t].vertex[(v + 2) % 3] == end.anchor.GetVertex())
						goto NextContourSegment;

					link = vertex_links[link];
				}
			}

			link = vertex_obj.first_link_index;
			while (link >= 0)
			{
				next_tri = Surface::Link2Triangle(link);
				exit_edge = (Surface::Link2Vertex(link) + 1) % 3;
				if (DoesSectionIntersectEdge(begin, end, next_tri, exit_edge, is_v0_at_left))
					break;
				link = vertex_links[link];
			}
			if (link < 0)
			{
				// Link < 0 means that an intersection was not found. 
				// Therefore the currect segment ends within fan of triangles around this vertex,
				// and we can just proceed to the next segment of input contour on sutface.
				// TODO: put here a check that previous anchor's triangle (or one of the two) contains this vertex , then no messages are necessary.
				// Otherwise report a problem.

				goto NextContourSegment;
			}
		}
		else
		{
//@			ASSERT(false, "BEGIN ANCHOR TYPE NOT SET");
			if (!(false))
				throw runtime_error("BEGIN ANCHOR TYPE NOT SET");
		}
		//if (verbose_local) cout << "   Starting triangle " << next_tri << endl;

		//===============================================
		//          Obtaining goal triangle(s)
		//===============================================
		if (end.anchor.type == Anchor::Type::InsideTriangle)
		{
			goal_tri = end.anchor.GetTriangle();
			goal_tri2 = too_big_triangle_index;
		}
		else if (end.anchor.type == Anchor::Type::OverEdge)
		{
			end.anchor.GetTwoTriangles(*this, goal_tri, goal_tri2);
		}
		else if (end.anchor.type == Anchor::Type::OverVertex)
		{   // OverVertex
			const Vertex& vertex_obj = vertices[end.anchor.GetVertex()];
			int link = vertex_obj.first_link_index;
			while (link >= 0)
			{
				goal_tri = Surface::Link2Triangle(link);
				if (DoesSectionIntersectEdge(begin, end, goal_tri, (Surface::Link2Vertex(link) + 1) % 3, is_v0_at_left))
					break;
				link = vertex_links[link];
			}
			if (link < 0)
			{
				// TODO: put here a check that if previous anchor's triangle (or one of the two) contains this vertex , then no messages are necessary.
				// Otherwise report a problem.
				//if (verbose_local) cout << "  Goal triangle could not be found, going to next contour segment\n";

				goto NextContourSegment;
			}
			goal_tri2 = too_big_triangle_index;
		}
		else
		{
//@			ASSERT(false, "END ANCHOR TYPE NOT SET");
			if (!(false))
				throw runtime_error("END ANCHOR TYPE NOT SET");
		}

		if (verbose)
		{
			cout << "   Goal triangle " << goal_tri;
			if (end.anchor.type == Anchor::Type::OverEdge) cout << "  or  " << goal_tri2;
			cout << endl;
		}

		//===========================================================
		//      Traversing edges from curr_tri to goal_tri
		//===========================================================
		while (next_tri != goal_tri && next_tri != goal_tri2)
		{
			if (verbose) cout << "    ---  " << next_tri << "  " << triangles[next_tri] << endl;
			curr_tri = next_tri;
			if (exit_edge > 2)
			{
				// We should obtain value for exit_edge
				exit_edge = FindBestEdge(begin, end, curr_tri, entry_edge, is_v0_at_left);     // Instead of DoesSectionIntersectEdge

				if (is_v0_at_left)
				{
					//if (verbose)
					{
						cout << "Inverse edge encountered, triangle " << curr_tri << endl;
						// Inverse edge here means that current segment exited current traversed triangle 
						// so that the exit edge of triangle has wrong (inverse) order of vertices.
						// This is not possible if adjacent triangles belong to a single plane or at least their normals almost coincide.
						// So this situation signals about a fold between triangles or contour segments.
						// Exception is thrown because we rely on the invariance of vertex order and can't proceed if it broke.

						cout << "   " << curr_tri << triangles[curr_tri];
						cout << "begin anchor: " << begin.anchor << endl;
						cout << "end anchor: " << end.anchor << endl;
						if (end.anchor.type == Anchor::InsideTriangle)
							cout << "   " << end.anchor.GetTriangle() << triangles[end.anchor.GetTriangle()];

						VertexOfContourOnSurface& afterend = cs.vertices[(i + 2) % n];
						cout << "next after end anchor: " << afterend.anchor << endl;
					}
					throw EdgeInversionException(curr_tri, begin.anchor.projected_vertex, end.anchor.projected_vertex);
				}

//@				ASSERT(!is_v0_at_left, "Incorrect is_v0_at_left when intersecting section with triangle (Find best edge) " << curr_tri);
				if (!(!is_v0_at_left))
					throw runtime_error("Incorrect is_v0_at_left when intersecting section with triangle (Find best edge) " + std::to_string(curr_tri));

				is_v0_at_left ? num_inversion++ : 0;


				if (verbose) cout << "   exit edge " << exit_edge << endl;
//@				ASSERT(exit_edge <= 2, "Second intersection not found for triangle " << curr_tri << ", entry edge " << entry_edge)
				if (!(exit_edge <= 2))
					throw runtime_error("Second intersection not found for triangle " + std::to_string(curr_tri) + ", entry edge " + std::to_string(entry_edge));
			}
			vertex_to_left(triangles[curr_tri].vertex[(exit_edge + 1) % 3]);
			vertex_to_right(triangles[curr_tri].vertex[exit_edge]);
			triangle_out(curr_tri);

			next_tri = triangles[curr_tri].AdjTriIndx(exit_edge);
			entry_edge = triangles[curr_tri].AdjTriEdge(exit_edge);
			exit_edge = too_big_edge_index;
		}
		// End of traversing triangles along the straight section, now next_tri == goal_tri or goal_tri2
		triangle_out(next_tri);

	NextContourSegment:;
	}
	if (verbose)
	{
		cout << "Attempted inversions  " << num_inversion << endl;
		cout << "Missed intersections  " << num_notfound << endl;
	}

	// Marking out triangles around OverVertex vertices and counting anchor types
	unsigned int ni = 0, ne = 0, nv = 0;
	for (i = 0; i < n; i++)
	{
		Anchor::Type type = cs.vertices[i].anchor.type;
		type == Anchor::Type::InsideTriangle ? ni++ : (type == Anchor::Type::OverEdge ? ne++ : nv++);

		if (type != Anchor::Type::OverVertex)
			continue;

		unsigned int iv = cs.vertices[i].anchor.GetVertex();
		const Vertex& vert = this->vertices[iv];
		int link = vert.first_link_index;
//@		ASSERT(link >= 0, "No vertex link for vertex " << iv);
		if (!(link >= 0))
			throw runtime_error("No vertex link for vertex " +std::to_string(iv));

		unsigned int t = Surface::Link2Triangle(link);
		unsigned int v = Surface::Link2Vertex(link);
		unsigned int first_triangle = t;
		do
		{
			triangle_out(t);

			const Triangle& tri = triangles[t];

			v = (v + 2) % 3;

			// v is local index (0, 1, 2) of both triangle's vertex and edge sourcing from it (by convention edges point conterclockwise)
			//cout << "   triangle " << t << tri_stat[t] << endl;
			//cout << "   vertex   " << tri.vertex[v] << vert_stat[tri.vertex[v]] << endl;

//@			ASSERT(tri.HasAdjacentTriangle(v), "No adjacent triangle for " << t << "." << v);
			if (!(tri.HasAdjacentTriangle(v)))
				throw runtime_error("No adjacent triangle for " +std::to_string(t) + "." + std::to_string(v));

			t = tri.AdjTriIndx(v);
			v = tri.AdjTriEdge(v);
		} while (t != first_triangle);
	}
	if (verbose)
		cout << "Anchor types: " << ni << " inside triangle, " << ne << " over edge, " << nv << " over vertex" << endl;
}


void Surface::ShowDisputedVertices(Triangle::Status tri_stat[], Vertex::Status vert_stat[]) const
{
	for (unsigned int i = 0; i < vertices.size(); i++)
		if (vert_stat[i].Disputed())
			TellStatusesAroundFan(i, tri_stat, vert_stat);
}


// Prints statuses of triangles and vertices counterclockwise around the given vertex. 
// All triangles are expected to exist (every triangle must have 3 neighbors), otherwise an assertion will fire.
void Surface::TellStatusesAroundFan(unsigned int ivertex, Triangle::Status tri_stat[], Vertex::Status vert_stat[]) const
{
	const Vertex& vert = this->vertices[ivertex];
	int link = vert.first_link_index;
//@	ASSERT(link >= 0, "No vertex link for vertex " << ivertex);
	if (!(link >= 0))
		throw runtime_error("No vertex link for vertex " + std::to_string(ivertex));

	cout << endl << "Statuses around vertex " << ivertex << vert_stat[ivertex] << endl;
	unsigned int t = Surface::Link2Triangle(link);
	unsigned int v = Surface::Link2Vertex(link);
	unsigned int first_triangle = t;
	do
	{
		const Triangle& tri = triangles[t];
		v = (v + 2) % 3; // v is local index (0, 1, 2) of both triangle's vertex and edge sourcing from it (by convention edges point conterclockwise)
		cout << "   triangle " << t << tri_stat[t] << endl;
		cout << "   vertex   " << tri.vertex[v] << vert_stat[tri.vertex[v]] << endl;
//@		ASSERT(tri.HasAdjacentTriangle(v), "No adjacent triangle for " << t << "." << v);
		if (!(tri.HasAdjacentTriangle(v)))
			throw runtime_error("No adjacent triangle for " + std::to_string(t) + "." + std::to_string(v));

		t = tri.AdjTriIndx(v);
		v = tri.AdjTriEdge(v);
	} while (t != first_triangle);
}

void Surface::AddFanToSet(unsigned int center_vertex, std::set<unsigned int> &tri_indices) const
{
//@	ASSERT(center_vertex < vertices.size(), "Center vertex index " << center_vertex << " out of range " << vertices.size() << endl);
	if (!(center_vertex < vertices.size()))
		throw runtime_error("Center vertex index " + std::to_string(center_vertex) + " out of range " + std::to_string(vertices.size()) + "\n");

	int link = vertices[center_vertex].first_link_index;
	do
	{
		tri_indices.insert(Surface::Link2Triangle(link));
		link = vertex_links[link];
	} while (link >= 0);
}

void Surface::TellAroundTriangle(unsigned int t0) const
{
//@	ASSERT(rims.size() == 0, "function TellAroundTriangle is only designed for closed surfaces");
	if (!(rims.size() == 0))
		throw runtime_error("function TellAroundTriangle is only designed for closed surfaces");

//@	ASSERT(t0 < triangles.size(), "Given triangle index " << t0 << " out of range " << triangles.size() << endl);
	if (!(t0 < triangles.size()))
		throw runtime_error("Given triangle index " + std::to_string(t0) + " out of range " + std::to_string(triangles.size())+ "\n");

	auto tell_triangle = [this](unsigned int t)
	{
		const Triangle& tri = triangles[t];
		cout << "Triangle " << t << ":" <<
			"   (" << tri.vertex[0] << ")  neib.tri " << tri.AdjTriIndx(0) << /*   "." << tri.AdjTriEdge(0) <<  */
			"   (" << tri.vertex[1] << ")  neib.tri " << tri.AdjTriIndx(1) << /*   "." << tri.AdjTriEdge(1) <<  */
			"   (" << tri.vertex[2] << ")  neib.tri " << tri.AdjTriIndx(2) << /*   "." << tri.AdjTriEdge(2) <<  */
			endl;
	};

	auto tell_around_corner = [this, &tell_triangle](unsigned int t0, unsigned int v0)
	{
		cout << "Around corner " << v0 << endl;
		unsigned int vertex0 = triangles[t0].vertex[v0];
		unsigned int v = (v0 + 2) % 3;
		unsigned int t = t0;
		do
		{
			const Triangle& tri = triangles[t];
			cout << "(" << tri.vertex[(v + 1) % 3] << ") ------- (" << tri.vertex[v] << ")" << endl;
			cout << "     ";
			t = tri.AdjTriIndx(v);
			v = (tri.AdjTriEdge(v) + 2) % 3;
			tell_triangle(t);
		} while (t != t0);
	};

	tell_triangle(t0);
	tell_around_corner(t0, 0);
	tell_around_corner(t0, 1);
	tell_around_corner(t0, 2);
}

// DegreeOfVertex returns number of triangles incident to the given vertex
unsigned int Surface::DegreeOfVertex(unsigned int v0) const
{
//@	ASSERT(v0 < vertices.size(), "DegreeOfVertex: vertex index " << v0 << " out of range " << vertices.size() << endl);
	if (!(v0 < vertices.size()))
		throw runtime_error("DegreeOfVertex: vertex index " + std::to_string(v0) + " out of range " + std::to_string(vertices.size()) + "\n");

	unsigned int count = 0;
	int link = vertices[v0].first_link_index;
	while (link >= 0)
	{
		count++;
		link = vertex_links[link];
	}
	return count;
}


#include <algorithm>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <iostream>

using namespace std;
#include "Auxiliary.h"
#include "Xyz.h"
#include "Triangle.h"
#include "Contour.h"
#include "Surface.h"

class SeamTriangle
{
public:
	SeamTriangle() {}
	SeamTriangle(unsigned int v0, bool l0, unsigned int v1, bool l1, unsigned int v2, bool l2)
	{
		vertex[0] = v0;  left_side[0] = l0;
		vertex[1] = v1;  left_side[1] = l1;
		vertex[2] = v2;  left_side[2] = l2;
	}
	unsigned int vertex[3];
	bool left_side[3];
	Vector3d normal;
	Xyz center;
	void CalculateNormal(const Surface& left, const Surface& right)
	{
		const Vertex& v0 = left_side[0] ? left.vertices[vertex[0]] : right.vertices[vertex[0]];
		const Vertex& v1 = left_side[1] ? left.vertices[vertex[1]] : right.vertices[vertex[1]];
		const Vertex& v2 = left_side[2] ? left.vertices[vertex[2]] : right.vertices[vertex[2]];
		Vector3d cross = Vector3d::CrossProduct(Vector3d(v0, v1), Vector3d(v0, v2));
		double d = cross.Norm();
		if (d < 1e-15)
			throw DegenerateTriangleException();
		normal.x = cross.x / d;
		normal.y = cross.y / d;
		normal.z = cross.z / d;
		center.x = (v0.x + v1.x + v2.x) / 3;
		center.y = (v0.y + v1.y + v2.y) / 3;
		center.z = (v0.z + v1.z + v2.z) / 3;
	}

	Xyz GetVertexCoords(unsigned int v, const Surface& left, const Surface& right) const
	{
//@		ASSERT(v >= 0 && v <= 2, "Invalid local vertex index " << v << " (must range 0 to 2)");
		if (!(v >= 0 && v <= 2))
			throw runtime_error("Invalid local vertex index " + std::to_string(v) + " (must range 0 to 2)");

		return (left_side[v] ? left.vertices[vertex[v]] : right.vertices[vertex[v]]);
	}

	void OutputToStlStream(FILE* stl, const Surface& left, const Surface& right, Color clr) const
	{
		float fx, fy, fz;
		fx = (float)normal.x;
		fy = (float)normal.y;
		fz = (float)normal.z;
		fwrite((void*)&fx, 4, 1, stl);
		fwrite((void*)&fy, 4, 1, stl);
		fwrite((void*)&fz, 4, 1, stl);

		// Writing vertices
		Xyz v;
		for (unsigned int j = 0; j < 3; j++)
		{
			v = GetVertexCoords(j, left, right);
			fx = (float)v.x;
			fy = (float)v.y;
			fz = (float)v.z;

			fwrite((void*)&fx, 4, 1, stl);
			fwrite((void*)&fy, 4, 1, stl);
			fwrite((void*)&fz, 4, 1, stl);
		}
		// Writing the trailing 2 bytes
		fwrite(clr.c, 1, 2, stl);
	}
	static bool WriteSeamToFile(char* stl_file_path, char* title, const vector<SeamTriangle> seam, const Surface& l, const Surface& r, Color clr_seam);
};


std::vector<SeamTriangle> Stitch(Surface& left, Surface& right, std::vector<ContourOnSurface> vcs, Vertex::Status r_vert_stat[]);
unsigned int StitchAlongRim(Surface& left, Surface& right, unsigned int vleft, unsigned int vright, std::vector<SeamTriangle>& seam);
bool IsSeamGood(std::vector<SeamTriangle>& seam, unsigned int base, unsigned int n, unsigned int &num_double_folds, unsigned int &num_single_folds);

class PatchedSurface : public  Surface
{
public:
	PatchedSurface(const Surface l, const Surface r, const std::vector<SeamTriangle> s) :
		Surface(l, r, s),
		vertex_offset(l.vertices.size()),
		triangle_offset(l.triangles.size()),
		seam_offset(l.triangles.size() + r.triangles.size())
	{
		cout << "Patched surface created, offsets  " << vertex_offset << "  " << triangle_offset << "  " << seam_offset << endl;
	}

	bool WriteToFile(char* stl_file_path, char* title, Color clr_l, Color clr_r, Color clr_seam) const;
	//{
	//	return Surface::WriteToFile(stl_file_path, title, clr_l);
	//}

	void SmoothPatchedSurface(unsigned int num_it1, unsigned int num_it2);
	void RepairSeam();
	void RefineSeamMesh();

private:
	unsigned int vertex_offset;
	unsigned int triangle_offset;
	unsigned int seam_offset;
};


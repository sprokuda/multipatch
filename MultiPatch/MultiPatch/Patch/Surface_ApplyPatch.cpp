#include "stdafx.h"
#include <exception>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>

#include "Surface.h"
#include "PatchedSurface.h"

//  The most important procedure of this project. Includes upper-scale logic of applying a patch.
PatchedSurface Surface::ApplyPatch(Surface& patch) 
{
	bool verbose = false;					// Set true to see more info 
	bool create_intermediate_files = true;  // Set true to create stl files of seam, cutout of base, adjusted patch, result before smoothing etc.

	unsigned int it1 = 0, it2 = 0;			// To keep track of repeating processing after exceptions of selected types (folds or self-crossing rims)
	std::vector<ContourOnSurface> vcs;

//@	ASSERT(rims.size() == 0, "Surface to be patched must be closed but the supplied one has " << rims.size() << " rim contours.");
	if (!(rims.size() == 0))
		throw runtime_error("Surface to be patched must be closed but the supplied one has " + to_string(rims.size()) + " rim contours.");

//@	ASSERT(patch.rims.size() != 0, "Patch surface must have rims but the supplied patch has none.");
	if (!(patch.rims.size() != 0))
		throw runtime_error("Patch surface must have rims but the supplied patch has none.");

	patch.SmoothRim();
	if (create_intermediate_files)
		patch.WriteToFile("patch_smoothed.stl", "Smoothed patch", Color::LightViolet());

AgainAlloc:
	Triangle::Status* p_tri_stat = new Triangle::Status[triangles.size()];
	Vertex::Status* p_vertex_stat = new Vertex::Status[vertices.size()];
Again:
	for (Triangle::Status* p = p_tri_stat; p < p_tri_stat + triangles.size(); p++)
		*p = Triangle::Status::Unknown;
	for (Vertex::Status* p = p_vertex_stat; p < p_vertex_stat + vertices.size(); p++)
		p->Clear();

	// vcs ("vector of contours on surface") is a collection of projected rims, each rim being a close contour
	vcs.clear();

	try
	{
		cout << endl << "Projecting patch rim contours..." << endl;
		for (const Contour& c : patch.rims)
		{
			ContourOnSurface& cs = patch.ProjectRimContourToSurface(*this, c, verbose);

			cs.HasFolds();
			// NB: Folds of this contour on surface do not always translate to folds of triangles.
			// It happens only when patch is raised (moved upward) to base surface, and folds belong to the rim 
			// contained by the raised part of the patch surface.

			cout << "trying to remove folds... ";
			unsigned int num_folds = cs.RemoveFoldsIfAny();
			cout << num_folds << " removed\n";


			vcs.push_back(cs);
		    
			// Marking vertices and triangles that stay or go after cutting.
			// We remember that contours may be nested (patch may have holes).
			// CutByContour may throw EdgeInversionException if a fold is encountered.

			CutByContour(cs, p_tri_stat, p_vertex_stat);
		}
		cout << "\nLifting portions of patch that are below base...\n";

		bool any_move = patch.EnsureNotBelowBase(*this, vcs);
		// This function uses projections of rims, that's why it is called here, after they are obtained.

		if (create_intermediate_files)
			patch.WriteToFile("patch_adjusted.stl", "Smoothed and base-adjusted patch", Color::LightViolet());

	}
	catch (EdgeInversionException ex) // Most likely a fold was encountered
	{
		it2++;
		cout << "\n\n   *** ERROR: RIM CROSSED A FOLD OR RIM FOLD ENCOUNTERED at base triangle  " << ex.base_triangle <<
			", patch verices " << ex.patch_vertex_1 << ", " << ex.patch_vertex_2 << ".  *** \n\n";
		cout << "Trying to repair base surface and start over\n\n";

		if (TryToRepairTriangle(ex.base_triangle))
		{
			if (create_intermediate_files)
				WriteToFile("base_repaired.stl", "Repaired base surface", Color::SaladGreen());
			goto Again;
		}

		cout << endl << "Could not repair base triangle " << ex.base_triangle << endl;

		// Output of small fragments of base and patch surrounding the problem location
		for (ContourOnSurface& cs : vcs)
			patch.AlterRimVertices(cs);

		std::set<unsigned int> patch_triangles;
		patch.AddFanToSet(ex.patch_vertex_1, patch_triangles);
		patch.AddFanToSet(ex.patch_vertex_2, patch_triangles);
		cout << "Number of triangles in the fragment of patch " << patch_triangles.size() << endl;
		patch.FragmentToFile("patch_fragment_altered.stl", "Fragment of patch, altered", patch_triangles, Color::LightViolet());

		std::set<unsigned int> base_triangles;
		this->AddFanToSet(triangles[ex.base_triangle].vertex[0], base_triangles);
		this->AddFanToSet(triangles[ex.base_triangle].vertex[1], base_triangles);
		this->AddFanToSet(triangles[ex.base_triangle].vertex[2], base_triangles);

		cout << "Number of triangles in base fragment " << base_triangles.size() << endl;
		FragmentToFile("base_fragment.stl", "Fragment of base", base_triangles, Color::SaladGreen());

//		exit(1);
		throw runtime_error("Most likely a fold was encountered");

		// end of output
	}
	catch (std::exception ex)
	{
		cout << "*** Exception ***" << ex.what() << endl;
//		exit(1);
		throw ex;
	}

	cout << endl << "Cutting out base by contours..." << endl;
	MarkDeletedParts(p_tri_stat, p_vertex_stat);

	try
	{
		// Creating base cutout surface
		Surface base(*this, p_tri_stat, std::vector<Triangle>());     // may throw SelfIntersectingRimException

		if (create_intermediate_files)
			base.WriteToFile("base_cutout.stl", "Base cutout", Color::Green());

		// For stitching, it is important rims of patch and base cutout would match
		if (base.rims.size() != patch.rims.size())
		{
			cout << "\n\n   ***  WARNING:  Number of patch'es rim contours (" << patch.rims.size() <<
				" does not match that of base cutout (" << base.rims.size() << ")  *** \n\n";
			cout << "      This error may happen if patch has a small hole or two patch rims come too close to each other\n";
			//exit(1);
		}

		cout << endl << "Stitching..." << endl;
		std::vector<SeamTriangle> seam = Stitch(patch, base, vcs, p_vertex_stat);

		if (create_intermediate_files)
			SeamTriangle::WriteSeamToFile("seam.stl", "Seam", seam, patch, base, Color::Orange());


		cout << endl << "Constructing result surface..." << endl;
		PatchedSurface result(patch, base, seam);
		if (create_intermediate_files)
			result.WriteToFile("result_before_smooth.stl", "Result before smooth", Color::LightViolet(), Color::LightViolet(), Color::SaladGreen());

		const unsigned int num_it1 = 7;  // There is no point to increase this number more, result is the same
		const unsigned int num_it2 = 3;  // Second stage of smoothing may eat from the base volume, so use it lightly
		cout << endl << "Smoothing near seams (" << num_it1 << " / " << num_it2 << " it)..." << endl;

		result.RepairSeam();                       // A repetion of RepairSeam does not actually repair anything
		result.SmoothPatchedSurface(num_it1, num_it2);
		cout << "SmoothPatchedSurface #1" << endl;

		result.RefineSeamMesh();
		cout << "RefineSeamMesh #1" << endl;
		result.SmoothPatchedSurface(num_it1, num_it2);
		cout << "SmoothPatchedSurface #2" << endl;

		delete[] p_tri_stat;
		delete[] p_vertex_stat;

		return result;
	}
	catch (SelfIntersectingRimException ex)
	{
		it1 ++;
		delete[] p_tri_stat;
		delete[] p_vertex_stat;
		cout << "\n\nSelfIntersectingRimException fired at vertex " << ex.vertex << ": refining mesh and starting over\n";
		std::vector<unsigned int> c;
		c.push_back(ex.vertex);
		RefineMesh(c, std::vector<unsigned int>(), true);
		goto AgainAlloc;
	}

}

// Setting coordinates of rim vertices to their projections onto base surface.
// Normals and centers of triangles referencing rim vertices are updated as well.
void Surface::AlterRimVertices(ContourOnSurface& cs)
{
	for (unsigned int i = 0; i < cs.num_vertices; i++)
	{
		VertexOfContourOnSurface& vcs = cs.vertices[i];
		this->vertices[vcs.anchor.projected_vertex].SetCoords(vcs);
	}
	for (unsigned int i = 0; i < cs.num_vertices; i++)
	{
		VertexOfContourOnSurface& vcs = cs.vertices[i];
		unsigned int v0 = vcs.anchor.projected_vertex;
		int link = vertices[v0].first_link_index;
		while (link >= 0)
		{
			unsigned int t = Surface::Link2Triangle(link);
			triangles[t].CalculateNormalAndCenter(&vertices[0]);
			link = vertex_links[link];
		}
	}
}

unsigned int Surface::NearestRimVertex(const ContourOnSurface& cs, unsigned int vertex, unsigned int hint) const
{
	double dmin = DBL_MAX, d;
	unsigned int i_dmin;
	const Vertex& v = vertices[vertex];
	for (int j = ((int)hint) - 10; j < ((int)hint) + 10; j++)
	{
		int i = j % (cs.num_vertices);
		VertexOfContourOnSurface& vcs = cs.vertices[i];
		d = vertices[vcs.anchor.projected_vertex].DistanceTo(v);
		if (d < dmin)
		{
			dmin = d;
			i_dmin = i;
		}
	}
	return i_dmin;
}
unsigned int Surface::NearestRimVertex(const ContourOnSurface& cs, unsigned int vertex) const
{
	double dmin = DBL_MAX, d;
	unsigned int i_dmin;
	const Vertex& v = vertices[vertex];
	for (unsigned int i = 0; i < cs.num_vertices; i++)
	{
		VertexOfContourOnSurface& vcs = cs.vertices[i];
		d = vertices[vcs.anchor.projected_vertex].DistanceTo(v);
		if (d < dmin)
		{
			dmin = d;
			i_dmin = i;
		}
	}
	return i_dmin;
}
double Surface::DistanceToRim(const ContourOnSurface& cs, unsigned int vertex, unsigned int nearest_rim_vertex) const
{
	auto distance_to_segment = [this, &cs, vertex, nearest_rim_vertex](unsigned int next_rim_vertex) -> double
	{
		const Vertex& v = vertices[vertex];
		const VertexOfContourOnSurface& nearest = cs.vertices[nearest_rim_vertex];
		const VertexOfContourOnSurface& next = cs.vertices[next_rim_vertex];
		Vector3d segment(next, nearest);
		Vector3d hypotenuse(next, v);
		double segment_norm = segment.Norm();
		double cathetus = Vector3d::DotProduct(hypotenuse, segment) / segment_norm;

		if (cathetus < 0)
		{
			cout << "cathetus = " << cathetus << ",  nrv " << nearest_rim_vertex << "    ";
			cout << " recounted nrv " << this->NearestRimVertex(cs, vertex) << "    ";
		}
//@		ASSERT(cathetus >= 0, "Not a nearest rim vertex");
		if (!(cathetus >= 0))
			throw runtime_error("Not a nearest rim vertex");

		if (cathetus > segment_norm)
			return -1;
		return sqrt(hypotenuse.Norm2() - cathetus*cathetus);
	};

	double d = distance_to_segment((((int)nearest_rim_vertex) - 1) % (cs.num_vertices));
	if (d >= 0)
		return d;
	d = distance_to_segment((nearest_rim_vertex + 1) % (cs.num_vertices));
	if (d >= 0)
		return d;
	return vertices[vertex].DistanceTo(cs.vertices[nearest_rim_vertex]);
}
void Surface::ReverseRim(unsigned int rim_vertex)
{
	//Vertex& vert = vertices[rim_vertex];
//@	ASSERT(vertices[rim_vertex].GetNextVertex() >= 0, "A rim vertex should be specified");
	if (!(vertices[rim_vertex].GetNextVertex() >= 0))
		throw runtime_error("A rim vertex should be specified");

	unsigned int prev = rim_vertex, v = vertices[prev].GetNextVertex(), next;
	do
	{
		next = vertices[v].GetNextVertex();
		vertices[v].SetNextVertex(prev);
		prev = v;
		v = next;

	} while (next >= 0 && prev != rim_vertex);
//@	ASSERT(next >= 0, "Rim must be closed");
	if (!(next >= 0))
		throw runtime_error("Rim must be closed");
}


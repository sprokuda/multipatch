// Patch.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <time.h>

#include "Surface.h" 
#include "PatchedSurface.h" 

void ProcessSample()
{
	Surface s("sphere.stl");
	s.CheckIntegrity();
	std::vector<unsigned int> c;
	c.push_back(50);
	c.push_back(48);
	s.RefineMesh(c, std::vector<unsigned int>(), true);
	s.WriteToFile("sphere2.stl", "Refined mesh", Color::Blue());
}

void RemoveQuotes(char* buf, unsigned int size)
{
	unsigned int j = 0;
	for (unsigned int i = 0; i < size; i++)
		if (buf[i] != '"' && buf[i] != '\'')
			buf[j++] = buf[i];
}
#if 0

int main(int argc, char* argv[])
{
	//ProcessSample();
	//exit(0);

	char *path_s, *path_p, *path_r;
	char buf_s[256], buf_p[256], buf_r[256];

	if (argc > 1)
		path_s = argv[1];
	else
	{ 
		cout << "Enter path for base surface: ";
		std::cin.getline(buf_s, sizeof(buf_s)); // This works, unlike cin >> buf, if there are spaces in the path, regardless of quotes
		cout << "\n";
		RemoveQuotes(buf_s, sizeof(buf_s));
		path_s = buf_s;
	}

	if(argc > 2)
		path_p = argv[2];
	else
	{
		cout << "Enter path for patch: ";
		std::cin.getline(buf_p, sizeof(buf_p));
		cout << "\n";
		RemoveQuotes(buf_p, sizeof(buf_p));
		path_p = buf_p;
	}

	if(argc > 3)
		path_r = argv[3];
	else
	{
		cout << "Enter path for resulting surface (if exists old contents will be replaced): ";
		std::cin.getline(buf_r, sizeof(buf_r));
		cout << "\n";
		RemoveQuotes(buf_r, sizeof(buf_r));
		path_r = buf_r;
	}

	clock_t before = clock(), before0 = before;

	/**/
	Surface s(path_s);
	//s.CheckIntegrity();
	cout << "\nElapsed time " << float(clock() - before) / float(CLOCKS_PER_SEC) << " sec\n\n"; 
	before = clock();

	cout << endl;

	Surface p(path_p);
	//p.CheckIntegrity();
	cout << "\nElapsed time " << float(clock() - before) / float(CLOCKS_PER_SEC) << " sec\n\n";
	before = clock();

	cout << endl;

	PatchedSurface res = s.ApplyPatch(p); 

	cout << "\nElapsed time " << float(clock() - before) / float(CLOCKS_PER_SEC) << " sec\n\n";
	cout << "\nTotal elapsed time " << float(clock() - before0) / float(CLOCKS_PER_SEC) << " sec\n\n";

	res.WriteToFile(path_r, "Resulting patched surface", Color::Gray(), Color::Gray(), Color::Gray());

	//std::cin >> buf_p;
   exit(0);
}

#endif

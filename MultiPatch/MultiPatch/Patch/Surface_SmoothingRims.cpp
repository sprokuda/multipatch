#include "stdafx.h"
#include <exception>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>

#include "Surface.h"


void Surface::SmoothRim()
{
	const unsigned int max_iter = 7;

	for (unsigned int i = 0; i < max_iter; i++)
	{
		unsigned int old_n1 = triangles.size();
		RemoveRimDents();

		unsigned int old_n2 = triangles.size();
		FillRimIndents();
		if (triangles.size() == old_n1 && old_n1 == old_n2)
			break;
	}
}

void  Surface::RemoveRimDents()
{
	std::set<unsigned int> tri_to_remove;
	int link;

	for (const Contour& c : rims)
	{
		unsigned int i = c.first_vertex;
		unsigned int i_prev = i, i_next;
		do
		{
			const Vertex& v = vertices[i];
			link = v.first_link_index;
//@			ASSERT(link >= 0, "Negative first link of rim vertex " << i);
			if (!(link >= 0))
				throw runtime_error("Negative first link of rim vertex " + std::to_string(i));

			if (vertex_links[link] < 0)  // it means this vertex belongs to exactly 1 triangle
			{
				int tri_index = Surface::Link2Triangle(link);
				tri_to_remove.insert(tri_index);
			}
			else if (vertex_links[vertex_links[link]] < 0 &&     // this vertex belongs to exactly 2 triangles
				i_prev != i)
			{
				// If a sharp angle we'll remove these 2 triangles too 
				i_next = v.GetNextVertex();
				const Vertex& vprev = vertices[i_prev];
				const Vertex& vnext = vertices[i_next];

				Vector3d e1(vprev, v);
				Vector3d e2(v, vnext);

				Vector3d cross_prod = Vector3d::CrossProduct(e1, e2);
				const Triangle& tri = triangles[GetATriangleOfVertex(i)];

				if (Vector3d::AreNearlyCollinearVectorsCodirected(cross_prod, tri.normal) &&
					Vector3d::DotProduct(e1, e2) < 0.5 * e1.Norm() * e2.Norm())
				{
					int tri_index1 = Surface::Link2Triangle(link);
					int tri_index2 = Surface::Link2Triangle(vertex_links[link]);
					tri_to_remove.insert(tri_index1);
					tri_to_remove.insert(tri_index2);
				}
			}

			i_prev = i;
			i = v.GetNextVertex();

		} while (i != c.first_vertex);
	}
	cout << "Erasing dents: " << tri_to_remove.size() << " triangles to remove" << endl;
	RemoveTriangles(tri_to_remove);
}

void Surface::FillRimIndents()
{
	std::vector<Triangle> new_triangles;

	for (const Contour& c : rims)
	{
		unsigned int i_prev = c.first_vertex;
		unsigned int i = vertices[i_prev].GetNextVertex();
		unsigned int i_next;
		do
		{
			const Vertex& v = vertices[i];

			i_next = v.GetNextVertex();
			const Vertex& vprev = vertices[i_prev];
			const Vertex& vnext = vertices[i_next];

			Vector3d e1(vprev, v);
			Vector3d e2(v, vnext);

			double dot_prod = Vector3d::DotProduct(e1, e2);
			Vector3d cross_prod = Vector3d::CrossProduct(e1, e2);
			const Triangle& tri = triangles[GetATriangleOfVertex(i)];

			if (dot_prod < 0 &&
				Vector3d::AreNearlyCollinearVectorsOpposite(cross_prod, tri.normal))
			{
				double d = cross_prod.Norm();
				Xyz center(
					(v.x + vprev.x + vnext.x) / 3,
					(v.y + vprev.y + vnext.y) / 3,
					(v.z + vprev.z + vnext.z) / 3);
				new_triangles.push_back(Triangle(i_prev, i_next, i, Vector3d(-cross_prod.x / d, -cross_prod.y / d, -cross_prod.z / d), center));
				// After filling gap i_prev remains the same
			}
			else
				i_prev = i;

			i = v.GetNextVertex();

		} while (i_prev != c.first_vertex);
	}
	cout << "Filling indents: " << new_triangles.size() << " triangles to add" << endl;
	if (new_triangles.size() > 0)
	{
		triangles.reserve(triangles.size() + new_triangles.size());
		triangles.insert(triangles.end(), new_triangles.begin(), new_triangles.end());
		RebuildConnectivity();
		CheckIntegrity(false);
	}
}


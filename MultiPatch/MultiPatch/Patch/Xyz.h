#pragma once
#include <algorithm>
#include <iostream>
using namespace std;

#include "Auxiliary.h"

class Xyz
{
public:
	double x, y, z;
	Xyz() : x(0), y(0), z(0) {}
	Xyz(double xi, double yi, double zi) : x(xi), y(yi), z(zi) {}
	Xyz(const Xyz& pnt) : x(pnt.x), y(pnt.y), z(pnt.z) {}
	Xyz(const Xyz& pnt, const Xyz& offset) : x(pnt.x - offset.x), y(pnt.y - offset.y), z(pnt.z - offset.z) {}
	inline void Initialize() { x = 0; y = 0; z = 0; }
	void SetCoords(const Xyz& p) { x = p.x;  y = p.y; z = p.z; }
	void SetCoords(double newx, double newy, double newz) { x = newx;  y = newy; z = newz; }
	void OffsetCoords(double dx, double dy, double dz) { x += dx;  y += dy; z += dz; }
	void OffsetCoords(const Xyz& d) { x += d.x;  y += d.y;  z += d.z; }

	double DistanceTo(const Xyz& target) const { return sqrt(pow(x - target.x, 2) + pow(y - target.y, 2) + pow(z - target.z, 2)); }
	double CDistanceTo(const Xyz& target) const { return max(max(abs(x - target.x), abs(y - target.y)), abs(z - target.z)); }
	double DistanceToLine(const Xyz& a, const Xyz& b) const;

	static const double position_tolerance;
	bool operator == (const Xyz& r) const {
		return
			(abs(x - r.x) < position_tolerance &&
				abs(y - r.y) < position_tolerance &&
				abs(z - r.z) < position_tolerance);
	}

	bool operator <  (const Xyz& r) const {
		if (x < r.x - position_tolerance)
			return true;
		if (x > r.x + position_tolerance)
			return false;
		if (y < r.y - position_tolerance)
			return true;
		if (y > r.y + position_tolerance)
			return false;
		return (z < r.z - position_tolerance);
	}
	Xyz operator + (const Xyz& r) const { return Xyz(x + r.x, y + r.y, z + r.z); }
	Xyz operator - (const Xyz& r) const { return Xyz(x - r.x, y - r.y, z - r.z); }
	Xyz operator * (double r) const { return Xyz(x*r, y*r, z*r); }

	friend std::ostream& operator<< (std::ostream &out, const Xyz &xyz) {
		char buf[256];
		sprintf_s(buf, " xyz(%.10f  %.10f  %.10f) ", xyz.x, xyz.y, xyz.z);
		out << buf;
		//out << " xyz(" << xyz.x << "  " << xyz.y << "  " << xyz.z << ")";
		return out;
	};

	bool HasNotANumber() const { return (NotANumber(x) || NotANumber(y) || NotANumber(z)); }


};

class XyzPrecise : public Xyz
{
public:
	XyzPrecise(const Xyz& pnt) : Xyz(pnt) {}
	static bool verbose;
	bool operator == (const Xyz& r) const { return (x == r.x && y == r.y && z == r.z); }
	bool operator <  (const Xyz& r) const { return (x < r.x || (x == r.x && (y < r.y || (y == r.y && z < r.z)))); }
	bool operator >  (const Xyz& r) const { return (x > r.x || (x == r.x && (y > r.y || (y == r.y && z > r.z)))); }
};



class Vertex : public Xyz
{
public:
	int vertex_index;
	int first_link_index;
	int last_link_index;
	Vertex() : first_link_index(-1), last_link_index(-1) {}
	Vertex(Xyz& point) : Vertex() { x = point.x; y = point.y; z = point.z; }

	// Reuse of field 'last_link_index' for defining rim contours
	void SetNextVertex(int vertex) { last_link_index = vertex; }
	int GetNextVertex() const { return last_link_index; }
	bool IsRimVertex() const { return (GetNextVertex() >= 0); }

	friend std::ostream& operator<< (std::ostream &out, const Vertex &xyz) {
		out << " xyz(" << xyz.x << "  " << xyz.y << "  " << xyz.z << ")";
		return out;
	};

	class Status
	{
	public:
		Status() : w(0) {}
		void Clear() { w = 0; }

		inline void IncrementLCount() { w += 256; }
		inline void IncrementRCount() { w += 1; }
		inline void MakeOut() { w = 0x100000; }

		inline unsigned int LCount() const { return w >> 8; }
		inline unsigned int RCount() const { return w & 0xFF; }

		inline bool Unknown() const { return w == 0; }
		inline bool Left() const { return LCount() > RCount(); }
		inline bool Right() const { return LCount() < RCount(); }
		inline bool Disputed() const { return (w > 0 && LCount() == RCount()); }
		inline bool Out() const { return Left(); }
		inline bool Stay() const { return Right(); }

		bool operator == (const Vertex::Status& r) const { return (w == r.w); }

	private:
		unsigned int w;
	};

	friend std::ostream& operator<< (std::ostream &out, const Vertex::Status& s) {
		out << (s.Unknown() ? " - " : (s.Out() ? " L (Out)" : ((s.Stay() ? " R (Stay)" : " ? (Disputed)"))));
		if (!s.Unknown())
			out << "  " << s.LCount() << " - " << s.RCount();
		return out;
	};
};

class Vector3d : public Xyz
{
public:
	Vector3d() {};
	Vector3d(double xi, double yi, double zi) : Xyz(xi, yi, zi) { }
	Vector3d(const Xyz& p1, const Xyz& p2) : Xyz(p2.x - p1.x, p2.y - p1.y, p2.z - p1.z) { }
	Vector3d(const Xyz& p1, const Xyz& p2, float f) : Xyz(f*(p2.x - p1.x), f*(p2.y - p1.y), f*(p2.z - p1.z)) { }


	Vector3d operator + (const Vector3d& r) const { return Vector3d(x + r.x, y + r.y, z + r.z); }
	Vector3d operator - (const Vector3d& r) const { return Vector3d(x - r.x, y - r.y, z - r.z); }
	void operator += (const Vector3d& r) { x += r.x; y += r.y; z += r.z; }
	void operator -= (const Vector3d& r) { x -= r.x; y -= r.y; z -= r.z; }
	Vector3d operator * (double r) const { return Vector3d(x*r, y*r, z*r); }
	Vector3d operator / (double r) const { return Vector3d(x / r, y / r, z / r); }
	void operator *= (double r) { x *= r; y *= r; z *= r; }
	void operator /= (double r) { x /= r; y /= r; z /= r; }

	Vector3d& Normalize()
	{
		double d = Norm();
		if (d > 1e-10)
		{
			x /= d;
			y /= d;
			z /= d;
		}
		return *this;
	}

	static inline double DotProduct(const Vector3d& a, const Vector3d& b) {
		return (a.x * b.x + a.y * b.y + a.z * b.z);
	}
	static inline Vector3d CrossProduct(const Vector3d& a, const Vector3d& b) {
		return Vector3d(
			a.y * b.z - a.z * b.y,
			a.z * b.x - a.x * b.z,
			a.x * b.y - a.y * b.x);
	}

	static inline bool AreCollinearVectorsOpposite(const Vector3d& a, const Vector3d& b) {
		return DotProduct(a, b) < 0;
	}

	static inline bool AreCollinearVectorsCodirected(const Vector3d& a, const Vector3d& b) {
		return DotProduct(a, b) > 0;
	}

	static inline bool AreNearlyCollinearVectorsOpposite(const Vector3d& a, const Vector3d& b) {
		return DotProduct(a, b) < 0;
	}

	static inline bool AreNearlyCollinearVectorsCodirected(const Vector3d& a, const Vector3d& b) {
		return DotProduct(a, b) > 0;
	}

	// Returns true if angle between vectors is less than 90 degrees
	static inline bool AreVectorsDirectionsClose(const Vector3d& a, const Vector3d& b) {
		return DotProduct(a, b) >= 0;
	}

	double inline Norm() const { return sqrt(x*x + y*y + z*z); }

	double inline Norm2() const { return (x*x + y*y + z*z); }

	//TODO: check wether it'll be faster if DotProduct and CrossProduct are inlined
	static double CNorm(const Vector3d v) {
		double absx = (v.x < 0 ? -v.x : v.x);
		double absy = (v.y < 0 ? -v.y : v.y);
		double absz = (v.z < 0 ? -v.z : v.z);
		return (absx > absy ? max(absx, absz) : max(absy, absz));
	}
	friend std::ostream& operator<< (std::ostream &out, const Vector3d &vec) {
		out << " vector(" << vec.x << "  " << vec.y << "  " << vec.z << ")";
		return out;
	}
};

Vector3d PerpendicularFromLine(const Xyz& p, const Xyz& a, const Xyz& b);   // Vector perpendicular from line (ab) to point p

bool IsPointInsideTriangle(const Xyz& point, const Xyz& t0, const Xyz& t1, const Xyz& t2, const Vector3d& normal);

bool DoSectionsIntersect(const Xyz& a, const Xyz& b, const Xyz& v0, const Xyz& v1, const Vector3d normal, bool& is_v0_on_the_left);
bool DoSectionsIntersect(const Xyz& a, const Xyz& b, const Xyz& v0, const Xyz& v1);
bool DoSectionsIntersect_verbose(const Xyz& a, const Xyz& b, const Xyz& v0, const Xyz& v1);

class CrossProductWithTolerance
{
public:
	Vector3d result;
	Vector3d tol;
	CrossProductWithTolerance(Vector3d r1, Vector3d r2, Vector3d t1, Vector3d t2) :
		result(Vector3d::CrossProduct(r1, r2)),
		tol(Vector3d(
			abs(t1.y) * abs(r2.z) + abs(r1.y) * abs(t2.z) + abs(r1.z) * abs(t2.y) + abs(t1.z) * abs(r2.y),
			abs(t1.x) * abs(r2.z) + abs(r1.x) * abs(t2.z) + abs(r1.z) * abs(t2.x) + abs(t1.z) * abs(r2.x),
			abs(t1.y) * abs(r2.x) + abs(r1.y) * abs(t2.x) + abs(r1.x) * abs(t2.y) + abs(t1.x) * abs(r2.y)
		)) {}

	CrossProductWithTolerance(const CrossProductWithTolerance& c1, const CrossProductWithTolerance& c2) :
		CrossProductWithTolerance(c1.result, c2.result, c1.tol, c2.tol) {}

	bool ZeroNorm() { return result.Norm() <= tol.Norm(); }

	friend std::ostream& operator<< (std::ostream &out, const CrossProductWithTolerance &cpt) {
		out << " result" << cpt.result << " +- " << cpt.tol;
		return out;
	};
};

class DotProductWithTolerance
{
public:
	double result;
	double tol;
	DotProductWithTolerance(Vector3d r1, Vector3d r2, Vector3d t1, Vector3d t2) :
		result(Vector3d::DotProduct(r1, r2)),
		tol(
			abs(r1.x) * abs(t2.x) + abs(r2.x) * abs(t1.x) +
			abs(r1.y) * abs(t2.y) + abs(r2.y) * abs(t1.y) +
			abs(r1.z) * abs(t2.z) + abs(r2.z) * abs(t1.z)
		) {}

	DotProductWithTolerance(const CrossProductWithTolerance& c1, const CrossProductWithTolerance& c2) :
		result(Vector3d::DotProduct(c1.result, c2.result)),
		tol(
			abs(c1.result.x) * abs(c2.tol.x) + abs(c2.result.x) * abs(c1.tol.x) +
			abs(c1.result.y) * abs(c2.tol.y) + abs(c2.result.y) * abs(c1.tol.y) +
			abs(c1.result.z) * abs(c2.tol.z) + abs(c2.result.z) * abs(c1.tol.z)
		) {}

	bool operator == (const double& r) const { return (abs(r - result) < tol); }
	bool operator < (const double& r) const { return (result + tol < r); }
	bool operator > (const double& r) const { return (result - tol > r); }
	bool operator <= (const double& r) const { return (result - tol <= r); }
	bool operator >= (const double& r) const { return (result + tol >= r); }

	bool operator == (const DotProductWithTolerance& r) const { return (abs(r.result - result) <(r.tol + tol)); }
	bool operator < (const DotProductWithTolerance& r) const { return (result + tol < r.result - r.tol); }
	bool operator > (const DotProductWithTolerance& r) const { return (result - tol > r.result + r.tol); }
	bool operator <= (const DotProductWithTolerance& r) const { return (result - tol <= r.result + r.tol); }
	bool operator >= (const DotProductWithTolerance& r) const { return (result + tol >= r.result - r.tol); }

	friend std::ostream& operator<< (std::ostream &out, const DotProductWithTolerance &dpt) {
		out << "  " << dpt.result << " +- " << dpt.tol;
		return out;
	};

};

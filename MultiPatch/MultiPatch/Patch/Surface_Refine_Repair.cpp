#include "stdafx.h"
#include <exception>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>

#include "Surface.h"


// This class header is placed in cpp file for the following reasons.
// 1) Class RefinementMode is used in only one procedure (Surface::RefineMesh).
// 2) It has a static member array 'update_table' that can only be placed in cpp file.
// But the contents of this table is intrinsically closely connected with other declarations,
// therefore better readability requires the table and the class definition to be kept together.
//
class RefinementMode  // Defines refinement mode of a triangle
{
public:
#define AllThreeEdges
	RefinementMode(unsigned int edge) : v(edge) {}
	RefinementMode(unsigned int edge1, unsigned int edge2) : v(update_table[edge1][edge2]) {}
	RefinementMode(AllThreeEdges) : v(6) {}

	void Update(unsigned int edge) { v = update_table[v][edge]; }
	void Update(AllThreeEdges) { v = 6; }
	void Update(RefinementMode m) { v = update_table[v][m.v]; }

	bool OneEdge() const { return v <= 2; }
	bool TwoEdges() const { return v >= 3 && v <= 5; }
	bool ThreeEdges() const { return v == 6; }

	unsigned int GetEdge() const { return OneEdge() ? v : (TwoEdges() ? ((v - 2) % 3) : 0); }
	// GetEdge returns local edge index (0, 1, or 2) where new vertex is created, or first of the two consecutive edges (for TwoEdges), or 0 for ThreeEdges. 
	// Formula for TwoEdges derives from this: when v=3, result must be 1 (edges 1,2); when v=4 result 2 (edges 2,0), when v=5 result 0 (edges 0,1)

	friend std::ostream& operator<< (std::ostream &out, const RefinementMode &m) {
		out << m.v;
		return out;
	}

private:

	unsigned int v;  // Means: 0, 1, 2 - new vertex on edge 0, 1, or 2;  3, 4, 5 = new vertex on two edges 1 and 2, 2 and 0, or 0 and 1; 6 - on all three edges.
	static const unsigned int update_table[7][7];
};

// Update table gives resulting refinement mode if two modes are applied to the same triangle
const unsigned int RefinementMode::update_table[7][7] = {
	{ 0, 5, 4, 6, 4, 5, 6 },
	{ 5, 1, 3, 3, 6, 5, 6 },
	{ 4, 3, 2, 3, 4, 6, 6 },
	{ 6, 3, 3, 3, 6, 6, 6 },
	{ 4, 6, 4, 6, 4, 6, 6 },
	{ 5, 5, 6, 6, 6, 5, 6 },
	{ 6, 6, 6, 6, 6, 6, 6 } };


// Locally refine mesh in the vicinity of given vertices and in specified triangles
void Surface::RefineMesh(std::vector<unsigned int> centers, std::vector<unsigned int> tri_faces, bool verbose)
{
	if (verbose)
		cout << "Local refiment around " << centers.size() << " vertices and in " << tri_faces.size() << " triangles" << endl;

	std::map<unsigned int, RefinementMode> refined_triangles;
	auto process_triangle = [&refined_triangles](unsigned int t, RefinementMode m)
	{
		auto ptr = refined_triangles.find(t);
		if (ptr == refined_triangles.end())
		{
			refined_triangles.insert({ t, m });
			//cout << "Inserting new tri " << t << " with mode " << m << endl;
		}
		else
		{
			ptr->second.Update(m);
			//cout << "Updating tri " << t << " with mode " << m << " -> " << ptr->second << endl;
		}
	};

	struct RefinedEdgeInfo
	{
		unsigned int new_vertex;
		RefinedEdgeInfo(unsigned int v) : new_vertex(v) {}
	};

	std::map<std::pair<unsigned int, unsigned int>, RefinedEdgeInfo> refined_edges;
	unsigned int vertex_count = vertices.size();
	auto process_edge = [&refined_edges, &vertex_count](unsigned int v1, unsigned int v2)
	{
		auto e = std::pair<unsigned int, unsigned int>((v1 < v2 ? v1 : v2), (v1 < v2 ? v2 : v1));
		if (refined_edges.find(e) == refined_edges.end())
			refined_edges.emplace(e, RefinedEdgeInfo(vertex_count++));
	};

	// Collecting triangles and edges to be refined: 1) around vertices-centers
	for (unsigned int center : centers)
	{
//@		ASSERT(center < vertices.size(), "Index of refinement center " << center << " is out of range 0 - " << (vertices.size() - 1));
		if (!(center < vertices.size()))
			throw runtime_error("Index of refinement center " + std::to_string(center) + " is out of range 0 - " + std::to_string(vertices.size() - 1));

		int link = vertices[center].first_link_index;
		while (link >= 0)
		{
			unsigned int t = Surface::Link2Triangle(link);
			unsigned int v = Surface::Link2Vertex(link);
			process_triangle(t, RefinementMode(AllThreeEdges));

			Triangle& tri = triangles[t];

			unsigned int v1 = (v + 1) % 3;
			unsigned int v2 = (v + 2) % 3;

			process_edge(center, tri.vertex[v1]);
			process_edge(tri.vertex[v1], tri.vertex[v2]);
			if (!tri.HasAdjacentTriangle(v2)) // sic! because if that triangle exists, the edge is added from that triangle
				process_edge(center, tri.vertex[v2]);

			if (tri.HasAdjacentTriangle((v + 1) % 3))
			{
				t = tri.AdjTriIndx((v + 1) % 3);
				unsigned int e = tri.AdjTriEdge((v + 1) % 3);
				process_triangle(t, RefinementMode(e));
			}

			link = vertex_links[link];
		} // while
	}

	// Collecting triangles and edges to be refined: 2) individually specified triangles
	for (unsigned int i : tri_faces)
	{
//@		ASSERT(i < triangles.size(), "Index of refinement face " << i << " is out of range 0 - " << (triangles.size() - 1));
		if (!(i < triangles.size()))
			throw runtime_error("Index of refinement face " + std::to_string(i) + " is out of range 0 - " + std::to_string(triangles.size() - 1));

		process_triangle(i, RefinementMode(AllThreeEdges));

		Triangle& tri = triangles[i];

		for (unsigned int j = 0; j <= 2; j++)
		{
			process_edge(tri.vertex[j], tri.vertex[(j + 1) % 3]);
			if (tri.HasAdjacentTriangle(j))
			{
				unsigned int t = tri.AdjTriIndx(j);
				unsigned int e = tri.AdjTriEdge(j);
				process_triangle(t, RefinementMode(e));
			}
		}
	}

	if (verbose)
		cout << "Number of refined triangles " << refined_triangles.size() << ", edges " << refined_edges.size() <<
		"; " << (vertex_count - vertices.size()) << " added edge vertices" << endl;

	// Now only those new vertices are counted that should be created on old edges. 
	// We have to add vertices that should appear inside triangles with 2 new vertices on edges (RefinementMode 3, 4, 5).
	// And we need to count how many triangles are being added (taking into account that old triangles objects are reused).
	unsigned int new_internal_vertex_count = 0;
	unsigned int new_tri_count = triangles.size();

	for (auto &rf : refined_triangles)
		if (rf.second.ThreeEdges())
			new_tri_count += 3; // 4 new triangles
		else if (rf.second.OneEdge())
			new_tri_count += 1;  // 2 new triangles
		else // if (rf.second.TwoEdges())
		{
			new_tri_count += 4;
			new_internal_vertex_count++;
		}

	if (verbose)
		cout << "New internal vertex count " << new_internal_vertex_count << endl;

	// Creating new vertices only at edges; internal vertices are created later
	vertices.resize(
		vertex_count +
		new_internal_vertex_count);
	if (verbose)
		cout << "New total number of vertices " << (vertex_count + new_internal_vertex_count) << endl;

	for (auto &re : refined_edges)
	{
		RefinedEdgeInfo& info = re.second;
		Vertex& v1 = vertices[re.first.first];
		Vertex& v2 = vertices[re.first.second];
		vertices[info.new_vertex].SetCoords((v1.x + v2.x) / 2, (v1.y + v2.y) / 2, (v1.z + v2.z) / 2);

		// Offset coords...
		/*
		Vector3d mean_normal1 = MeanNormalAtVertex(re.first.first);
		double sinus = MeanRelativeDeltaAroundVertex(re.first.first, mean_normal1);
		double shift = -0.5 * v1.DistanceTo(v2) * (abs(sinus) < 0.1 ? (sinus / sqrt(1 - sinus * sinus)) : ((1 - sqrt(1 - sinus * sinus)) / sinus));
		//cout << j_mindist << "  "<< shift << "    "  << sinus << endl;
		Vector3d mean_normal2 = MeanNormalAtVertex(re.first.second);
		Vector3d mean_normal(0.5 * (mean_normal1.x + mean_normal2.x), 0.5 * (mean_normal1.y + mean_normal2.y), 0.5 * (mean_normal1.z + mean_normal2.z));
		vertices[info.new_vertex].OffsetCoords(mean_normal * shift);
		*/
	}
	// If there are any triangles with TwoEdges refinement mode, there will be a new vertex inside each. We'll create those vertices totgether with new triangles.

	// Creating triangles
	// After this procedure all secondary connectivity information (adjacent triangles, vertex links) is invalidated. 
	// But it remains valid for each refined triangle until it is replaced by one of its child triangles. 
	// Primary connectivity information - shared triangle vertices - will be updated and kept valid.

	auto edge_2_new_vertex = [&refined_edges](unsigned int v1, unsigned int v2) -> unsigned int
	{
		auto p = std::pair<unsigned int, unsigned int>((v1 < v2 ? v1 : v2), (v1 < v2 ? v2 : v1));
		auto ptr = refined_edges.find(p);
//@		ASSERT(ptr != refined_edges.end(), "Refined edge between vertices " << v1 << " and " << v2 << " not found");
		if (!(ptr != refined_edges.end()))
			throw runtime_error("Refined edge between vertices " + std::to_string(v1) + " and " + std::to_string(v2) + " not found");

		return ptr->second.new_vertex;
	};

	unsigned int old_tri_count = triangles.size();
	triangles.resize(new_tri_count);
	if (verbose)
		cout << "New total number of triangles " << new_tri_count << endl;
	unsigned int new_t_index = old_tri_count;


	// Now, actually creating triangles

	const Vertex* p = &(vertices[0]);

	for (auto &rt : refined_triangles)
	{
		Triangle& parent_tri = triangles[rt.first];
		RefinementMode m = rt.second;
		unsigned int e = m.GetEdge();

		unsigned int v1 = parent_tri.vertex[e];
		unsigned int v2 = parent_tri.vertex[(e + 1) % 3];
		unsigned int v3 = parent_tri.vertex[(e + 2) % 3];

		if (m.OneEdge())
		{
			// One new vertex
			unsigned int v0 = edge_2_new_vertex(v1, v2);

			// One new triangle
			triangles[new_t_index++].SetVertices(v0, v2, v3, p);

			// Reusing old triangle
			parent_tri.SetVertices(v1, v0, v3, p);
		}
		else if (m.ThreeEdges())
		{
			// Three new vertices
			unsigned int v4 = edge_2_new_vertex(v1, v3);
			unsigned int v5 = edge_2_new_vertex(v1, v2);
			unsigned int v6 = edge_2_new_vertex(v2, v3);

			// Three new triangles
			triangles[new_t_index++].SetVertices(v1, v5, v4, p);
			triangles[new_t_index++].SetVertices(v5, v2, v6, p);
			triangles[new_t_index++].SetVertices(v4, v6, v3, p);

			// Reusing old triangle
			parent_tri.SetVertices(v4, v5, v6, p);
		}
		else // i.e. if (m.TwoEdges())
		{
			unsigned int v4 = edge_2_new_vertex(v1, v2);
			unsigned int v5 = edge_2_new_vertex(v2, v3);
			unsigned int v0 = vertex_count;
			vertices[vertex_count].SetCoords(
				(vertices[v1].x + vertices[v4].x + vertices[v5].x + vertices[v3].x) / 4,
				(vertices[v1].y + vertices[v4].y + vertices[v5].y + vertices[v3].y) / 4,
				(vertices[v1].z + vertices[v4].z + vertices[v5].z + vertices[v3].z) / 4);

			// Offset along normal at this point can only be done if essential vertex data (mean normal and mean relative dalta)
			// were calculated before reusing old triangles. 
			/*
			Vector3d mean_normal1 = MeanNormalAtVertex(v1);
			ASSERT(!mean_normal1.HasNotANumber(), "Nan's in mean normal");

			double sinus = MeanRelativeDeltaAroundVertex(v1, mean_normal1);
			ASSERT(!NotANumber(sinus), "Calculated sinus is Nan");

			double shift = -vertices[v1].DistanceTo(vertices[vertex_count]) *
			(abs(sinus) < 0.1 ? (sinus / sqrt(1 - sinus * sinus)) : ((1 - sqrt(1 - sinus * sinus)) / sinus));
			ASSERT(!NotANumber(shift), "Calculated vertex shift is Nan");

			ASSERT(!parent_tri.normal.HasNotANumber(), "Nan's in parent triangle's normal");
			vertices[vertex_count].OffsetCoords(parent_tri.normal * shift);
			*/

			vertex_count++;

			//cout << v0 << vertices[v0];

			// One old triangle
			parent_tri.SetVertices(v1, v0, v3, p);

			// Four new triangles
			triangles[new_t_index++].SetVertices(v1, v4, v0, p);
			triangles[new_t_index++].SetVertices(v0, v5, v3, p);
			if (vertices[v4].DistanceTo(vertices[v5]) < vertices[v0].DistanceTo(vertices[v2]))
			{
				triangles[new_t_index++].SetVertices(v0, v4, v5, p);
				triangles[new_t_index++].SetVertices(v5, v4, v2, p);
			}
			else
			{
				triangles[new_t_index++].SetVertices(v0, v4, v2, p);
				triangles[new_t_index++].SetVertices(v2, v5, v0, p);
			}
		}
	} // loop over refined triangles

//@	ASSERT(vertex_count == vertices.size(), "Incorrect vertex counter after refinement, " << vertex_count << " vs expected " << vertices.size());
	if (!(vertex_count == vertices.size()))
		throw runtime_error("Incorrect vertex counter after refinement, " + std::to_string(vertex_count) + " vs expected " + std::to_string(vertices.size()));

//@	ASSERT(new_t_index == triangles.size(), "Incorrect triangle counter after refinement, " << new_t_index << " vs expected " << triangles.size());
	if (!(new_t_index == triangles.size()))
		throw runtime_error("Incorrect triangle counter after refinement, " + std::to_string(new_t_index) + " vs expected " + std::to_string(triangles.size()));

	// Releasing resources
	refined_edges.clear();
	refined_triangles.clear();

	RebuildConnectivity();
	CheckIntegrity(verbose);
}


bool Surface::TryToRepairTriangle(unsigned int tri_index)
{
	if (IsBadTriangle(tri_index))
		return RepairTriangle(tri_index);

	for (int j = 0; j < 3; j++)
		if (triangles[tri_index].HasAdjacentTriangle(j))
		{
			unsigned int t = triangles[tri_index].AdjTriIndx(j);
			if (IsBadTriangle(t))
				return RepairTriangle(t);
		}
	return false;
}

// Triangle is bad if its normal rather opposes than coincides with its neighbors normals 
bool Surface::IsBadTriangle(unsigned int tri_index) const
{
	bool is_bad = true;
	for (int j = 0; j < 3; j++)
		if (triangles[tri_index].HasAdjacentTriangle(j))
			is_bad &=
			Vector3d::AreNearlyCollinearVectorsOpposite(
				triangles[tri_index].normal,
				triangles[triangles[tri_index].AdjTriIndx(j)].normal);
	return is_bad;
}

// It is taken for granted that the triangle is bad (it is not checked)
// Returns true if changes are made: two neighbor triangles are "flipped" - turned within their common 4 vertices.
// This routine does not change anyting and returns false if
//    1) a neighbor triangle faced towards the given one is not found;
//    2) one of the two vertices shared by the neighbors has degree less than 4 (because degree of both these vertices is decreased when flipping,
// and a degree 3 would become 2, which means a triangle fold).
bool Surface::RepairTriangle(unsigned int tri_index)
{
	bool verbose = false;
	Triangle& tri = triangles[tri_index];

	// Measuring how bad this triangle is. When trying to repair, that measure should not worsen.
	// Let's the measure be minimum of dot products of normals of this and neighbor triangles.
	double how_bad = 1.0;
	for (unsigned int j = 0; j < 3; j++)
		if (tri.HasAdjacentTriangle(j))
			how_bad = min(how_bad, Vector3d::DotProduct(tri.normal, triangles[tri.AdjTriIndx(j)].normal));

	// We look for neighbor triangle that is faced towards the bad one.
	// Criterion is that vector (neighbor center, opposite vertex) is codirected with neighbor's normal.
	// Then we replace those 2 triangles with the other two triangles on the same 4 vertices.
	for (unsigned int j = 0; j < 3; j++)
	{
		if (tri.HasAdjacentTriangle(j))
		{
			unsigned int t = tri.AdjTriIndx(j);
			unsigned int e = tri.AdjTriEdge(j);
			Triangle& nbr = triangles[t];
			if (Vector3d::AreNearlyCollinearVectorsCodirected(
				Vector3d(nbr.center, vertices[tri.vertex[(j + 2) % 3]]),
				nbr.normal))
			{
//@				ASSERT(tri.vertex[j] == nbr.vertex[(e + 1) % 3], "Corner vertex mismatch");
				if (!(tri.vertex[j] == nbr.vertex[(e + 1) % 3]))
					throw runtime_error("Corner vertex mismatch");

//@				ASSERT(tri.vertex[(j + 1) % 3] == nbr.vertex[e], "Corner vertex mismatch");
				if (!(tri.vertex[(j + 1) % 3] == nbr.vertex[e]))
					throw runtime_error("Corner vertex mismatch");

				unsigned int v1 = tri.vertex[(j + 2) % 3];
				unsigned int v2 = tri.vertex[j];
				unsigned int v3 = tri.vertex[(j + 1) % 3];
				unsigned int v4 = nbr.vertex[(e + 2) % 3];

				// Triangles (v1, v2, v3) and (v3, v2, v4) are transformed into (v1, v2, v4) and (v3, v1, v4).
				// But only if degrees of v2, v3 are greater than 3.

				if (vertices[v1].DistanceTo(vertices[v4]) < 1e-15)
				{
					cout << " Triangle fold detected " <<
						tri_index << " [" << tri.vertex[0] << " " << tri.vertex[1] << " " << tri.vertex[2] << "]    " <<
						t << " [" << nbr.vertex[0] << " " << nbr.vertex[1] << " " << nbr.vertex[2] << "]    " << " \n";

					throw TriangleFoldWithCoincidentVertices(tri_index, t, v1, v4);
				}

				if (DegreeOfVertex(v2) < 4 || DegreeOfVertex(v3) < 4)
					continue;

				// Checking if the repair will reduce badness
				double badness_of_repair = 1.0;

				auto normal_of_would_be_triangle = [this](unsigned int v0, unsigned int v1, unsigned int v2) -> Vector3d
				{
					Vector3d normal = Vector3d::CrossProduct(Vector3d(vertices[v0], vertices[v1]), Vector3d(vertices[v0], vertices[v2]));
					double d = normal.Norm();
					if (d < 1e-15)
						throw DegenerateTriangleException();
					normal.x /= d;
					normal.y /= d;
					normal.z /= d;

//@					ASSERT(abs(normal.Norm() - 1.0) < 1e-4, "Non-unit norm " << normal.Norm() << " of would-be triangle");
					if (!(abs(normal.Norm() - 1.0) < 1e-4))
						throw runtime_error("Non-unit norm " + std::to_string(normal.Norm()) + " of would-be triangle");

					return normal;
				};

				try
				{
					Vector3d normal1 = normal_of_would_be_triangle(v1, v2, v4);
					Vector3d normal2 = normal_of_would_be_triangle(v3, v1, v4);
					badness_of_repair = min(badness_of_repair, Vector3d::DotProduct(normal1, normal2));

					if (tri.HasAdjacentTriangle((j + 1) % 3))
					{
						unsigned int t1 = tri.AdjTriIndx((j + 1) % 3);
						badness_of_repair = min(badness_of_repair, Vector3d::DotProduct(normal2, triangles[t1].normal));
					}

					if (tri.HasAdjacentTriangle((j + 2) % 3))
					{
						unsigned int t1 = tri.AdjTriIndx((j + 2) % 3);
						badness_of_repair = min(badness_of_repair, Vector3d::DotProduct(normal1, triangles[t1].normal));
					}

					if (nbr.HasAdjacentTriangle((e + 1) % 3))
					{
						unsigned int t1 = nbr.AdjTriIndx((e + 1) % 3);
						badness_of_repair = min(badness_of_repair, Vector3d::DotProduct(normal1, triangles[t1].normal));
					}

					if (nbr.HasAdjacentTriangle((e + 2) % 3))
					{
						unsigned int t1 = nbr.AdjTriIndx((e + 2) % 3);
						badness_of_repair = min(badness_of_repair, Vector3d::DotProduct(normal2, triangles[t1].normal));
					}
					if (verbose)
						cout << "Quality of possible repair " << badness_of_repair << ", original " << how_bad << endl;

					if (badness_of_repair < how_bad || badness_of_repair < 0)
						goto NextNeighbor;
				}
				catch (DegenerateTriangleException ex)
				{
					badness_of_repair = -100;
					goto NextNeighbor;
				}

				// Modifying the two triangles and their neighbors

				tri.vertex[(j + 1) % 3] = v4;
				nbr.vertex[(e + 1) % 3] = v1;

				tri.adjacent_triangle[j] = nbr.adjacent_triangle[(e + 1) % 3];
				if (nbr.HasAdjacentTriangle((e + 1) % 3))
					triangles[nbr.AdjTriIndx((e + 1) % 3)].adjacent_triangle[nbr.AdjTriEdge((e + 1) % 3)] = Triangle::AdjacentCode(tri_index, j);

				nbr.adjacent_triangle[e] = tri.adjacent_triangle[(j + 1) % 3];
				if (tri.HasAdjacentTriangle((j + 1) % 3))
					triangles[tri.AdjTriIndx((j + 1) % 3)].adjacent_triangle[tri.AdjTriEdge((j + 1) % 3)] = Triangle::AdjacentCode(t, e);

				nbr.adjacent_triangle[(e + 1) % 3] = Triangle::AdjacentCode(tri_index, (j + 1) % 3);
				tri.adjacent_triangle[(j + 1) % 3] = Triangle::AdjacentCode(t, (e + 1) % 3);

				// Updating links!
				auto remove_link_from_vertex = [this](int target_link, unsigned int target_vertex)
				{
					int lnk = vertices[target_vertex].first_link_index;
					if (lnk == target_link)
						vertices[target_vertex].first_link_index = vertex_links[target_link];
					else
					{
						while (vertex_links[lnk] != target_link)
						{
							lnk = vertex_links[lnk];
//@							ASSERT(lnk >= 0, "Negative link");
							if (!(lnk >= 0))
								throw runtime_error("Negative link");
						}
						vertex_links[lnk] = vertex_links[target_link];
					}
				};
				auto insert_link_to_vertex = [this](int target_link, unsigned int target_vertex)
				{
					vertex_links[target_link] = vertices[target_vertex].first_link_index;
					vertices[target_vertex].first_link_index = target_link;
				};

				int link_tri = Triangle::AdjacentCode(tri_index, (j + 1) % 3);  // to be transfered from v3 to v4
				int link_nbr = Triangle::AdjacentCode(t, (e + 1) % 3);          // to be transfered from v2 to v1

				remove_link_from_vertex(link_tri, v3);
				insert_link_to_vertex(link_tri, v4);

				remove_link_from_vertex(link_nbr, v2);
				insert_link_to_vertex(link_nbr, v1);

				tri.CalculateNormalAndCenter(&(vertices[0]));
				nbr.CalculateNormalAndCenter(&(vertices[0]));

				CheckIntegrity(false);

				return true;
			} // if (triangles tri and nbr are faced towards each other)
		}
	NextNeighbor:;

	} // Loop over tri's edges
	return false;
}

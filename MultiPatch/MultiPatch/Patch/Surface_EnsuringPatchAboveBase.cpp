#include "stdafx.h"
#include <exception>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>

#include "Surface.h"


// For every vertex of 'this' surface (which is supposed to be a patch patch) 
// a check is performed whether it is located below the outer side of the given base 
// (outer side is formed by triangles with  part of the given surface where normals are not directed against normal of 
// 'this' surface at the considered vertex).
// If the vertex is lower, it is shifted along normal of an appropriate base's triangle towards projection of that vertex on that triangle.
bool Surface::EnsureNotBelowBase(const Surface& base, const vector<ContourOnSurface> cs_vec)
{
	bool verbose = true;

	auto is_point_below_facet = [base](Xyz& point, unsigned int base_tri_index) -> bool
	{
		return (Vector3d::DotProduct(Vector3d(base.triangles[base_tri_index].center, point), base.triangles[base_tri_index].normal) < 0);
	};
	auto find_rim_triangle = [this](unsigned int vcurr, unsigned int& v_offrim) -> unsigned int
	{
//@		ASSERT(vcurr < vertices.size(), "Current rim vertex " << vcurr << " out of range " << vertices.size());
		if (!(vcurr < vertices.size()))
			throw runtime_error("Current rim vertex " + std::to_string(vcurr) + " out of range " + std::to_string(vertices.size()));

//@		ASSERT(vertices[vcurr].GetNextVertex() >= 0, "Vertex " << vcurr << " does not belong to rim");
		if (!(vertices[vcurr].GetNextVertex() >= 0))
			throw runtime_error("Vertex " + std::to_string(vcurr) + " does not belong to rim");

		unsigned int vnext = vertices[vcurr].GetNextVertex();
		int link = vertices[vcurr].first_link_index;
		while (link >= 0)
		{
			unsigned int t = Surface::Link2Triangle(link);
			unsigned int v = Surface::Link2Vertex(link);
			if (triangles[t].vertex[(v + 1) % 3] == vnext)
			{
//@				ASSERT(triangles[t].adjacent_triangle[v] == -1,
//@					"Triangle having vertices " << vcurr << " and " << vnext << " should be rim triangle");
				if (!(triangles[t].adjacent_triangle[v] == -1))
					throw runtime_error("Triangle having vertices " + std::to_string(vcurr) + " and " + std::to_string(vnext) + " should be rim triangle");

				v_offrim = triangles[t].vertex[(v + 2) % 3]; // this is third vertex of this triangle (may be belonging to rim or not)
				return t;
			}
			link = vertex_links[link];
		}
		// Triangle not found...
//@		ASSERT(false, "No triangle having both vertices " << vcurr << " and " << vnext << " could be found");
		if (!(false))
			throw runtime_error("No triangle having both vertices " + std::to_string(vcurr) + " and " + std::to_string(vnext) + " could be found");
	};

	int *base_triangle_of_vertex_projection = new int[vertices.size()];
	for (unsigned int i = 0; i < vertices.size(); i++)
		base_triangle_of_vertex_projection[i] = -1;  // -1 means the vertex is not processed yet

	auto is_vertex_processed = [base_triangle_of_vertex_projection](unsigned int v) -> bool
	{
		return (base_triangle_of_vertex_projection[v] >= 0);
	};

	vector<unsigned int> moved; // actually moved patch vertices
	double max_move = 0;

	auto process_position = [this, base, base_triangle_of_vertex_projection, &is_vertex_processed, &is_point_below_facet, &max_move, &moved]
	(unsigned int v, unsigned int base_tri, const Xyz& proj)
	{
//@		ASSERT(v < vertices.size(), "Vertex index " << v << " out of range 0 - " << (vertices.size() - 1));
		if (!(v < vertices.size()))
			throw runtime_error("Vertex index " +std::to_string(v) + " out of range 0 - " + std::to_string(vertices.size() - 1));

		if (is_vertex_processed(v))
			return;

		base_triangle_of_vertex_projection[v] = base_tri;

		if (is_point_below_facet(vertices[v], base_tri))
		{
			double d = vertices[v].DistanceTo(proj);
			max_move = max(max_move, d);
			vertices[v].SetCoords(proj);
			moved.push_back(v);
		}
	};

	bool *is_triangle_processed = new bool[triangles.size()];
	for (unsigned int i = 0; i < triangles.size(); i++)
		is_triangle_processed[i] = false;
	unsigned int *processed_triangles = new unsigned int[triangles.size()];
	unsigned int processed_triangle_count = 0;
	unsigned int seed_index = 0;
	auto add_processed_triangle = [this, is_triangle_processed, processed_triangles, &processed_triangle_count](unsigned int t)
	{
//@		ASSERT(t < triangles.size(), "Triangle index " << t << " out of range " << triangles.size());
		if (!(t < triangles.size()))
			throw runtime_error("Triangle index " + std::to_string(t) + " out of range " + std::to_string(triangles.size()));

//@		ASSERT(processed_triangle_count < triangles.size(), "Processed triangle count " << processed_triangle_count << " out of range " << triangles.size());
		if (!(processed_triangle_count < triangles.size()))
			throw runtime_error("Processed triangle count " + std::to_string(processed_triangle_count) + " out of range " + std::to_string(triangles.size()));

		if (!is_triangle_processed[t])
		{
			processed_triangles[processed_triangle_count++] = t;
			is_triangle_processed[t] = true;
		}
	};

	auto process_rim_vertex = [](VertexOfContourOnSurface& vcs)
	{
	};
	auto process_internal_vertex = [this, &base, &is_vertex_processed, &process_position](unsigned int v, unsigned int t_base_hint)
	{
		if (!is_vertex_processed(v))
		{
			VertexOfContourOnSurface proj;
			unsigned int t_base_refined = base.RefineClosestTriangle_L2DistanceToCenter(vertices[v], t_base_hint); // Never ever skip this call!
			unsigned int t_base_more_refined;
			(void)base.RefineClosestTriangle_ProjDistanceToEdge(vertices[v], t_base_refined, t_base_more_refined, &proj);
			process_position(v, t_base_more_refined, proj);
		}
	};

	// Step 1: processing triangles along all rims (better process them all before internal triangles, because some rims make holes
	// and if we process internals before them, we would not use already existing projections.
	for (const ContourOnSurface& cs : cs_vec)
	{
		if (verbose)
			cout << "   processing contour of " << cs.num_vertices << " vertices";
		for (unsigned int cs_vertex_index = 0; cs_vertex_index < cs.num_vertices; cs_vertex_index++)
		{
			const VertexOfContourOnSurface& vcs = cs.vertices[cs_vertex_index];
			unsigned int v_patch = vcs.anchor.projected_vertex;
			unsigned int v_patch_offrim;
			unsigned int t_patch = find_rim_triangle(v_patch, v_patch_offrim);
			if (t_patch == 28583)
			{
				cout << "Patch triangle " << t_patch << ", rime vertex " << v_patch << ", offrim vertex " << v_patch_offrim << endl;
			}

			// Processing rim vertex
			unsigned int t_base = vcs.anchor.GetATriangle(base);
			process_position(v_patch, t_base, vcs);

			// Processing offrim vertex
			if (!(vertices[v_patch_offrim].IsRimVertex()))
				process_internal_vertex(v_patch_offrim, t_base);

			// Let's make sure the next vertex along cs is correct
//@			ASSERT(cs.vertices[(cs_vertex_index + 1) % (cs.num_vertices)].anchor.projected_vertex == vertices[v_patch].GetNextVertex(),
//@				"Next vertex mismatch at vertex " << cs_vertex_index << " of " << cs.num_vertices);
			if (!(cs.vertices[(cs_vertex_index + 1) % (cs.num_vertices)].anchor.projected_vertex == vertices[v_patch].GetNextVertex()))
				throw runtime_error("Next vertex mismatch at vertex " + std::to_string(cs_vertex_index) + " of " +std::to_string(cs.num_vertices));


			// We did not process the other rim vertex of this triangle,
			// but we add this triangle to processed triangles, because that vertex will be processed at the next step (or it was on the first one).
			add_processed_triangle(t_patch);
		}

		if (verbose)
			cout << "  ...current index " << processed_triangle_count << endl;
		//break;
	}

	// Step 2: processing triangles neighboring seed triangles (those that are added and not yet served as seeds)
	while (seed_index < processed_triangle_count)
	{
		unsigned int seed = processed_triangles[seed_index++];
		for (unsigned int j = 0; j < 3; j++)
			if (triangles[seed].HasAdjacentTriangle(j))
				if (!is_triangle_processed[triangles[seed].AdjTriIndx(j)])
					if (!IsRimVertex(VertexAcrossEdge(seed, j)))
					{
						if (base_triangle_of_vertex_projection[triangles[seed].vertex[j]] < 0)
						{
							//cout << "cs_vertex_index " << cs_vertex_index << " of " << num_vertices << endl;
							//for (int k = 0; k < seed_index; k++)
							//	cout << processed_triangles[k] << "  ";
							//cout << endl;
							std::set<unsigned int> err_tri;
							//AddFanToSet(triangles[seed].vertex[j], err_tri);
							AddFanToSet(triangles[seed].vertex[(j + 1) % 3], err_tri);
							FragmentToFile("err_rim_frag2.stl", "Fragment", err_tri, Color::SaladGreen());
						}
//@						ASSERT(base_triangle_of_vertex_projection[triangles[seed].vertex[j]] >= 0, "Vertex " << j << " (edge's beginning) of seed triangle " << seed << " must have been processed");
						if (!(base_triangle_of_vertex_projection[triangles[seed].vertex[j]] >= 0))
							throw runtime_error("Vertex " + std::to_string(j) + " (edge's beginning) of seed triangle " + std::to_string(seed) + " must have been processed");

//@						ASSERT(base_triangle_of_vertex_projection[triangles[seed].vertex[(j + 1) % 3]] >= 0, "Vertex " << ((j + 1) % 3) << " (edge's end) of seed triangle " << seed << " must have been processed");
						if (!(base_triangle_of_vertex_projection[triangles[seed].vertex[(j + 1) % 3]] >= 0))
							throw runtime_error("Vertex " + std::to_string((j + 1) % 3) + " (edge's end) of seed triangle " + std::to_string(seed) + " must have been processed");

						process_internal_vertex(VertexAcrossEdge(seed, j), base_triangle_of_vertex_projection[triangles[seed].vertex[j]]);
						add_processed_triangle(triangles[seed].AdjTriIndx(j));
					}
	}

	// Checking if any unprocessed triangles/vertices are left
	unsigned int num_vert_not_processed = 0;
	for (unsigned int i = 0; i < vertices.size(); i++)
		if (base_triangle_of_vertex_projection[i] < 0)
			if (vertices[i].first_link_index >= 0)
				num_vert_not_processed++;
	MILD_ASSERT(num_vert_not_processed == 0, "Nonzero number of not processed vertices " << num_vert_not_processed);

	unsigned int num_tri_not_processed = 0;
	for (unsigned int i = 0; i < triangles.size(); i++)
		is_triangle_processed[i] ? 0 : num_tri_not_processed++;
	MILD_ASSERT(num_tri_not_processed + processed_triangle_count == triangles.size(),
		"Numbers of counted unprocessed triangles " << num_tri_not_processed << " and of processed ones " << processed_triangle_count <<
		" do not add up to total triangle count " << triangles.size());

	MILD_ASSERT(processed_triangle_count == triangles.size(), "Number of processed triangles " << processed_triangle_count <<
		" does not match number of patch triangles " << triangles.size());

	unsigned int moved_count = moved.size();
	if (verbose)
	{
		cout << "Ensuring that patch is not below base: " << moved_count << " vertices moved";
		if (moved_count > 0)
			cout << ", max move " << max_move;
		cout << endl;
	}

	// Adjusting vertex projections due to curvature
	if (moved.size() > 0)
	{
		class _VertexData
		{
		public:
			_VertexData(/*unsigned int i,*/ Vector3d& n, double rd) : /*index(i),*/ mean_normal(n), mean_rdelta(rd) {}
			//unsigned int index;
			Vector3d mean_normal;
			double mean_rdelta;
		};
		unsigned int *base_vertex_data_1index = new unsigned int[base.vertices.size()];
		for (unsigned int i = 0; i < base.vertices.size(); i++)
			base_vertex_data_1index[i] = 0;

		std::vector<_VertexData> vdata;  // _VertexData for vertices of base projection triangles corresponding to patch vertices that were moved
		vdata.reserve(moved.size());

		for (unsigned int vp : moved)
		{
//@			ASSERT(base_triangle_of_vertex_projection[vp] >= 0, "No base triangle for moved patch vertex " << vp);
			if (!(base_triangle_of_vertex_projection[vp] >= 0))
				throw runtime_error("No base triangle for moved patch vertex " + std::to_string(vp));

//@			ASSERT(base_triangle_of_vertex_projection[vp] < (int)base.triangles.size(), "Base triangle for moved patch vertex " << vp << " is out of range 0 - " << (base.triangles.size() - 1));
			if (!(base_triangle_of_vertex_projection[vp] < (int)base.triangles.size()))
				throw runtime_error("Base triangle for moved patch vertex " + std::to_string(vp) + " is out of range 0 - " + std::to_string(base.triangles.size() - 1));

			const Triangle& base_tri = base.triangles[base_triangle_of_vertex_projection[vp]];

			// Calculating curvature for the given base triangle (if not done yet)
			for (unsigned int j = 0; j < 3; j++)
				if (base_vertex_data_1index[base_tri.vertex[j]] == 0)
				{
					Vector3d mean_normal = base.MeanNormalAtVertex(base_tri.vertex[j]);
					vdata.emplace_back(mean_normal, base.MeanRelativeDeltaAroundVertex(base_tri.vertex[j], mean_normal));
					base_vertex_data_1index[base_tri.vertex[j]] = vdata.size();
				}

			// Finding out which of the 3 vertices of base triangle is nearest
			double dist[3], min_dist = DBL_MAX;
			unsigned int j_mindist;
			for (unsigned int j = 0; j < 3; j++)
			{
				dist[j] = vertices[vp].DistanceTo(base.vertices[base_tri.vertex[j]]);
				if (dist[j] < min_dist)
				{
					min_dist = dist[j];
					j_mindist = j;
				}
			}

			// Using distance to the nearest vertex of base projection triangle and that vertex' curvature parameter to determine further shift of the current patch vertex (vp)
			unsigned int base_vertex_index = base_tri.vertex[j_mindist];
			double sinus = vdata[base_vertex_data_1index[base_vertex_index] - 1].mean_rdelta;
			double shift = -dist[j_mindist] * (abs(sinus) < 0.1 ? (sinus / sqrt(1 - sinus * sinus)) : ((1 - sqrt(1 - sinus * sinus)) / sinus));
			//cout << j_mindist << "  "<< shift << "    "  << sinus << endl;
			vertices[vp].OffsetCoords(base_tri.normal * shift);
		}
		if (verbose)
			cout << "Base vertices belonging to projection triangles: " << vdata.size() << endl;
		vdata.clear();
		delete[] base_vertex_data_1index;

		// Updating normals of patch triangles affected by vertex moves
		// We'll reuse array 'is_triangle_processed'. It will flag triangles whose normals were updated

		unsigned int degenerate_count = 0;

		for (unsigned int i = 0; i < triangles.size(); i++)
			is_triangle_processed[i] = false;

		for (unsigned int vp : moved)
		{
			int link = vertices[vp].first_link_index;
			while (link >= 0)
			{
				unsigned int t = Surface::Link2Triangle(link);
//@				ASSERT(t < (int)triangles.size(), "Invalid link for vertex " << vp);
				if (!(t < (int)triangles.size()))
					throw runtime_error("Invalid link for vertex " + std::to_string(vp));
				if (!is_triangle_processed[t])
				{
					try
					{
						triangles[t].CalculateNormalAndCenter(&vertices[0]);
					}
					catch (DegenerateTriangleException ex)
					{
						cout << "\n\nWARNING: A degenerate triangle was formed during adjustment of patch\n";
						cout << ex.what() << endl;
						std::set<unsigned int> degenerate;
						AddFanToSet(triangles[t].vertex[0], degenerate);
						AddFanToSet(triangles[t].vertex[1], degenerate);
						AddFanToSet(triangles[t].vertex[2], degenerate);

						char buf[128];
						sprintf_s(buf, "degenerate_%d.stl", degenerate_count);
						FragmentToFile(buf, "Fragment near degenerate triangle", degenerate, Color::Green());

						cout << "A fragment file '" << buf << "' with surrounding context is created. Try to superimpose with folds, if any, for it's the most likely cause.\n";
						degenerate_count++;
					}
					is_triangle_processed[t] = true;
				}
				link = vertex_links[link];
			}
		}

	}  // if (moved.size() > 0)


	moved.clear();
	delete[] base_triangle_of_vertex_projection;
	delete[] is_triangle_processed;
	delete[] processed_triangles;

	CheckIntegrity(verbose);

	return (moved_count > 0);
}

#pragma once
#include <iostream>
using namespace std;

#define ASSERT(expr, message)  if (!(expr)) { std::cout << endl << "Assertion failed: " << message << endl << endl << "Execution stopped" << endl << endl;  exit(1); }
#define MILD_ASSERT(expr, message)  if (!(expr)) { std::cout << endl << "MILD Assertion failed: " << message << endl << endl;  }

FILE* OpenStlFileForWrite(const char* stl_file_path, const char* title, unsigned int num_facets);
void CloseStlFileForWrite(FILE* stl);

void AddPostfixToPath(const char* path, const char* postfix, char* result, size_t nbytes);

class TopologicIrregularityException : public std::exception
{
};

class SelfIntersectingRimException : public TopologicIrregularityException
{
public:
	unsigned int vertex;  // vertex where problem was detected
	SelfIntersectingRimException(unsigned int v) : vertex(v) {}
	virtual const char* what() const { return "Self-intersection of rim encountered"; }
};

class EdgeInversionException : public TopologicIrregularityException
{
public:
	unsigned int base_triangle;
	unsigned int patch_vertex_1;
	unsigned int patch_vertex_2;
	EdgeInversionException(unsigned int t, unsigned int v1, unsigned int v2) : base_triangle(t), patch_vertex_1(v1), patch_vertex_2(v2) {}
	virtual const char* what() const { return "Inverted triangle edge direction encountered (usually means a surface fold)"; }
};

class DegenerateTriangleException : public TopologicIrregularityException
{
public:
	DegenerateTriangleException() {}
	virtual const char* what() const { return "Degenerate triangle encountered"; }
};

class TriangleFoldWithCoincidentVertices : public DegenerateTriangleException
{
public:
	unsigned int i_tri_1;
	unsigned int i_tri_2;
	unsigned int i_vrt_1;
	unsigned int i_vrt_2;
	unsigned int bad_triangle;
	TriangleFoldWithCoincidentVertices(unsigned int t1, unsigned int t2, unsigned int v1, unsigned int v2)
	{
		bad_triangle = t1;
		if (t1 < t2)
		{
			i_tri_1 = t1;
			i_vrt_1 = v1;
			i_tri_2 = t2;
			i_vrt_2 = v2;
		}
		else
		{
			i_tri_1 = t2;
			i_vrt_1 = v2;
			i_tri_2 = t1;
			i_vrt_2 = v1;
		}
		cout << "Exception TriangleFoldWithCoincidentVertices " << i_tri_1 << "  " << i_tri_2 << ", vert " << i_vrt_1 << "  " << i_vrt_2 << endl;
	}
	bool operator < (const TriangleFoldWithCoincidentVertices& r) const { return (i_tri_1 < r.i_tri_1); }
	virtual const char* what() const { return "Two folded triangles with coincident vertices"; }
};

inline bool NotANumber(float x)
{
	return !(x <= FLT_MAX);
}

inline bool NotANumber(double x)
{
	return !(x <= DBL_MAX);
}

// Colors to be used when writing to STL files. 
// As of May 2021, colors are only used for testing/debugging purposes. They are not required in the result file.
// Many colors now do not correspond to their names. Greens and violets do and others don't.
// Some research is needed for proper byte values.
class Color
{
public:
	unsigned char c[2];
	Color(unsigned char c1, unsigned char c2) : c{ c1, c2 } {}
	static Color White() { return Color((unsigned char)0xFF, (unsigned char)0xFF); }
	static Color Yellow() { return Color((unsigned char)0xFF, (unsigned char)0x55); }
	static Color Gray() { return Color((unsigned char)0x00, (unsigned char)0x00); }
	static Color VeryLightViolet() { return Color((unsigned char)0xEE, (unsigned char)0xEE); }
	static Color LightViolet() { return Color((unsigned char)0xDD, (unsigned char)0xDD); }
	static Color SaladGreen() { return Color((unsigned char)0xCF, (unsigned char)0xCF); }
	static Color Green() { return Color((unsigned char)0x0F, (unsigned char)0x0F); }
	static Color DarkGreen() { return Color((unsigned char)0x0C, (unsigned char)0x0C); }
	static Color Blue() { return Color((unsigned char)0x0E, (unsigned char)0x0E); }
	static Color DarkBlue() { return Color((unsigned char)0x0B, (unsigned char)0x0B); }
	static Color Orange() { return Color((unsigned char)0x45, (unsigned char)0x45); }
	static Color Purple() { return Color((unsigned char)0xA5, (unsigned char)0xA5); }
};

template <typename T> class Stats
{
public:
	Stats(char* what) { name = what; count = 0; };
	void Tell()
	{
		cout << name << ": total count " << count;
		if (count > 0)
			cout << ", range " << min << " - " << max << "  [" << (max - min) << "]   average " << (double)sum / double(count);
		cout << "\n";
	};

	void Add(T& v)
	{
		if (count == 0)
		{
			min = v;
			max = v;
		}
		else
		{
			if (min > v)
				min = v;
			else if (max < v)
				max = v;
		}
		count++;
		sum += v;
	}

private:
	char* name;
	unsigned int count;
	T min, max, sum;
};

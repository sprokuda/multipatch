#include "stdafx.h"
#include <exception>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>

#include "Surface.h"

// In this procedure, 'this' object is the patch
ContourOnSurface& Surface::ProjectRimContourToSurface(const Surface& s, const Contour &cntr, bool verbose) const
{
	ContourOnSurface* new_cntr = new ContourOnSurface(s, cntr);
	unsigned int tri1, tri2;
	tri2 = s.FindACloseTriangle(this->vertices[cntr.first_vertex], cntr.first_vertex_normal);
	unsigned int closest_vertex;


	unsigned int i = cntr.first_vertex, not_inside_count = 0, vertex_count = 0;
	bool is_inside;
	double max_distance = 0, d;
	bool last_hint_is_triangle = true;
	unsigned int num_coincident_vertices = 0;
	do
	{
		_ASSERT(i >= 0);
		_ASSERT(vertex_count < cntr.num_vertices);

		const Vertex& initial_vertex = this->vertices[i];

		//if (verbose)
		//	cout << endl << "Vertex " << i << ": " << initial_vertex;

		if (s.HasACoincidentVertex(initial_vertex, 0.02, (last_hint_is_triangle ? tri2 : closest_vertex), last_hint_is_triangle, closest_vertex)) //0.01
		{
			(new_cntr->vertices + vertex_count)->SetCoords(s.vertices[closest_vertex]);  //  !!!
			(new_cntr->vertices + vertex_count)->anchor.Set(Anchor::Type::OverVertex, closest_vertex);
			num_coincident_vertices++;
			last_hint_is_triangle = false;
			is_inside = true;
		}
		else
		{
			tri1 = s.RefineClosestTriangle_L2DistanceToCenter(initial_vertex, tri2);  // = tri2;
			is_inside = s.RefineClosestTriangle_ProjDistanceToEdge(initial_vertex, tri1, tri2, new_cntr->vertices + vertex_count);
		}

		(new_cntr->vertices + vertex_count)->anchor.projected_vertex = i;
		d = initial_vertex.DistanceTo(new_cntr->vertices[vertex_count]);
		if (max_distance < d)
			max_distance = d;
		//if (verbose)
		//	cout << "  --> " << *(new_cntr->vertices + vertex_count) << ",  d = " << d << endl;

		if (!is_inside)
			if (verbose)
				cout << "Vertex " << i << " [" << vertex_count << "] not inside (" << (new_cntr->vertices + vertex_count)->anchor << ")" << endl;

		vertex_count++;
		i = initial_vertex.GetNextVertex();

		if (!is_inside)
			not_inside_count++;
	} while (i != cntr.first_vertex);

	if (verbose)
		std::cout << "Not inside count: " << not_inside_count << " of " << new_cntr->num_vertices << ", max distance " << max_distance <<
		", coincidences " << num_coincident_vertices << endl;
	return *new_cntr;
}

bool Surface::HasACoincidentVertex(const Xyz& point, double tolerance, unsigned int starting_item, bool is_triangle, unsigned int &closest_vertex) const
{
	double dmin = DBL_MAX;

	auto check_vertex = [this, &point, &closest_vertex, &dmin](unsigned int vertex)
	{
		double d = point.DistanceTo(vertices[vertex]);
		if (d < dmin)
		{
			dmin = d;
			closest_vertex = vertex;
		}
	};

	if (is_triangle)
	{
		for (unsigned int j = 0; j < 3; j++)
			check_vertex(triangles[starting_item].vertex[j]);
	}
	else // starting item is vertex
		check_vertex(starting_item);

	unsigned int n = 0;  // TODO: take out this guard
	unsigned int starting_vertex;
	do
	{
		starting_vertex = closest_vertex;
		int link = vertices[starting_vertex].first_link_index;
		while (link > 0)
		{
			unsigned int t = Surface::Link2Triangle(link);
			unsigned int v = Surface::Link2Vertex(link);
//@			ASSERT(triangles[t].vertex[v] == starting_vertex, "Integrity broken near vertex " << starting_vertex);
			if (!(triangles[t].vertex[v] == starting_vertex))
				throw runtime_error("Integrity broken near vertex " + std::to_string(starting_vertex));

			check_vertex(triangles[t].vertex[(v + 1) % 3]);

			if (!triangles[t].HasAdjacentTriangle((v + 2) % 3))
				check_vertex(triangles[t].vertex[(v + 2) % 3]);

			link = vertex_links[link];
			n++;
		}
	} while (closest_vertex != starting_vertex && n < 1000);
//@	ASSERT(n < 1000, "Infinite loop while searching for a close vertex");
	if (!(n < 1000))
		throw runtime_error("Infinite loop while searching for a close vertex");
	//cout << "      " << n << " it,  " << dmin << endl;
	return (dmin < tolerance);
}

// Direct search through all triangles regardless of their connectivity. Minimum distance between the point and triangle center in C norm is seeked.
// Only triangles with normals directed less than 90 degrees away from the given vector are considered.
unsigned int Surface::FindACloseTriangle(const Xyz& point, const Vector3d& normal) const
{
	double min_dist = FLT_MAX, d;
	unsigned int min_index = 0;
	for (unsigned int i = 0; i<triangles.size(); i++)
	{
		d = triangles[i].center.CDistanceTo(point);
		if (d > min_dist)
			continue;
		if (Vector3d::DotProduct(normal, triangles[i].normal) <= 0)
			continue;
		min_dist = d;
		min_index = i;
	}
	return min_index;
}

// Refined search for closest triangle in L2 norm. Starting from the given triangle, the algorithm searches through 
// connected edges until a local minimum is found. Normals are not accounted for.
unsigned int Surface::RefineClosestTriangle_L2DistanceToCenter(const Xyz& point, unsigned int starting_triangle) const
{
	double min_dist = triangles[starting_triangle].center.DistanceTo(point), d;
	unsigned int min_index = starting_triangle, edge, entrance_edge, min_entrance_edge = 3, count = 0;
	bool closest_changed;
	do
	{
		const Triangle& base_tri = triangles[min_index];
		entrance_edge = min_entrance_edge;
		closest_changed = false;

		for (edge = 0; edge < 3; edge++)
		{
			if (edge != entrance_edge && base_tri.HasAdjacentTriangle(edge))
			{
				d = triangles[base_tri.AdjTriIndx(edge)].center.DistanceTo(point);
				if (d < min_dist)
				{
					min_dist = d;
					min_index = base_tri.AdjTriIndx(edge);
					min_entrance_edge = base_tri.AdjTriEdge(edge);
					closest_changed = true;
					count++;
				}
			}
		}
	} while (closest_changed);
	//cout << "    RefineClosestTriangle: count = " << count << endl;
	return min_index;
}


//
//	Fine-tuning of determining the base triangle to project to. Criterion is distance of projection point to triangle (inside triangle means search is over).
//  BE VERY CAREFUL about giving a good starting_triangle   since the criterion involves only projection's position (rather than initial point's).
//  Using RefineClosestTriangle_L2DistanceToCenter to provide starting_triangle for this procudure is strongly advised. Otherwise, if skipped,
//  local curvatures may cause the result triangle to slide afar from any reasonable location.
//
bool Surface::RefineClosestTriangle_ProjDistanceToEdge(const Xyz& point, unsigned int starting_triangle, unsigned int& final_triangle,
	VertexOfContourOnSurface *presult) const
{
	unsigned int previous_triangle = starting_triangle, min_distance_triangle, first_fan_triangle;
	unsigned int edge_or_vertex, edge_or_vertex2, min_distance_edge_or_vertex;
	unsigned int fan_vertex = vertices.size() + 1;
	bool is_edge, is_edge2, min_distance_is_edge;
	bool over_edge = false;
	double distance, min_distance = DBL_MAX;
	Xyz previous_projection, min_distance_projection;
	int num_iter = 0;

	Xyz& projection = *presult;
	//unsigned int& final_triangle = presult->triangle;
	//Xyz projection;
	//unsigned int final_triangle;



	final_triangle = starting_triangle;

	const unsigned int MaxNotInsideIterations = 20u;
	const double PositionTolerance = 5e-8;

	//cout << "RefineClosestTriangle_ProjDistanceToEdge:";
	while (num_iter < MaxNotInsideIterations)
	{

		ProjectPointToTriangle(point, final_triangle, projection);
		distance = MeasureDistanceToTriangle(projection, final_triangle, edge_or_vertex, is_edge);
		if (distance < 0)
		{
			presult->anchor.Set(Anchor::Type::InsideTriangle, final_triangle);
			return true;
		}

		if (distance < min_distance)
		{
			min_distance = distance;
			min_distance_triangle = final_triangle;
			min_distance_projection = projection;
			min_distance_edge_or_vertex = edge_or_vertex;
			min_distance_is_edge = is_edge;
		}

		//cout << " is_edge: " << (is_edge ? "EDGE " : "VERTEX ") << edge;

		if (is_edge)
		{
			// Passing to adjacent triangle over edge 'edge_or_vertex'
			//cout << " ->";
//@			ASSERT(triangles[final_triangle].HasAdjacentTriangle(edge_or_vertex), "No adjacent triangle");
			if (!(triangles[final_triangle].HasAdjacentTriangle(edge_or_vertex)))
				throw runtime_error("No adjacent triangle");

			over_edge = (triangles[final_triangle].AdjTriIndx(edge_or_vertex) == previous_triangle);
			if (over_edge)
			{
				projection = ProjectPointOverEdge(final_triangle, projection, previous_triangle, previous_projection);

				distance = MeasureDistanceToTriangle(projection, final_triangle, edge_or_vertex, is_edge);

				if (distance < 0)
				{
					//presult->anchor.Set(Anchor::Type::OverEdge, final_triangle, edge_or_vertex);
					presult->anchor.Set(Anchor::Type::OverEdge, final_triangle, edge_or_vertex,
						triangles[final_triangle].AdjTriIndx(edge_or_vertex),
						triangles[final_triangle].AdjTriEdge(edge_or_vertex));
					return true;
				}

				if (distance < min_distance)
				{
					min_distance = distance;
					min_distance_triangle = final_triangle;
					min_distance_projection = projection;
					min_distance_edge_or_vertex = edge_or_vertex;
					min_distance_is_edge = is_edge;
				}

				double distance2 = MeasureDistanceToTriangle(projection, previous_triangle, edge_or_vertex2, is_edge2);  // not previous_projection!!!
				if (distance2 < 0)
				{
					presult->anchor.Set(Anchor::Type::OverEdge, final_triangle, edge_or_vertex,
						triangles[final_triangle].AdjTriIndx(edge_or_vertex),
						triangles[final_triangle].AdjTriEdge(edge_or_vertex));
					return true;
				}

				if (distance2 < min_distance)
				{
					min_distance = distance2;
					min_distance_triangle = previous_triangle;
					min_distance_projection = projection;
					min_distance_edge_or_vertex = edge_or_vertex2;
					min_distance_is_edge = is_edge2;
				}

				//const Triangle& t = triangles[final_triangle];
				//const Triangle& t2 = triangles[previous_triangle];
				//cout << "Over edge between triangles " << final_triangle << " and " << previous_triangle << ", distances " << 
				//	distance << (is_edge ? " edge " : " vertex ") << edge << " (" << t.vertex[edge] << " " << (is_edge ? t.vertex[(edge+1)%3] : -1) << "), " <<
				//	distance2 << (is_edge2 ? " edge " : " vertex ") << edge2 << " (" << t2.vertex[edge2] << " " << (is_edge2 ? t2.vertex[(edge2 + 1) % 3] : -1) << ")\n";

				//if(distance2 < distance)
				//return false;
			}
			previous_triangle = final_triangle;
			previous_projection = projection;
			final_triangle = triangles[final_triangle].AdjTriIndx(edge_or_vertex);
		}
		else // Over vertex
		{
			// Visiting triangles around vertex 'edge' (which in this case means vertex)
			first_fan_triangle = final_triangle;
			if (fan_vertex == triangles[first_fan_triangle].vertex[edge_or_vertex])  // I.e. this fan vertex was encountered before
			{
				// OVER VERTEX
				final_triangle = min_distance_triangle;

				presult->SetCoords(vertices[fan_vertex]);
				presult->anchor.Set(Anchor::Type::OverVertex, fan_vertex);
				return true;
			}
			fan_vertex = triangles[first_fan_triangle].vertex[edge_or_vertex];
			//cout << " *" << fan_vertex;

			const Vertex& vert = vertices[fan_vertex];
			int link = vert.first_link_index;
			while (link >= 0)
			{
				final_triangle = Surface::Link2Triangle(link);
				//cout << " _" << final_triangle;
				if (final_triangle != first_fan_triangle)
				{
					ProjectPointToTriangle(point, final_triangle, projection);
					distance = MeasureDistanceToTriangle(projection, final_triangle, edge_or_vertex, is_edge);
					//cout << " " << distance;
					if (distance < 0)
					{
						presult->anchor.Set(Anchor::Type::InsideTriangle, final_triangle);
						return true;
					}

					if (distance < min_distance)
					{
						min_distance = distance;
						min_distance_triangle = final_triangle;
						min_distance_projection = projection;
						min_distance_edge_or_vertex = edge_or_vertex;
						min_distance_is_edge = is_edge;
					}
				}
				link = vertex_links[link];
			}
			final_triangle = min_distance_triangle;
		}
		num_iter++;
	}
	final_triangle = min_distance_triangle;
	projection = min_distance_projection;

	if (min_distance_is_edge)
		presult->anchor.Set(Anchor::Type::OverEdge, min_distance_triangle, min_distance_edge_or_vertex,
			triangles[min_distance_triangle].AdjTriIndx(min_distance_edge_or_vertex),
			triangles[min_distance_triangle].AdjTriEdge(min_distance_edge_or_vertex));
	else
		presult->anchor.Set(Anchor::Type::OverVertex, min_distance_triangle, triangles[min_distance_triangle].vertex[min_distance_edge_or_vertex]);

	//cout << "RefineClosestTriangle_ProjDistanceToEdge: return after exceeding iterations limit, distance " << min_distance <<
	//	(min_distance_is_edge ? " EDGE" : " VERTEX") << endl;

	return (min_distance < PositionTolerance);
}

// Returns point that belongs to intersection of the triangles and the plane containing proj_1 and proj_2 perpendicular to both triangles.
// Proj_1 and proj_2 are projections of a certain point to planes of the first and second triangle, correspondingly.
Xyz Surface::ProjectPointOverEdge(unsigned int tri_index_1, const Xyz& proj_1, unsigned int tri_index_2, const Xyz& proj_2) const
{
	const Triangle& tri_1 = triangles[tri_index_1];
	const Triangle& tri_2 = triangles[tri_index_2];
	Xyz proj_12 = tri_2.ProjectPoint(proj_1);
	Xyz proj_21 = tri_1.ProjectPoint(proj_2);
	double h1 = proj_1.DistanceTo(proj_12);
	double h2 = proj_2.DistanceTo(proj_21);

	//cout << endl;
	//cout << " proj_12 inside tri2: " << IsPointInsideTriangle(proj_12, tri_2) << endl;
	//cout << " proj_21 inside tri1: " << IsPointInsideTriangle(proj_21, tri_1) << endl;

	if (h1 + h2 < 1e-15)
	{
		return Xyz(0.5*(proj_12.x + proj_21.x), 0.5*(proj_12.y + proj_21.y), 0.5*(proj_12.z + proj_21.z));
	}

	double cos_alpha = Vector3d::DotProduct(tri_1.normal, tri_2.normal);
	double k = h1 * cos_alpha / (h2 + h1 * cos_alpha);

	Xyz proj = Xyz(
		proj_12.x + (proj_2.x - proj_12.x)*k,
		proj_12.y + (proj_2.y - proj_12.y)*k,
		proj_12.z + (proj_2.z - proj_12.z)*k);

	return proj;
}

Xyz Triangle::ProjectPoint(const Xyz& point) const
{
	double alpha = (center.x - point.x)*normal.x + (center.y - point.y)*normal.y + (center.z - point.z)*normal.z;
	return Xyz(point.x + alpha * normal.x,
		point.y + alpha * normal.y,
		point.z + alpha * normal.z);
}

void Triangle::ProjectPoint(const Xyz& point, Xyz& projection) const
{
	double alpha = (center.x - point.x)*normal.x + (center.y - point.y)*normal.y + (center.z - point.z)*normal.z;
	projection.x = point.x + alpha * normal.x;
	projection.y = point.y + alpha * normal.y;
	projection.z = point.z + alpha * normal.z;
}

bool Surface::IsPointInsideTriangle(const Xyz& point, const Triangle& tri) const
{
	Vector3d v0(point, vertices[tri.vertex[0]]);     //, 100.0);
	Vector3d v1(point, vertices[tri.vertex[1]]);     //, 100.0);
	Vector3d v2(point, vertices[tri.vertex[2]]);     //, 100.0);
	Vector3d r0 = Vector3d::CrossProduct(v0, v1);
	Vector3d r1 = Vector3d::CrossProduct(v1, v2);
	Vector3d r2 = Vector3d::CrossProduct(v2, v0);
	//cout << endl;
	//cout << "         " << 0x1 << "  norm " << Vector3d::CNorm(r0) << endl;
	//cout << "         " << 1x2 << "  norm " << Vector3d::CNorm(r1) << endl;
	//cout << "         " << 2x0 << "  norm " << Vector3d::CNorm(r2) << endl;
	return	((r0.x <= 0 && r1.x <= 0 && r2.x <= 0) || (r0.x >= 0 && r1.x >= 0 && r2.x >= 0)) &&
		((r0.y <= 0 && r1.y <= 0 && r2.y <= 0) || (r0.y >= 0 && r1.y >= 0 && r2.y >= 0)) &&
		((r0.z <= 0 && r1.z <= 0 && r2.z <= 0) || (r0.z >= 0 && r1.z >= 0 && r2.z >= 0));
	// The criterion above is curiously correct, although r0, r1, r2 should be compared to the triangle's normal.
	// For the point to be inside triangle all 4 vectors should be pointed in one direction.
}

bool Surface::IsPointInsideTriangle(const Xyz& point, const Triangle& tri, bool verbose) const
{
	Vector3d v0(point, vertices[tri.vertex[0]]);   //, 100.0);
	Vector3d v1(point, vertices[tri.vertex[1]]);   //, 100.0);
	Vector3d v2(point, vertices[tri.vertex[2]]);   //, 100.0);
	Vector3d r0 = Vector3d::CrossProduct(v0, v1);
	Vector3d r1 = Vector3d::CrossProduct(v1, v2);
	Vector3d r2 = Vector3d::CrossProduct(v2, v0);
	bool is_inside =
		((r0.x <= 0 && r1.x <= 0 && r2.x <= 0) || (r0.x >= 0 && r1.x >= 0 && r2.x >= 0)) &&
		((r0.y <= 0 && r1.y <= 0 && r2.y <= 0) || (r0.y >= 0 && r1.y >= 0 && r2.y >= 0)) &&
		((r0.z <= 0 && r1.z <= 0 && r2.z <= 0) || (r0.z >= 0 && r1.z >= 0 && r2.z >= 0));
	if (verbose)
	{
		cout << " Is inside: " << (is_inside ? "true" : "FALSE") << endl;
		cout << "       0x1  " << r0 << "  norm " << Vector3d::CNorm(r0) << endl;
		cout << "       1x2  " << r1 << "  norm " << Vector3d::CNorm(r1) << endl;
		cout << "       2x0  " << r2 << "  norm " << Vector3d::CNorm(r2) << endl;
	}
	return is_inside;
}

// NOTE: It is strongly advised for the point to roughly belong to triangle's plane
bool Surface::IsPointInsideTriangle(const Xyz& point, const Triangle& tri, unsigned int& edge) const
{
	Vector3d v0(point, vertices[tri.vertex[0]]);
	Vector3d v1(point, vertices[tri.vertex[1]]);
	Vector3d v2(point, vertices[tri.vertex[2]]);
	Vector3d r0 = Vector3d::CrossProduct(v0, v1);
	Vector3d r1 = Vector3d::CrossProduct(v1, v2);
	Vector3d r2 = Vector3d::CrossProduct(v2, v0);
	bool res = ((r0.x < 0 && r1.x < 0 && r2.x < 0) || (r0.x >= 0 && r1.x >= 0 && r2.x >= 0)) &&
		((r0.y < 0 && r1.y < 0 && r2.y < 0) || (r0.y >= 0 && r1.y >= 0 && r2.y >= 0)) &&
		((r0.z < 0 && r1.z < 0 && r2.z < 0) || (r0.z >= 0 && r1.z >= 0 && r2.z >= 0));
	if (!res)
	{
		if ((r1.x < 0 && r2.x < 0) || (r1.x >= 0 && r2.x >= 0))
			edge = 0;
		else if ((r0.x < 0 && r2.x < 0) || (r0.x >= 0 && r2.x >= 0))
			edge = 1;
		else if ((r0.x < 0 && r1.x < 0) || (r0.x >= 0 && r1.x >= 0))
			edge = 2;
		else
		{
			std::cout << "         Can't decide what edge: " << endl;
			std::cout << "         " << r0 << "  norm " << Vector3d::CNorm(r0) << endl;
			std::cout << "         " << r1 << "  norm " << Vector3d::CNorm(r1) << endl;
			std::cout << "         " << r2 << "  norm " << Vector3d::CNorm(r2) << endl;
			exit(1); // TODO: get rid of exit()
		}
	}
	// cout << "IsPointInsideTriangle:  result " << (res? "TRUE" : "FALSE") << ",  edge " << edge << endl;
	return res;
}

// Projection onto triangle's plane
void Surface::ProjectPointToTriangle(const Xyz& point, const unsigned int& tri_index, Xyz& projection) const
{
	const Triangle& tri = triangles[tri_index];
	double alpha = (tri.center.x - point.x)*tri.normal.x + (tri.center.y - point.y)*tri.normal.y + (tri.center.z - point.z)*tri.normal.z;
	projection.x = point.x + alpha * tri.normal.x;
	projection.y = point.y + alpha * tri.normal.y;
	projection.z = point.z + alpha * tri.normal.z;
}


// Returns -1 if 'projection' is inside the triangle, or distance to its closest point otherwise. 
// Output parameters 'edge_or_vertex', 'is_edge' are set only if the 'projection' is outside.
// 'Is_edge' means whether 'projection' lies across one edge (if true) or across two edges, that is, behind a vertex (if false).
// So in the first case 'edge_or_vertex' contains local index (values 0, 1, 2) of an edge, and in the second case, that of a vertex. 
//
double Surface::MeasureDistanceToTriangle(const Xyz& projection, const unsigned int& tri_index,
	unsigned int& edge_or_vertex, bool& is_edge) const
{
	const Triangle& tri = triangles[tri_index];

	Vector3d v0(projection, vertices[tri.vertex[0]]);
	Vector3d v1(projection, vertices[tri.vertex[1]]);
	Vector3d v2(projection, vertices[tri.vertex[2]]);
	Vector3d r0 = Vector3d::CrossProduct(v0, v1);
	Vector3d r1 = Vector3d::CrossProduct(v1, v2);
	Vector3d r2 = Vector3d::CrossProduct(v2, v0);

	double min_d = DBL_MAX;

	is_edge = false;
	bool edges[3] = { false, false, false };

	auto check_opposite_to_normal = [tri, this, &min_d, &is_edge, &edge_or_vertex, &edges](Vector3d& vec, unsigned int edge)
	{
		if (Vector3d::DotProduct(vec, tri.normal) < 0)
			/*(tri.normal.x > 0 && vec.x < 0) ||
			(tri.normal.x < 0 && vec.x > 0) ||
			(tri.normal.y > 0 && vec.y < 0) ||
			(tri.normal.y < 0 && vec.y > 0) ||
			(tri.normal.z > 0 && vec.z < 0) ||
			(tri.normal.z < 0 && vec.z > 0))*/
		{
			double d = vec.Norm() / vertices[tri.vertex[edge]].DistanceTo(vertices[tri.vertex[(edge + 1) % 3]]);
			if (d < min_d)
				min_d = d;
			is_edge = !is_edge;
			edge_or_vertex = edge;
			edges[edge] = false;
		}
		else
			edges[edge] = true;
	};
	check_opposite_to_normal(r0, 0);
	check_opposite_to_normal(r1, 1);
	check_opposite_to_normal(r2, 2);

	if (edges[0] && edges[1] && edges[2]) // i.e. r0, r1 and r1 are all codirected to normal, therefore point is inside the triangle
		return -1.0;

	if (!(edges[0] || edges[1] || edges[2]))
	{
		cout << " r0 " << r0 << endl;
		cout << " r1 " << r1 << endl;
		cout << " r2 " << r2 << endl;
		cout << " normal " << tri.normal << endl;
	}

//@	ASSERT(edges[0] || edges[1] || edges[2], " MeasureDistanceToTriangle: edges[0], edges[1], edges[2] all three false at triangle " << tri_index);
	if (!(edges[0] || edges[1] || edges[2]))
		throw runtime_error(" MeasureDistanceToTriangle: edges[0], edges[1], edges[2] all three false at triangle " + std::to_string(tri_index));

	if (is_edge)
	{
		// We have to project the projection onto this edge's line  and find out if it's on the edge
		unsigned int index_v1 = tri.vertex[edge_or_vertex];
		unsigned int index_v2 = tri.vertex[(edge_or_vertex + 1) % 3];
		Vector3d perp_vec = Vector3d::CrossProduct(tri.normal, Vector3d(vertices[index_v1], vertices[index_v2]));

		double perp_norm = perp_vec.Norm();
//@		ASSERT(perp_norm > 1e-15, "Triangle's edge length is too small");  // ASSERT perp_norm is not near zero
		if (!(perp_norm > 1e-1))
			throw runtime_error("Triangle's edge length is too small");

		double coeff = min_d / perp_norm;

		Xyz projproj = Xyz(projection.x + coeff * perp_vec.x, projection.y + coeff * perp_vec.y, projection.z + coeff * perp_vec.z);

		Vector3d vec1 = Vector3d(vertices[index_v1], projproj);
		Vector3d vec2 = Vector3d(vertices[index_v2], projproj);

		double d1 = projection.DistanceTo(vertices[index_v1]);
		double d2 = projection.DistanceTo(vertices[index_v2]);

		if (Vector3d::AreCollinearVectorsCodirected(vec1, vec2))
			min_d = (d1 < d2) ? d1 : d2;

		// NB! If vectors vec1 and vec2 have opposite directions, min_d determined earlier remains valid as min distance to triangle
	}
	else
	{
		if (edges[0])
			edge_or_vertex = 2;
		else if (edges[1])
			edge_or_vertex = 0;
		else // if(edges[2])
			edge_or_vertex = 1;

		min_d = projection.DistanceTo(vertices[tri.vertex[edge_or_vertex]]);

		// TODO: ASSERT that distance to this vertex is less than distances to other two
	}

	return min_d;
}

// Returns true if the resulting projection is inside triangle. If not inside, 'edge' is assigned either 0 (edge between vertices 0 and 1), 
// 1 (edge between vertices 1 and 2) or 2 (edge between vertices 2 and 0).
bool Surface::ProjectPointToTriangleAndCheckIfInside(const Xyz& point, const unsigned int& tri_index, Xyz& projection, unsigned int& edge) const
{
	const Triangle& tri = triangles[tri_index];
	double alpha = (tri.center.x - point.x)*tri.normal.x + (tri.center.y - point.y)*tri.normal.y + (tri.center.z - point.z)*tri.normal.z;
	projection.x = point.x + alpha * tri.normal.x;
	projection.y = point.y + alpha * tri.normal.y;
	projection.z = point.z + alpha * tri.normal.z;

	Vector3d v0(projection, vertices[tri.vertex[0]]);
	Vector3d v1(projection, vertices[tri.vertex[1]]);
	Vector3d v2(projection, vertices[tri.vertex[2]]);
	Vector3d r0 = Vector3d::CrossProduct(v0, v1);
	Vector3d r1 = Vector3d::CrossProduct(v1, v2);
	Vector3d r2 = Vector3d::CrossProduct(v2, v0);

	bool is_inside = ((r0.x < 0 && r1.x < 0 && r2.x < 0) || (r0.x >= 0 && r1.x >= 0 && r2.x >= 0)) &&
		((r0.y < 0 && r1.y < 0 && r2.y < 0) || (r0.y >= 0 && r1.y >= 0 && r2.y >= 0)) &&
		((r0.z < 0 && r1.z < 0 && r2.z < 0) || (r0.z >= 0 && r1.z >= 0 && r2.z >= 0));

	if (!is_inside)
	{
		if ((r1.x < 0 && r2.x < 0) || (r1.x >= 0 && r2.x >= 0))
			edge = 0;
		else if ((r0.x < 0 && r2.x < 0) || (r0.x >= 0 && r2.x >= 0))
			edge = 1;
		else // if ((r0.x < 0 && r1.x < 0) || (r0.x >= 0 && r1.x >= 0))
			edge = 2;
	}
	return is_inside;
}


#include "stdafx.h"
#include <exception>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>

#include "Surface.h"
#include "PatchedSurface.h"

Surface::Surface(char* stl_file_path)
{

	try
	{
		if (ReadTriangles(stl_file_path))
		{
//@			ASSERT(triangles.size() > 0, "File '" << stl_file_path << "' could not be opened or read");
			if (!(triangles.size() > 0))
			{
				std::string str_stl_file_path(stl_file_path);
				throw runtime_error("File '" + str_stl_file_path + "' could not be opened or read");
//				throw runtime_error("File '" + to_string(4) + "' could not be opened or read");
			}

			RebuildConnections:
			try
			{
				RebuildConnectivity();
			}
			catch (SelfIntersectingRimException& ex)
			{
				cout << endl << "Error while processing data from '" << stl_file_path << "': " << ex.what() << endl;
				cout << "Removing vertex " << ex.vertex << endl;
				RemoveVertexAndItsTriangles(ex.vertex);
				goto RebuildConnections;

				//std::set<unsigned int> frag;
				//AddFanToSet(ex.vertex, frag);
			}
		}

//@		ASSERT(triangles.size() > 0, "No triangles remained");
		if (!(triangles.size() > 0))
			throw runtime_error("No triangles remained");

		m_stl_path = stl_file_path;

		bool only_normals;
		bool ok = CheckIntegrity(true, only_normals);
		if ((!ok) && only_normals)
		{
			// This can't happen anymore as normals from file are ingored (as consequence of applying vertex tolerances).
			// Instead they are calculated every time after reading.
			cout << "Correcting triangle normals\n";
			Vertex* pv = &vertices[0];
			for (Triangle& tri : triangles)
				tri.CalculateNormalAndCenter(pv);
			ok = true;
		}



//@		ASSERT(ok, "Data read from file '" << stl_file_path << "' have flaws, see messages above");
		if (!(ok))
		{
			std::string str_stl_file_path(stl_file_path);
			throw runtime_error("Data read from file '" + str_stl_file_path + "' have flaws, see messages above");
//			throw runtime_error("Data read from file '" + to_string(5) + "' have flaws, see messages above");
		}
		double threshold = -0.9;
		m_number_of_folds = CountFolds(threshold);
		if (m_number_of_folds > 0)
		{
			cout << "\nWARNING: Folds are detected in '" << stl_file_path << "'. Processing will continue but may fail if projection collides with a fold\n";
			//char buf[256];
			//AddPostfixToPath(stl_file_path, "_folds", buf, sizeof(buf));
			//auto folds = GetFolds(threshold);
			//FragmentToFile(buf, "Folded triangles", folds, Color::SaladGreen());
		}
	}

	catch (std::exception& ex)
	{
		cout << endl << "Error while reading surface from '" << stl_file_path << "': " << ex.what() << endl;
		char buf[256];
		AddPostfixToPath(stl_file_path, "_err", buf, sizeof(buf));
		WriteToFile(buf, "Surface with error", Color::Yellow());
		throw ex;
//@		exit(-1);
	}
}


Surface::Surface(const Surface& s, const Triangle::Status tri_stat[], std::vector<Triangle> new_triangles)
{
	ImportTriangles(s, tri_stat, new_triangles);
	int k = FindRimVertices();
	cout << "Rims include " << k << " vertices";
	FindRimContours();
	cout << " and " << rims.size() << " contours" << endl;

	// New: calculating mean normals
	for (Triangle& tri : triangles)
		tri.CalculateMeanNormal(&vertices[0], &triangles[0]);

	CheckIntegrity(false);
}

Surface::Surface(const Surface l, const Surface r, const std::vector<SeamTriangle> seam)
{
	// Copying vertices
	cout << "Copying vertices...\n";

	vertices.resize(l.vertices.size() + r.vertices.size());

	memcpy_s((void*)&(vertices[0]), sizeof(Vertex) * l.vertices.size(), (void*)&(l.vertices[0]), sizeof(Vertex) * l.vertices.size());
	memcpy_s((void*)&(vertices[l.vertices.size()]), sizeof(Vertex) * r.vertices.size(), (void*)&(r.vertices[0]), sizeof(Vertex) * r.vertices.size());

	// Copying triangles from left and right surfaces
	cout << "Copying triangles...\n";

	triangles.resize(l.triangles.size() + r.triangles.size() + seam.size());

	unsigned int triangle_offset = l.triangles.size();
	unsigned int vertex_offset = l.vertices.size();
	memcpy_s((void*)&(triangles[0]), sizeof(Triangle) * l.triangles.size(), (void*)&(l.triangles[0]), sizeof(Triangle) * l.triangles.size());
	memcpy_s((void*)&(triangles[triangle_offset]), sizeof(Triangle) * r.triangles.size(), (void*)&(r.triangles[0]), sizeof(Triangle) * r.triangles.size());

	// Offsetting vertex indices from right triangle
	for (unsigned int i = triangle_offset; i < triangle_offset + r.triangles.size(); i++)
	{
		Triangle& tri = triangles[i];
		tri.vertex[0] += vertex_offset;
		tri.vertex[1] += vertex_offset;
		tri.vertex[2] += vertex_offset;
	}

	// Copying seam triangles and offsetting indices of their vertices from the right surface
	cout << "Copying seam triangles...\n";

	const Vertex* p = &(vertices[0]);
	unsigned int seam_offset = l.triangles.size() + r.triangles.size();
	for (unsigned int i = 0; i < seam.size(); i++)
	{
		const SeamTriangle& stri = seam[i];
		Triangle& tri = triangles[seam_offset + i];

		tri.vertex[0] = stri.left_side[0] ? stri.vertex[0] : stri.vertex[0] + vertex_offset;
		tri.vertex[1] = stri.left_side[1] ? stri.vertex[1] : stri.vertex[1] + vertex_offset;
		tri.vertex[2] = stri.left_side[2] ? stri.vertex[2] : stri.vertex[2] + vertex_offset;

		tri.normal = stri.normal;
		tri.center = stri.center;
		//tri.CalculateNormalAndCenter(p);
	}

	RebuildConnectivity();
	CheckIntegrity(true);
}

bool Surface::ImportTriangles(const Surface& s, const Triangle::Status tri_stat[], std::vector<Triangle> new_triangles)
{
	vertices.resize(s.vertices.size());
	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		vertices[i].SetCoords(s.vertices[i]);
		vertices[i].first_link_index = -1;
		vertices[i].last_link_index = -1;
	}

	unsigned int  k, n = 0;
	for (k = 0; k < s.triangles.size(); k++)
		(tri_stat[k] == Triangle::Status::Out) ? 0 : n++;

	cout << "Importing " << n << " of " << s.triangles.size() << " triangles (" << 
		(s.triangles.size() - n) << " dropped) and " << new_triangles.size() << " new" <<endl;

	n = n + new_triangles.size();

	triangles.resize(n);
	vertex_links.resize(4 * n);

	int v_index, link_index;
	unsigned int i = 0, j;

	for (k = 0; k < s.triangles.size(); k++) //+ new_triangles.size(); k++)
	{
		if (tri_stat[k] == Triangle::Status::Out)
			continue;
		const Triangle& old_tri = s.triangles[k];

		triangles[i].normal.SetCoords(old_tri.normal);
		triangles[i].center.SetCoords(old_tri.center);

		for (j = 0; j < 3; j++)
		{
			v_index = old_tri.vertex[j];

			triangles[i].vertex[j] = v_index;
			link_index = Surface::TriangleAndVertex2Link(i, j);  //4 * i + j;

			if (vertices[v_index].first_link_index < 0)
				vertices[v_index].first_link_index = link_index;
			else
				vertex_links[vertices[v_index].last_link_index] = link_index;

			vertices[v_index].last_link_index = link_index;
			vertex_links[link_index] = -1;
		}

		i++;
	}

	for (Triangle& old_tri : new_triangles)
	{
		triangles[i].normal.SetCoords(old_tri.normal);
		triangles[i].center.SetCoords(old_tri.center);

		for (j = 0; j < 3; j++)
		{
			v_index = old_tri.vertex[j];

			triangles[i].vertex[j] = v_index;
			link_index = Surface::TriangleAndVertex2Link(i, j);  //4 * i + j;

			if (vertices[v_index].first_link_index < 0)
				vertices[v_index].first_link_index = link_index;
			else
				vertex_links[vertices[v_index].last_link_index] = link_index;

			vertices[v_index].last_link_index = link_index;
			vertex_links[link_index] = -1;
		}
		i++;
	}

	return true;
}


// Using only 'triangles', removing only from 'triangles' and 'vertices'.
void Surface::RemoveVertexAndItsTriangles(unsigned int vertex_to_remove)
{
//@	ASSERT(vertex_to_remove < vertices.size(), "Vertex_to_remove " << vertex_to_remove << " out of range 0 - " << (vertices.size() - 1));
	if (!(vertex_to_remove < vertices.size()))
		throw runtime_error("Vertex_to_remove " + std::to_string(vertex_to_remove) + " out of range 0 - " + std::to_string(vertices.size() - 1));

	vertices.erase(vertices.begin() + vertex_to_remove);

	unsigned int num_removed_tri = 0;
	for (Triangle& tri : triangles)
		if (tri.vertex[0] == vertex_to_remove || tri.vertex[1] == vertex_to_remove || tri.vertex[2] == vertex_to_remove)
			num_removed_tri++;
	std::vector<Triangle> new_triangles(triangles.size() - num_removed_tri);

	unsigned int k = 0;
	for (Triangle& tri : triangles)
	{
		if (tri.vertex[0] == vertex_to_remove || tri.vertex[1] == vertex_to_remove || tri.vertex[2] == vertex_to_remove)
			continue;
		
		if (tri.vertex[0] > vertex_to_remove)
			tri.vertex[0]--;
		if (tri.vertex[1] > vertex_to_remove)
			tri.vertex[1]--;
		if (tri.vertex[2] > vertex_to_remove)
			tri.vertex[2]--;
		new_triangles[k++] = tri;
	}
//@	ASSERT(k == new_triangles.size(), "RemoveVertexAndItsTriangles: triangle index mismatch, " << k << " vs " << new_triangles.size());
	if (!(k == new_triangles.size()))
		throw runtime_error("RemoveVertexAndItsTriangles: triangle index mismatch, " + std::to_string(k) + " vs " + std::to_string(new_triangles.size()));

	triangles.clear();
	triangles.shrink_to_fit();
	triangles = std::move(new_triangles);
	cout << "RemoveVertexAndItsTriangles: old size " << (triangles.size() + num_removed_tri) << ", new size " << triangles.size() << endl;
}

void Surface::RemoveTriangles(std::set<unsigned int> tri_to_remove)
{
	if (tri_to_remove.size() == 0)
		return;

	// tri_to_remove must have descreasing sorting or be traversed in reverse order
	set<unsigned int>::const_reverse_iterator rit;
	for (rit=tri_to_remove.rbegin(); rit != tri_to_remove.rend(); rit++)
	{
//@		ASSERT(*rit < triangles.size(), "Triangle index " << *rit << " out of range 0 - " << (triangles.size()-1));
		if (!(*rit < triangles.size()))
			throw runtime_error("Triangle index " + std::to_string(*rit) + " out of range 0 - " + std::to_string(triangles.size() - 1));

		triangles.erase(triangles.begin() + *rit);
	}
	RebuildConnectivity();
	CheckIntegrity(false);
}


int Surface::GetATriangleOfVertex(const int vertex) const
{
	int link = vertices[vertex].first_link_index;
	_ASSERT(link >= 0);
	return Surface::Link2Triangle(link);
}









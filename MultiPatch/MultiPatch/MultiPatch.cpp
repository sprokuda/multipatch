#include "MultiPatch.h"

MultiPatch::MultiPatch(QWidget *parent)
    : QWidget(parent)
{

    buttonFont = new QFont("Calibri", 9, QFont::Medium);
    smallFont = new QFont("Calibri", 8, QFont::Medium);

    addFilesButton = new QPushButton(tr("Add Aligner File(s)"));
    addFilesButton->setFixedSize(buttonWidth, buttonHeight);
    addFilesButton->setFont(*buttonFont);
    connect(addFilesButton, SIGNAL(clicked()), this, SLOT(addFiles()));

    QHBoxLayout* buttonLayout1 = new QHBoxLayout;
    buttonLayout1->addWidget(addFilesButton);
    buttonLayout1->addStretch();

    filesTable = new QTableWidget(initialRows, 2, this);
    filesTable->setColumnWidth(0, initialColumnWidth);
    filesTable->setColumnWidth(1, initial2ndColumnWidth);
    filesTable->setHorizontalHeaderLabels({ "Aligner File","Select" });
    filesTable->horizontalHeader()->setFont(*buttonFont);
    filesTable->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    filesTable->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    filesTable->setMinimumHeight(300);

    selectAllButton = new QPushButton(tr("Select All"));
    selectAllButton->setFixedSize(buttonWidth, buttonHeight);
    selectAllButton->setFont(*buttonFont);
    connect(selectAllButton, SIGNAL(clicked()), this, SLOT(selectAll()));

    deleteSelectedButton = new QPushButton(tr("Delete Selected"));
    deleteSelectedButton->setFixedSize(buttonWidth, buttonHeight);
    deleteSelectedButton->setFont(*buttonFont);
    connect(deleteSelectedButton, SIGNAL(clicked()), this, SLOT(deleteSelected()));

    QHBoxLayout* buttonLayout2 = new QHBoxLayout;
    buttonLayout2->addStretch();
    buttonLayout2->addWidget(selectAllButton);
    buttonLayout2->addWidget(deleteSelectedButton);

    addPatchButton = new QPushButton(tr("Add Patch File"));
    addPatchButton->setFixedSize(buttonWidth, buttonHeight);
    addPatchButton->setFont(*buttonFont);
    connect(addPatchButton, SIGNAL(clicked()), this, SLOT(addPatchFile()));

    QHBoxLayout* buttonLayout3 = new QHBoxLayout;
    buttonLayout3->addWidget(addPatchButton);
    buttonLayout3->addStretch();

    patchFileEdit = new QLineEdit;
    patchFileEdit->setEnabled(false);
    patchFileEdit->setFixedHeight(buttonHeight);
    patchFileEdit->setFont(*buttonFont);

    setOutputDirectoryButton = new QPushButton(tr("Set Output Directory"));
    setOutputDirectoryButton->setFixedSize(buttonWidth, buttonHeight);
    setOutputDirectoryButton->setFont(*buttonFont);
    connect(setOutputDirectoryButton, SIGNAL(clicked()), this, SLOT(setOutputDirectory()));

    QHBoxLayout* buttonLayout4 = new QHBoxLayout;
    buttonLayout4->addWidget(setOutputDirectoryButton);
    buttonLayout4->addStretch();

    outputDirectory = new QLineEdit(this);
    outputDirectory->setEnabled(false);
    outputDirectory->setFixedHeight(buttonHeight);
    outputDirectory->setFont(*buttonFont);

    runPatchingButton = new QPushButton(tr("Apply Patch"));
    runPatchingButton->setFont(*buttonFont);
    runPatchingButton->setFixedHeight(buttonHeight);
//    connect(runPatchingButton, SIGNAL(clicked()), this, SLOT(runPatching()));
    connect(runPatchingButton, SIGNAL(clicked()), this, SLOT(runLoopPatching()));

    logTextEdit = new QTextBrowser(this);
    logTextEdit->setFixedHeight(180);
    logTextEdit->setFont(*smallFont);

    exitAppButton = new QPushButton(tr("Exit Program"));
    exitAppButton->setFixedSize(buttonWidth, buttonHeight);
    exitAppButton->setFont(*buttonFont);
//    connect(exitAppButton, &QPushButton::clicked, qApp, &QCoreApplication::quit);
    connect(exitAppButton, &QPushButton::clicked, this, &MultiPatch::exitProgram);

    saveLogButton = new QPushButton(tr("Save Log"));
    saveLogButton->setFixedSize(buttonWidth, buttonHeight);
    saveLogButton->setFont(*buttonFont);
    connect(saveLogButton, SIGNAL(clicked()), this, SLOT(saveLog()));

    QHBoxLayout* buttonLayout5 = new QHBoxLayout;
    buttonLayout5->addStretch();
    buttonLayout5->addWidget(exitAppButton);
    buttonLayout5->addWidget(saveLogButton);

    QLabel* statusLabel = new QLabel("Status:", this);
    statusLabel->setFont(*buttonFont);

    statusOfRun = new QLineEdit(this);
    statusOfRun->setEnabled(false);
    statusOfRun->setFont(*buttonFont);

    QHBoxLayout* buttonLayout6 = new QHBoxLayout;
    buttonLayout6->addWidget(statusLabel);
    buttonLayout6->addWidget(statusOfRun);

    QVBoxLayout* mainLayout = new QVBoxLayout(this);

    mainLayout->addLayout(buttonLayout1);
    mainLayout->addWidget(filesTable);

    mainLayout->addLayout(buttonLayout2);

    mainLayout->addLayout(buttonLayout3);
    mainLayout->addWidget(patchFileEdit);

    mainLayout->addLayout(buttonLayout4);
    mainLayout->addWidget(outputDirectory);

    mainLayout->addWidget(runPatchingButton);
    mainLayout->addWidget(logTextEdit);
    mainLayout->addLayout(buttonLayout5);

    mainLayout->addLayout(buttonLayout6);

    setLayout(mainLayout);
    setWindowTitle(tr("Multiply patch application"));

//    this->setSizePolicy(QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding));
    this->resize(mainWidth, mainHeight);

    thread = new QThread();
    calc = new SurfacePatching();
    calc->moveToThread(thread);
    connect(calc, SIGNAL(calcCompleted()), this, SLOT(enableRunPatchingButton()));
    connect(calc, SIGNAL(runtimeErrorHappened()), this, SLOT(handleRuntimeError()));
    thread->start();

}

MultiPatch::~MultiPatch()
{
}

void MultiPatch::addFiles()
{
    QStringList addedFiles = QFileDialog::getOpenFileNames(this,
        "Select one or more aligner file(s) to add", "C:/", tr("Aligner files (*.stl)"));
    if (addedFiles.isEmpty()) return;
    else
    {
        alignersFiles.append(addedFiles);
        alignersFiles.removeDuplicates();
        setTable(this->alignersFiles);
    }
}

void MultiPatch::setTable(QStringList& lst)
{
    if (lst.isEmpty()) return;

    for (size_t i = 0; i < lst.size(); i++)
    {
        QLineEdit* tmp = new QLineEdit(lst[i]);
        tmp->setEnabled(false);
        tmp->setFont(*buttonFont);
        tmp->setFixedHeight(buttonHeight);

        filesTable->setCellWidget(i, 0, tmp);

        QCheckBox* checkBox = new QCheckBox();   //create QCheckBox
        checkBox->setStyleSheet("text-align: center; margin-left:15%; margin-right:15%;");

        filesTable->setCellWidget(i, 1, checkBox);
        filesTable->setRowHeight(i, buttonHeight);

        if (filesTable->rowCount() == i+1)
        {
            filesTable->insertRow(i+1);
        }

    }
    statusOfRun->setText(QString::fromStdString(std::to_string(lst.size()) + " aligner's files were added"));
    filesTable->resizeRowsToContents();
}

void MultiPatch::selectAll()
{
    for (int i = 0; i < filesTable->rowCount(); i++)
    {
        auto tmp = dynamic_cast<QCheckBox*>(const_cast<QWidget*>(filesTable->cellWidget(i, 1)));
        if(tmp)
        tmp->setCheckState(Qt::Checked);
    }
}

void MultiPatch::deleteSelected()
{
    int i = 0;
    while (i < filesTable->rowCount())
    {
        auto tmp = dynamic_cast<QCheckBox*>(const_cast<QWidget*>(filesTable->cellWidget(i, 1)));
        if (tmp && tmp->checkState() == Qt::Checked)
        {
            filesTable->removeRow(i);
            alignersFiles.removeAt(i);
        }
        else
        {
            i++;
        }

    }

    filesTable->resizeRowsToContents();
}


void MultiPatch::addPatchFile()
{
    patchFilePath = QFileDialog::getOpenFileName(this,
        "Select patch file", "C:/", tr("STL file (*.stl)"));
    patchFileEdit->setText(patchFilePath);
}


void MultiPatch::setOutputDirectory()
{
    outputDirPath = QFileDialog::getExistingDirectory(this,
        "Select directory for patched files", "C:/", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    outputDirectory->setText(outputDirPath);

}

void MultiPatch::runPatching()
{
   runPatchingButton->setEnabled(false);

    QString alignment = "C:/work/stl_files/aligner.stl";
    QString patch = "C:/work/stl_files/patch.stl";
    QString patched = "C:/work/stl_files/patched.stl";

    QMetaObject::invokeMethod(calc, "asynchCalc",
        Qt::QueuedConnection,
//        Qt::DirectConnection,
        Q_ARG(QString, alignment),
        Q_ARG(QString, patch),
        Q_ARG(QString, patched)
    );
}

vector<QString> va =
{
    "C:/work/stl_files/aligner_0.stl",
    "C:/work/stl_files/aligner_1.stl",
    "C:/work/stl_files/aligner_2.stl"
};

vector<QString> vp =
{
    "C:/work/stl_files/patched_0.stl",
    "C:/work/stl_files/patched_1.stl",
    "C:/work/stl_files/patched_2.stl"
};

void MultiPatch::runLoopPatching()
{
    isError = false;

    runPatchingButton->setEnabled(false);
    if (alignersFiles.isEmpty())
    {
        checkBeforeApply("There is no aligner file(s), please add", "Add Aligner file(s)", &MultiPatch::addFiles);

        if (alignersFiles.isEmpty())
        {
            runPatchingButton->setEnabled(true);
            return;
        }
    }

    if (patchFilePath.isEmpty())
    {
        checkBeforeApply("The patch file is not added, please add it", "Add Patch File", &MultiPatch::addPatchFile);

        if (patchFilePath.isEmpty())
        {
            runPatchingButton->setEnabled(true);
            return;
        }
    }

    if (outputDirPath.isEmpty())
    {
        checkBeforeApply("The output directory is not set, please set it", "Set Output Directory", &MultiPatch::setOutputDirectory);
        if (outputDirPath.isEmpty())
        {
            runPatchingButton->setEnabled(true);
            return;
        }
    }

    QMessageBox* msgBox = new QMessageBox();
    msgBox->setText("The patching calculations will be started");
    QPushButton* connectButton = msgBox->addButton(tr("Start Patching"), QMessageBox::ActionRole);
    QPushButton* abortButton = msgBox->addButton(QMessageBox::Abort);

    msgBox->setFont(*buttonFont);
    connectButton->setFont(*buttonFont);
    abortButton->setFont(*buttonFont);

    msgBox->exec();

    if (msgBox->clickedButton() == connectButton)
    {

        addFilesButton->setEnabled(false);
        addPatchButton->setEnabled(false);
        setOutputDirectoryButton->setEnabled(false);
        selectAllButton->setEnabled(false);
        deleteSelectedButton->setEnabled(false);
        runPatchingButton->setEnabled(false);
        saveLogButton->setEnabled(false);

        statusOfRun->setText("Patching is started");
        for (int i = 0; i < filesTable->rowCount(); i++)
        {
            auto tmp = dynamic_cast<QLineEdit*>(const_cast<QWidget*>(filesTable->cellWidget(i, 0)));
            if (tmp)
            {
                QString aligner_file_path = tmp->text();
                QStringList aligner_file_path_splitted = aligner_file_path.split('/');
                QString aligner_file = aligner_file_path_splitted[aligner_file_path_splitted.size() - 1];
//                QString patched_file_path = outputDirPath + "/" + aligner_file.split('.')[0] + "_patched.stl";
                QString patched_file_path = outputDirPath + "/" + aligner_file;

                QMetaObject::invokeMethod(calc, "asynchCalc",
                    Qt::QueuedConnection,
                    //        Qt::DirectConnection,
                    Q_ARG(QString, aligner_file_path),
                    Q_ARG(QString, patchFilePath),
                    Q_ARG(QString, patched_file_path)
                );
            }
        }
    }
    else if (msgBox->clickedButton() == abortButton)
    {
    runPatchingButton->setEnabled(true);
    return;
    }

}

void MultiPatch::updateLog(QString log)
{
    logTextEdit->append(log);
}

void MultiPatch::checkBeforeApply(QString text, QString button_text, fType function)
{
    QMessageBox* msgBox = new QMessageBox();
    msgBox->setFont(*buttonFont);
    msgBox->setText(text);
    QPushButton* connectButton = msgBox->addButton(button_text, QMessageBox::ActionRole);
    connectButton->setFont(*buttonFont);
    QPushButton* abortButton = msgBox->addButton(QMessageBox::Abort);
    abortButton->setFont(*buttonFont);

    msgBox->exec();

    if (msgBox->clickedButton() == connectButton)
    {
        (this->*function)();
    }
    else if (msgBox->clickedButton() == abortButton)
    {
        runPatchingButton->setEnabled(true);
        return;
    }
    if (outputDirPath.isEmpty())
    {
        runPatchingButton->setEnabled(true);
        return;
    }
}


void MultiPatch::saveLog()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Open File To Save Log", "C:/log.txt","Text file (*.txt)");

    QFile file(fileName);

    if (!file.open(QIODevice::WriteOnly)) {
        file.close();
    }
    else {
        file.write(logTextEdit->toPlainText().toUtf8());
        file.close();
    }
}

void MultiPatch::enableRunPatchingButton()
{
    if (numberOfRuns == (filesTable->rowCount() - 2))
    {
        addFilesButton->setEnabled(true);
        addPatchButton->setEnabled(true);
        setOutputDirectoryButton->setEnabled(true);
        selectAllButton->setEnabled(true);
        deleteSelectedButton->setEnabled(true);
        runPatchingButton->setEnabled(true);
        saveLogButton->setEnabled(true);

        if (!isError)
        {
            statusOfRun->setText(alignersFiles[numberOfRuns] + " patching is done, all aligner's files are patched");
        }
        else
        {
            statusOfRun->setText("Patching is done with error(s), see log for details");
        }

        numberOfRuns = 0;
        return;
    }

    statusOfRun->setText(alignersFiles[numberOfRuns] + " patching is done");

    numberOfRuns++;
}

void MultiPatch::exitProgram()
{
    QMessageBox* msgBox = new QMessageBox();
    msgBox->setText("Conform you want exit program");
    QPushButton* connectButton = msgBox->addButton(tr("Exit Program"), QMessageBox::ActionRole);
    QPushButton* abortButton = msgBox->addButton(QMessageBox::Abort);

    msgBox->setFont(*buttonFont);
    connectButton->setFont(*buttonFont);
    abortButton->setFont(*buttonFont);


    msgBox->exec();

    if (msgBox->clickedButton() == connectButton)
    {
        qApp->quit();
    }
    else if (msgBox->clickedButton() == abortButton)
    {
        return;
    }

}

void MultiPatch::handleRuntimeError()
{
    isError = true;
    statusOfRun->setText("Error happened, patching process is aborted");

    QMessageBox* msgBox = new QMessageBox();
    msgBox->setText("Runtime Error happened, see log for details");
    QPushButton* connectButton = msgBox->addButton(tr("Ok"), QMessageBox::ActionRole);
    msgBox->setFont(*buttonFont);
    connectButton->setFont(*buttonFont);

    msgBox->exec();

    //addFilesButton->setEnabled(true);
    //addPatchButton->setEnabled(true);
    //setOutputDirectoryButton->setEnabled(true);
    //selectAllButton->setEnabled(true);
    //deleteSelectedButton->setEnabled(true);
    //runPatchingButton->setEnabled(true);
    //saveLogButton->setEnabled(true);

}

void MultiPatch::handleContextMenu(const QPoint& pos)
{
    //QModelIndex index = filesTable->indexAt(pos);

    //QMenu* menu = new QMenu(this);
    //menu->addAction(new QAction("Action 1", this));
    //menu->addAction(new QAction("Action 2", this));
    //menu->addAction(new QAction("Action 3", this));
    //menu->popup(filesTable->viewport()->mapToGlobal(pos));

//    filesTable->itemAt(pos)->setBackgroundColor(Qt::green);
}



void MultiPatch::mousePressEvent(QMouseEvent* event)
{
    //int l, t, r, b;
    //this->layout()->getContentsMargins(&l, &t, &r, &b);
    ////    std::cout << l << "  " << t << "  " << r << "  " << b << std::endl;

    //int low = t + buttonHeight + this->layout()->spacing() + filesTable->height();
    //int high = low + this->layout()->spacing();

    ////if (event->pos().y() > low && event->pos().y() < high)
    ////{
    ////    this->setCursor(Qt::SplitVCursor);
    ////}
}

void MultiPatch::mouseReleaseEvent(QMouseEvent* event)
{
//    this->unsetCursor();
}

void MultiPatch::mouseMoveEvent(QMouseEvent* event)
{
    //int l, t, r, b;
    //this->layout()->getContentsMargins(&l, &t, &r, &b);
    ////    std::cout << l << "  " << t << "  " << r << "  " << b << std::endl;

    //int low = t + buttonHeight + this->layout()->spacing() + filesTable->height();
    //int high = low + this->layout()->spacing();

    ////  prev1 = low;

    //int max_size = this->height();

    //if (event->pos().y() > low && event->pos().y() < high)
    //{
    //    this->setCursor(Qt::SplitVCursor);


    //        if (prev1 <= event->pos().y())
    //        {
    //            //            this->resize(600, max_size + (event->pos().y() - low));
    //            //            max_size = max_size + (event->pos().y() - low);
    ////        std:cout << event->pos().y() << "  " << event->pos().x() << std::endl;
    //            this->resize(600, max_size + (event->pos().y() - low));
    //            ///            max_size = max_size + (event->pos().y() - low);
    //            std::cout << max_size << std::endl;
    //        }
    //        else
    //        {
    //            if (event->pos().y() > low  && event->pos().y() < high - 2)
    //            {
    //                this->resize(600, max_size - (event->pos().y() - low));
    //                // this->resize(600, max_size - 1);
    //                //            max_size = max_size - (event->pos().y() - low);
    //                std::cout << max_size << std::endl;
    //            }
    //        }

    //}
    //else {
    //    this->unsetCursor();
    //}
    //prev1 = event->pos().y();

    //void QLayout::getContentsMargins(int *left, int *top, int *right, int *bottom) const
}

//    filesTable->setContextMenuPolicy(Qt::CustomContextMenu);
//    connect(filesTable, &QTableWidget::customContextMenuRequested, this, &MultiPatch::handleContextMenu);
//    connect(filesTable, SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(handleContextMenu(QPoint)));

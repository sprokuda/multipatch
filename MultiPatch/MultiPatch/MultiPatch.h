#pragma once

#include <QtWidgets>

#include "SurfacePatching.h"

QT_BEGIN_NAMESPACE
class QPushButton;
class QLabel;
class QLineEdit;
class QTextEdit;
QT_END_NAMESPACE

class MultiPatch : public QWidget
{
    Q_OBJECT
    using fType = void (MultiPatch::*)(void);
public:
    MultiPatch(QWidget *parent = Q_NULLPTR);
    ~MultiPatch();

    QThread* thread;
    SurfacePatching* calc;
    void setTable(QStringList& lst);

    void checkBeforeApply(QString text, QString button_text, fType function);

public slots:
    void addFiles();
    void selectAll();
    void deleteSelected();
    void addPatchFile();
    void setOutputDirectory();
    void runPatching();
    void runLoopPatching();
    void saveLog();
    void updateLog(QString log);
    void enableRunPatchingButton();
    void exitProgram();

    void handleRuntimeError();
    void handleContextMenu(const QPoint& pos);

    virtual void mouseMoveEvent(QMouseEvent* event) override;
    virtual void mousePressEvent(QMouseEvent* event) override;
    virtual void mouseReleaseEvent(QMouseEvent* event) override;
private:
    size_t initialRows = 1;
    size_t initialColumnWidth = 450;
    size_t initial2ndColumnWidth = 50;
    size_t buttonWidth = 170;
    size_t buttonHeight = 30;

    size_t mainWidth = 600;
    size_t mainHeight = 600;

    int numberOfRuns = 0;
    bool isError = false;

//    map<QString, QCheckBox*> files;
//    map<int, QCheckBox*> lines;
    QStringList alignersFiles;

    QString patchFilePath;
    QString outputDirPath;

    QFont* buttonFont;
    QFont* smallFont;

    QPushButton* addFilesButton;

    QTableWidget* filesTable;
    QPushButton* selectAllButton;
    QPushButton* deleteSelectedButton;

    QPushButton* addPatchButton;
    QLineEdit* patchFileEdit;

    QPushButton* setOutputDirectoryButton;
    QLineEdit* outputDirectory;

    QPushButton* runPatchingButton;
    QTextBrowser* logTextEdit;
    QPushButton* exitAppButton;
    QPushButton* saveLogButton;

    QLineEdit* statusOfRun;

};

#pragma once
#include <QObject>
#include <Qmutex.h>
#include "ThreadLogStream.h"

#include "Patch/Surface.h" 
#include "Patch/PatchedSurface.h" 

class SurfacePatching : public QObject
{
    Q_OBJECT
public:
    explicit SurfacePatching(QObject* parent = nullptr);

    ~SurfacePatching()
    {
        if(s) delete s;
        if(p) delete p;
        if(res) delete res;
    };

    int Delay = 5000;

	Surface* s;
	Surface* p;
	PatchedSurface* res;
    QMutex dataMutex;

    Q_INVOKABLE void asynchCalc(const QString& aligner_path, const QString& patch_path, const QString& patched_aligner_path);

    Q_INVOKABLE void asynchCalcTest(const QString& line, const QString& latinAlphabet, const QString& cyrillicAlphabet);
signals:
//    void sendPosition(QString output);
    void calcCompleted();
    void runtimeErrorHappened();

};


#include <QtTest/QTest>
#include <iostream>


#include <iostream>
#include <chrono>
#include <thread>

#include "SurfacePatching.h"
#include <QtTest/QTest>


SurfacePatching::SurfacePatching(QObject* parent) : QObject(parent), dataMutex()
{

}

void SurfacePatching::asynchCalc(const QString& aligner_path, const QString& patch_path, const QString& patched_aligner_path)
{
//    QMutexLocker lock(&dataMutex);
    try
    {
        std::cout << "Loading of aligment" << std::endl;
        s = new Surface(const_cast<char*>(aligner_path.toStdString().c_str()));
        //    fflush(stdout);
        std::cout << "Loading of aligment completed" << std::endl;
        std::cout << "-----------------" << std::endl;
        std::cout << "Loading of patch" << std::endl;
        p = new Surface(const_cast<char*>(patch_path.toStdString().c_str()));
        std::cout << "Loading of patch completed" << std::endl;
        std::cout << "-----------------" << std::endl;

        std::cout << "Aligner patching started" << std::endl;
        //    PatchedSurface res1 = s->ApplyPatch(*p);
        res = new PatchedSurface(s->ApplyPatch(*p));
        std::cout << "Aligner patching completed" << std::endl;

        res->WriteToFile(const_cast<char*>(patched_aligner_path.toStdString().c_str()), "Resulting patched surface", Color::Gray(), Color::Gray(), Color::Gray());

        emit calcCompleted();
    }
    catch (std::runtime_error& e)
    {
        std::cout << "\nRUNTIME ERROR: " << e.what() <<"\n" << std::endl;
        emit runtimeErrorHappened();
        emit calcCompleted();
    }
    catch (std::exception& e)
    {
        std::cout << "\nEXCEPTION: " << e.what() << "\n" << std::endl;
        emit runtimeErrorHappened();
        emit calcCompleted();
    }
    catch (...)
    {
        std::cout << "\nUKNOWN EXCEPTION HAPPENED" << "\n" << std::endl;
        emit runtimeErrorHappened();
        emit calcCompleted();
    }

}

void SurfacePatching::asynchCalcTest(const QString& line, const QString& latinAlphabet, const QString& cyrillicAlphabet)
{
    QString outputText;

    auto getChar = [](const QChar& x, const QString alphabet)
    {
        size_t pos(-1);
        for (int i = 0; i < alphabet.size(); ++i)
        {
            if (x == alphabet[i]) pos = i;
        }
        return pos;
    };

    for (int i = 0; i < line.size(); i++)
    {
        int j = getChar(line.at(i), latinAlphabet);
        int k = getChar(line.at(i), cyrillicAlphabet);

        if (j != -1 && k == -1)
        {
            if (j < latinAlphabet.size() - 1) outputText.append(latinAlphabet[j + 1]);
            else outputText.append(latinAlphabet[0]);
        }
        else if (j == -1 && k != -1)
        {
            if (k < cyrillicAlphabet.size() - 1) outputText.append(cyrillicAlphabet[k + 1]);
            else outputText.append(cyrillicAlphabet[0]);
        }
        else
        {
            outputText.append(line.at(i));
        }
    }

//    QTest::qSleep(Delay);

    std::cout << "Hello I'm waiting...." << std::endl;
    double f;
    for (size_t i = 0; i < 100000000; i++)
    {
        f = sin((double)i);
    }
    std::cout << outputText.toStdString() << std::endl;

//    emit sendPosition(outputText);
};


